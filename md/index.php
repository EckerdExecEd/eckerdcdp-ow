<?php
session_start();
if(!empty($_GET['lerr'])){
    $lerr=$_GET['lerr'];
    if("1"==$lerr){
		$msg="<font color='#ff0000'>Invalid Login<small><i>(<u>Note:</u> Have you entered the correct email address and PIN?.)</i><small></font>";
    }
    if("2"==$lerr){
		$msg="<font color='#ff0000'>Invalid Login<small><i>(<u>Note:</u> Program expired.)</i><small></font>";
    }
    if("3"==$lerr){
		$msg="<font color='#00aa00'><big>You have already completed the survey for this individual.</big></font>";
    }
}
session_unset();
include_once "../../admin/default.php";  //Determines if the logins will be available
$HTML=(!$owdSystemON)? $systemMSG : "Please enter your Email Address: 
<br>
<input type=\"text\" name=\"uid\" value=\"\">
<br>
Please enter your PIN: 
<br>
<input type=\"text\" name=\"pin\" value=\"\">
<br>
<input type=\"submit\" value=\"Log In\">";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head><!-- saved from url=(0043)http://www.eckerd.edu/ldi/about/index.shtml -->
<title>#1 in Leadership</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="Wed, 26 Jan 2005 10:05:59 -0500" name="date">
<link title="standard" media="screen" href="../index_files/eckerd-1.css" type="text/css" rel="stylesheet">
</head>
<body vlink="#990000" alink="#000000" link="#990000" bgcolor="#ffffff">
<center>
<table cellspacing="0" cellpadding="0" width="750" border="0">
  <tbody>
  <tr valign="top" align="left">
    <td><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr>
  <tr valign="top" align="right" bgcolor="#4f8d97">
        <td>
 </td>
      </tr>
  <tr valign="top" align="left">
    <td><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr>
  <tr valign="top" align="left">
    <td>
      <table cellspacing="0" cellpadding="0" width="750" bgcolor="#253355" border="0">
        <tbody>
        <tr valign="middle">
          <td align="left"><img height="64" src="../index_files/eclogotop.gif" width="155" border="0" alt=""></td>
          <td><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td>
          <td align="right"><img height="64" src="../index_files/ldi-title.gif" width="430" border="0" alt=""></td></tr></tbody></table></td></tr>
  <tr valign="top" align="left">
    <td><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr>
  <tr valign="top" align="left">
    <td>
      <table cellspacing="0" cellpadding="0" width="750" border="0">
        <tbody>
        <tr valign="top" align="left">
          <td class="primarynav" width="137" bgcolor="#253355">
            <p>
                  </p><p></p></td>
          <td width="1"><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td>
                <td width="450"> 
                  <table id="main" cellspacing="0" cellpadding="16" width="450" border="0">
              <tbody>
                      <tr valign="top" align="left"> 
                        <td class="content"> <p align="left">Respondent Login</p>
<?=$msg?>
<form id="frm1" action="respondent.php" method=POST>
<input type="hidden" name="s" value="1">
<?php echo $HTML; ?>
</form>
                          </td>
                      </tr></tbody></table></td>
          <td width="1"><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td>
                <td valign="top" width="161" bgcolor="#d2d0c8"> 
                  <div style="padding: 4px;"><span> 
                    </span>
                    <table cellspacing="0" cellpadding="0" width="161" border="0">
                      <tbody>
                        <tr valign="top" align="left" bgcolor="#4d4325"> 
                          <td valign="top"> <div style="padding: 4px; font-weight: bold; color: rgb(255, 255, 255);">Quick 
                              Contact</div></td>
                        </tr>
                        <tr valign="top" align="left" bgcolor="#ffffff"> 
                          <td><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td>
                        </tr>
                        <tr valign="top" align="left"> 
                          <td> 
                            <div style="padding: 4px; color: rgb(77, 67, 37);">
                              <p><b>CDP Customer Service</b></p>
                                Monday - Friday<br>
                                8:00 AM - 5:00 PM<br>
				&nbsp;<br>
                                888-359-9906<br>
                               <p><a href="mailto:cdp@eckerd.edu">cdp@eckerd.edu</a></p>
                              </div></td>
                        </tr>
                      </tbody>
                    </table>
                    <p>&nbsp;</p>
                     </div>    </td>
              </tr>
        <tr>
          <td bgcolor="#253355">
          </td><td>
          </td><td>
            <table cellspacing="0" cellpadding="0" width="450" border="0">
              <tbody>
              <tr bgcolor="#ffffff">
                <td><img height="1" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr>
              <tr bgcolor="#253355">
                <td><img height="30" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr></tbody></table></td>
          <td>
          </td><td bgcolor="#d2d0c8"></td></tr></tbody></table></td></tr>
  <tr valign="top" align="left">
    <td><img height="16" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr>
  <tr valign="top" align="left">
    <td>
      <table cellspacing="0" cellpadding="0" width="750" border="0">
        <tbody>
        <tr valign="top" align="left">
          <td width="100%" colspan="2"><a href="http://www.eckerd.edu/"><img height="51" alt="Eckerd College" src="../index_files/site-logo.gif" width="159" border="0"></a></td>
          <td width="9" rowspan="3"><img height="1" src="../index_files/pixel.gif" width="9" border="0" alt=""></td>
                <td align="right" width="81" rowspan="3">
</td>
              </tr>
        <tr valign="top" align="left">
          <td width="100%" colspan="2"><img height="8" src="../index_files/pixel.gif" width="1" border="0" alt=""></td></tr>
        <tr valign="top" align="left">
          <td width="4"><img height="1" src="../index_files/pixel.gif" width="4" border="0" alt=""></td>
          <td class="footertext" width="100%">4200 54th Avenue South . St. 
            Petersburg, Florida 33711<br>(800) 456-9009 or (727) 867-1166<br><a href="http://www.eckerd.edu/about/index.php?f=contact">E-mail</a> | 
            <a href="http://www.eckerd.edu/departments/index.php?f=home">Directory</a> 
            | <a href="http://www.eckerd.edu/its/index.php?f=pprivacy">Privacy 
            Policy</a><br><br>Copyright 2004 Eckerd College. All rights 
            reserved.</td></tr></tbody></table></td></tr></tbody></table></center><br><!--Begin SiteStats Code Dec 16, 2002-->
</body></html>

