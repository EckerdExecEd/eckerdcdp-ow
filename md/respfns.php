<?php
// Functions for the self survey
require_once "../meta/dbfns.php";
require_once "../meta/survey.php";

// Write the page header for the survey
function writeHeader(){
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>Internet Assessment System - Respondent</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
    echo "<body class=\".body\">";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">";
    echo "<tbody>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
    echo "</table><br>&nbsp;</br>"; 
}

// Write the page footer for the survey
function writeFooter(){
	echo sprintf("%c",169)." 2005 Eckerd College</body></html>";
}

// Write the navigation bar
function writeNavigation($page,$frm,$tid){
	$pic=array();
	$lnk=array();
	
	$pic[0]="../images/prevE.gif";
	$pic[1]="../images/1f.gif";
	$pic[2]="../images/2f.gif";
	$pic[3]="../images/3f.gif";
	$pic[4]="../images/4f.gif";
	$pic[5]="../images/5f.gif";
	$pic[6]="../images/6f.gif";
	$pic[7]="../images/7f.gif";
	$pic[8]="../images/8f.gif";
	$pic[9]="../images/9f.gif";
	$pic[10]="../images/nextE.gif";

	$pp=$page-1;
	$np=$page+1;
	
	$lnk[0]="javascript:$frm.page.value='$pp';$frm.submit();";
	$lnk[1]="javascript:$frm.page.value='1';$frm.submit();";
	$lnk[2]="javascript:$frm.page.value='2';$frm.submit();";
	$lnk[3]="javascript:$frm.page.value='3';$frm.submit();";
	$lnk[4]="javascript:$frm.page.value='4';$frm.submit();";
	$lnk[5]="javascript:$frm.page.value='5';$frm.submit();";
	$lnk[6]="javascript:$frm.page.value='6';$frm.submit();";
	$lnk[7]="javascript:$frm.page.value='7';$frm.submit();";
	$lnk[8]="javascript:$frm.page.value='8';$frm.submit();";
	$lnk[9]="javascript:$frm.page.value='9';$frm.submit();";
	// next page
	$lnk[10]="javascript:$frm.page.value='$np';$frm.submit();";
	
	
	// enable images and links based on the page we're on
	switch($page){
		case 1:
			$pic[0]="../images/prevD.gif";
			$pic[1]="../images/1n.gif";
			$lnk[0]="";
			break;
		case 2:
			$pic[2]="../images/2n.gif";
			break;
		case 3:
			$pic[3]="../images/3n.gif";
			break;
		case 4:
			$pic[4]="../images/4n.gif";
			break;
		case 5:
			$pic[5]="../images/5n.gif";
			break;
		case 6:
			$pic[6]="../images/6n.gif";
			break;
		case 7:
			$pic[7]="../images/7n.gif";
			break;
		case 8:
			$pic[8]="../images/8n.gif";
			break;
		case 9:
			$pic[9]="../images/9n.gif";
			// turn off next
			$pic[10]="";
			$lnk[10]="";
			break;
		default:
	}
	
	echo "<table border=0 cellspacing=0 cellpadding=0><tr>";
	$howMany=(9==$page)?10:11;
	for($i=0;$i<$howMany;$i++){
		echo "<td valign=top onClick=\"".$lnk[$i]."\">";
		echo "<img src='$pic[$i]'>";
		echo "</td>";
	}
	if($page!=9){
		echo "<td valign=top onClick=\"javascript:$frm.what.value='save';$frm.action='respexit.php';$frm.submit();\">&nbsp;&nbsp;<img src='../images/savex.gif'></td>";
	}
	
	echo "</tr></table>";
}

function writeRespondentSurvey($page,$rid,$frm){
	if(1==$page||2==$page){
		showRespondentDemographics($rid,"2",$page);	
	}
	elseif($page>=3 and $page<9){
		showSurvey($rid,"2","1",$page);
	}
	elseif($page==9){
		showByeBye($rid,$frm);
	}
}

function showByeBye($rid,$frm){
	$cnt=getNoAnswerCount($rid,"2");
	if($cnt>7){
		echo "<p><u>Please address the following issues:</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 78</p>&nbsp;</p>";
		$unAnsw=getUnansweredItems($rid,"2");
		echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
	}
	elseif($cnt>0){
		echo "<p><u>Are you sure you want to complete the survey?</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 78</p>&nbsp;</p>";
		$unAnsw=getUnansweredItems($rid,"2");
		echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
		echo "<p> It is strongly recommended that you go back and answer all questions.<br>However, you may click on the 'Submit' button below to complete the survey.</p><p>";
		echo "<input type='button' value='Submit' onClick=\"javascript:$frm.what.value='finish';$frm.action='respexit.php';$frm.submit();\">";
		echo "</p><p>&nbsp;</p>";
	}
	else{
		echo "<p> Click on the 'Submit' button below to complete the survey.</p><p>";
		echo "<input type='button' value='Submit' onClick=\"javascript:$frm.what.value='finish';$frm.action='respexit.php';$frm.submit();\">";
		echo "</p><p>&nbsp;</p>";
	}
}

?>
