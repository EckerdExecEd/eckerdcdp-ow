<?php
session_start();
require_once "../meta/dbfns.php";

if(empty($_SESSION['conid'])){
	// Try and log in
	$uid=addslashes($_POST['uid']);
	$pin=addslashes($_POST['pin']);
	$conid=mdLogin($uid,$pin);
	if(false==$conid){
	// Login error - go back to the login page
	header("Location: mdlogin.php?lerr=1");
	}

	$_SESSION['conid']=$conid;
}
if(empty($_SESSION['conid'])){
	die("You are not logged in.");
}

require_once "meta.php";

	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>Internet Assessment System - Respondent</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
    echo "<body class=\".body\">";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">";
    echo "<tbody>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
    echo "</table><br>&nbsp;</br>"; 
?>
<form action="" method=POST name="frm1">
<p>
<select name="programID">
<option value="">-- Select Session --</option>
<?php
$sessions=getAllMDSessions();
foreach($sessions as $sess){
	echo "<option value='$sess[0]'> $sess[1] (ID: $sess[0]) </option>";
}
?>
</select>
<p>
<input type="button" onClick="javascript:frm1.action='completion.php';frm1.submit();" value="Show Session Completion">
</p>
<p>
<input type="button" onClick="javascript:frm1.action='printdata.php';frm1.submit();" value="Print all responses">
</p>
<p>
<input type="button" onClick="javascript:frm1.action='mdlogin.php';frm1.submit();" value="Exit">
</p>
</form>
</body>
</html>
<?php
function mdLogin($uid,$pin){
	/*$conn=dbConnect();
	$query="select CONID from CONSULTANT where UID='$uid' and PWD=PASSWORD('$pin')";
	$rs=mysql_query($query);
	if(false==$rs){
		//echo $query;
		return false;
	}
	$row=mysql_fetch_array($rs);
	return count($row>0)?$row[0]:false;*/
	$mysqli=dbiConnect();
	if (!($query = $mysqli->prepare("select CONID from CONSULTANT where UID=? and PWD=PASSWORD(?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_param("ss", stripslashes($uid), $pin)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$query->execute()) {
		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_result($out_CONID)) {
		echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$row = $query->fetch();
	$query->close();
	if(!($row)){
		return false;
	}else{
		return $out_CONID;
	}
}

function getAllMDSessions(){
	$conn=dbConnect();
	$query="select programID,programTitle from MDData order by programTitle";
	$rs=mysql_query($query);
	if(false==$rs){
		//echo $query;
		return false;
	}
	return dbRes2Arr($rs);
}
?>

