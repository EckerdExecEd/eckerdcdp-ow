<?php
require_once "../meta/dbfns.php";
require_once "../md/meta.php";
require_once "../md/mailfns.php";

// Will get all incomplete raters for program that are due to be mailed today
// 1. Initial invitation
function getAllIncompleteRaters1($today1,$today2){
	$conn=dbConnect();
	$query="SELECT a.EMAIL,a.FNAM,a.LNAM,b.name,b.firstName,b.lastName,c.fromAddress,a.RID,c.surveyDueDate,c.programTitle,c.fromName from RATER a, MDParticipant b, MDData c  ";
	$query=$query." where a.CATID<>1 and a.ENDDT is NULL and c.raterInvitationDate between '$today1 00:00:00' and '$today2 23:59:59' ";
	$query=$query." and b.programID=c.programID and b.CID=a.CID";
	//echo $query;
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query;
		return false;
	}
	return dbRes2Arr($rs);
}

// 2. 1st Reminder
function getAllIncompleteRaters2($today1,$today2){
	$conn=dbConnect();
	$query="SELECT a.EMAIL,a.FNAM,a.LNAM,b.name,b.firstName,b.lastName,c.fromAddress,a.RID,c.surveyDueDate,c.programTitle,c.fromName from RATER a, MDParticipant b, MDData c  ";
	$query=$query." where a.CATID<>1 and a.ENDDT is NULL and c.initialReminderDate between '$today1 00:00:00' and '$today2 23:59:59' ";
	$query=$query." and b.programID=c.programID and b.CID=a.CID";
	//echo $query;
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query;
		return false;
	}
	return dbRes2Arr($rs);
}

// 3. 2nd Reminder
function getAllIncompleteRaters3($today1,$today2){
	$conn=dbConnect();
	$query="SELECT a.EMAIL,a.FNAM,a.LNAM,b.name,b.firstName,b.lastName,c.fromAddress,a.RID,c.surveyDueDate,c.programTitle,c.fromName from RATER a, MDParticipant b, MDData c  ";
	$query=$query." where a.CATID<>1 and a.ENDDT is NULL and c.finalReminderDate between '$today1 00:00:00' and '$today2 23:59:59' ";
	$query=$query." and b.programID=c.programID and b.CID=a.CID";
	//echo $query;
	$rs=mysql_query($query);
	if(false==$rs){
		echo $query;
		return false;
	}
	return dbRes2Arr($rs);
}

// Sends the emails
function sendRaterEmail($rows){
	foreach($rows as $row){
		// send to address
		$to=$row[0];
		
		// name of rater
		$rat=$row[1]." ".$row[2];
		
		// name of candidate
		$cand=(0<strlen($row[3]))?$row[3]:$row[4]." ".$row[5];
		
		// reply to address, i.e. MDParticipant.fromAddress
		$repTo=$row[6];
		
		// sender address - same as above
		$snd=$row[6]." (".$row[10].")";
		
		// bcc address
		$bcc=COMMON_MAILBOX;
		
		// pin number
		$pin=$row[7];
		
		// survey due date, in a nice format
		$dueDt=$row[8];
		$dueDt=strtotime($dueDt);
		$dueDt=strftime("%m/%d/%Y",$dueDt);
		
		// the subject is the project title
		$subject=$row[9];
		
		//3echo "$to $rat $cand $repTo $snd $bcc $pin $dueDt $title<br>";
		
		$body="Dear $rat,\n\n";
		$body=$body."You have been selected by $cand, to complete the Conflict\n"; 
		$body=$body."Dynamics Profile. The Conflict Dynamics Profile (CDP) was developed to prevent\n";
		$body=$body."harmful conflict in the workplace. It provides individuals with a greater awareness of\n";
		$body=$body."how they respond when faced with conflict so that they can improve on those behaviors\n";
		$body=$body."causing the most problems.\n\n";

		$body=$body."To access the questionnaire, please click on the link below:\n\n";

		$body=$body.getURLRoot()."/md/respondent.php?uid=$to&pin=$pin\n\n";

		$body=$body."You will be asked to provide your email address and a PIN number.\n\n";

		$body=$body."Your PIN number is $pin.\n\n";

		$body=$body."Please complete the survey by $dueDt.\n\n";  

		$body=$body."The questionnaire will take about 20-25 minutes to complete.\n\n";  

		$body=$body."***Please save this email and PIN number should you need to return to the\n"; 
		$body=$body."questionnaire.\n\n";

		$body=$body."Confidentiality:\n";
		$body=$body."If you are a direct report or a peer, you feedback will be kept strictly confidential.  Your\n";
		$body=$body."name will not be linked to your responses.\n";
		$body=$body."If you are a supervisor, your feedback will be openly shared and your name will be linked\n";
		$body=$body."to your responses.\n\n";

		$body=$body."Getting Help:\n";
		$body=$body."For assistance, please contact the CDP Customer Service.\n";
		$body=$body."Phone:  888-359-9906\n";
		$body=$body."Email: cdp@eckerd.edu\n";
		$body=$body."Hours: 8:00 AM - 5:00 PM\n"; 
		$body=$body."       (Monday - Friday)\n\n";

		$body=$body."Thank you,\n";
		$body=$body."The Mercer Delta Program Team\n\n";

		
		sendMercerDataMail($to,$snd,$repTo,$bcc,$subject,$body);
	}
}

// Do this when the script run
// get todays date, in database format
$today=strtotime('today');
$today=strftime("%Y-%m-%d",$today);
$today1=$today;
$today2=$today;

//echo $today;
// for testing only:
//$today1="2001-01-01";
//$today2="2010-12-31";

// any inital emails?
$rows=getAllIncompleteRaters1($today1,$today2);
sendRaterEmail($rows);
// any first reminders?
//$rows=getAllIncompleteRaters2($today1,$today2);
//sendRaterEmail($rows);
// any second reminders?
//$rows=getAllIncompleteRaters3($today1,$today2);
//sendRaterEmail($rows);

?>

