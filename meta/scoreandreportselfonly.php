<?php
// Pulls in dbfns.php as well
require_once "multilingual.php";

// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


//----------------------------------------------------
//--- Scoring
//----------------------------------------------------

// Has the candidate responded to enough questions?
function isItComplete($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from RATERRESP where RID=$cid and TID=3 and VAL is not NULL");
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	// We allow 3 missing answers currently i.e. must have 96 of 99 to score
	return $row[0]>=96;
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from SELFONLYREPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where RID=$cid and CID=$cid and PID=$pid";
	mysql_query($query);
}

// Compute the raw and standardized scale scores for a single self only rater
function computeSelfOnlyScaleScores($cid,$pid){
	$conn=dbConnect();
	
	// Raw score
	$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $pid, $cid, 1, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and a.RID=$cid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;	
	}
	
	// Standardized score
	$rows=getScaleMeanAndStd($cid,$pid);
	if(!$rows){
		return false;	
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$cid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "Calculated scale scores - OK";	
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
// for self only
function getScaleMeanAndStd($cid,$pid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL1,a.DEVVAL1,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$cid and b.CID=$cid and b.RID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate report scores for self only
function calculateSelfOnlyReportScores($cid,$pid){
	$conn=dbConnect();
	// always calculate for Self
	$query="insert into SELFONLYREPORTSCORE (SID,CID,AVGSCORE) select SID,CID,STDSCORE from SCALESCORE where RID=$cid and CID=$cid and PID=$pid and CATID=1 and STDSCORE is not NULL";
	if(false===mysql_query($query)){
		return false;
	}
	return "Calculated individual report scores - OK";
}

// checks to see if a license has been used for this candidate
function alreadyConsumedLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

// Consumes a self-only license
function consumeSelfOnlyLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("insert into SELFLIC (CONID,CID,TID,TS) values ($conid,$cid,$tid,NOW())");
	return $rs;
}

// Score all the candidates in a program
function scoreProgram($pid){
	$conn=dbConnect();
	$msg="Scoring...";
	
	$rs=mysql_query("select CONID from PROGCONS where PID=$pid");
	$row=mysql_fetch_row($rs);
	$conid=$row[0];
	
	$rs=mysql_query("select CID,FNAME,LNAME from CANDIDATE where PID=$pid");
	$rows=dbRes2Arr($rs);
	if(false==$rows){
		return "<font color=\"#ff0000\">Unable to score program: unable to retrive candidates</font>";
	}
	
	$ok=false;
	foreach($rows as $row){
		// we really don't need to score here since there's no group report
		// so let's just iterate over each candidate
		$msg=$msg."<br> $row[1] $row[2]";
		$cid=$row[0];
		consumeSelfOnlyLicense($cid,$conid,3);
		$ok=true;
	}
	
	if(false==$ok){
		return "<font color=\"#ff0000\">Unable to score program: no candidates</font>";
	}
	
	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	return $msg."<br>";
}

//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]." ".$row[1];	
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,&$pdf,$lid){
	$name=getCandidateName($cid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);
	
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
//	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
	$what=$mltxt[1];
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),754);

	return $name;	
}

function writeReportFooter(&$pdf,$name,&$page,$lid){
	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);
	
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,35,515,15);
	pdf_fill_stroke($pdf);
	$rptdt=date('D M d, Y');
	//$what=$rptdt."             This report was prepared for: ".$name."               Page ".$page;
	$what=$rptdt."             $mltxt[2] ".$name."               $mltxt[3] ".$page;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),39);	
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}	

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");
	
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,$page,"1");
}

function renderPage1($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);
	
	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,600,0.2);
		
	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	$title="Individual Version";
	$title=$mltxt[11];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),525);
	$title="Feedback Report";
	$title=$mltxt[12];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),500);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="Sal Capobianco, Ph.D.               Mark Davis, Ph.D.               Linda Kraus, Ph.D.";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),450);

	// 6 inches form top of 11 inch page
	$size=14.0;
	$ypos=round((5.25*PAGE_HEIGHT)/11);
	$linehgth=1.5*$size;
	
	pdf_setfont($pdf,$font,$size);
//	$title="Prepared for:";
	$title=$mltxt[13];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos);

	pdf_show_xy($pdf,strtoupper($name),calcCenterStartPos($pdf,strtoupper($name),$font,$size),$ypos-$linehgth);

	$title=date("F j, Y");
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos-$linehgth-$linehgth);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage2($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	
	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=16.0;
	pdf_setfont($pdf,$bfont,$size);
	
	// Table of contents
	$title=$mltxt[21];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),650);
	
	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$y=650;
	$x=55;
	$x2=475;
	
//	Introduction
	pdf_show_xy($pdf,$mltxt[22],$x,$y-=30);	
	pdf_show_xy($pdf,"3",$x2,$y);

//	Guide to Your Feedback Report
	pdf_show_xy($pdf,$mltxt[23],$x,$y-=30);	
	pdf_show_xy($pdf,"4",$x2,$y);

//	Constructive Response Profile
	pdf_show_xy($pdf,$mltxt[24],$x,$y-=30);	
	pdf_show_xy($pdf,"5",$x2,$y);

//	Destructive Response Profile
	pdf_show_xy($pdf,$mltxt[25],$x,$y-=30);	
	pdf_show_xy($pdf,"6",$x2,$y);

//	Hot Buttons Profile
	pdf_show_xy($pdf,$mltxt[26],$x,$y-=30);	
	pdf_show_xy($pdf,"7",$x2,$y);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage3($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind3");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	// Introduction
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);
	
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
//	Conflict refers to any situation in which people have incompatible interests, goals, principles, or
//	feelings. This is, of course, a broad definition and encompasses many different situations. A conflict
//	could arise, for instance, over a long-standing set of issues, a difference of opinion about strategy or
//	tactics in the accomplishment of some business goal, incompatible beliefs, competition for resources,
//	and so on. Conflicts can also result when one person acts in a way that another individual sees as
//	insensitive, thoughtless, or rude. A conflict, in short, can result from anything that places you and
//	another person in opposition to one another.
	pdf_show_xy($pdf,$mltxt[11],50,640);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
//	Thus, conflict in life is inevitable. Despite our best efforts to prevent it, we inevitably find ourselves
//	in disagreements with other people at times. This is not, however, necessarily bad. Some kinds of
//	conflict can be productive--differing points of view can lead to creative solutions to problems. What
//	largely separates useful conflict from destructive conflict is how the individuals respond when the
//	conflict occurs. Thus, while conflict itself is inevitable, ineffective and harmful responses to conflict can
//	be avoided, and effective and beneficial responses to conflict can be learned. That proposition is at
//	the heart of the Conflict Dynamics Profile (CDP) Feedback Report you have received.
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	
//	Some responses to conflict, whether occurring at its earliest stages or after it develops, can be
//	thought of as constructive responses. That is, these responses have the effect of not escalating the
//	conflict further. They tend to reduce the tension and keep the conflict focused on ideas, rather than
//	personalities. Destructive responses, on the other hand, tend to make things worse--they do little to
//	reduce the conflict, and allow it to remain focused on personalities. If conflict can be thought of as a
//	fire, then constructive responses help to put the fire out, while destructive responses make the fire
//	worse. Obviously, it is better to respond to conflict with constructive rather than destructive responses.
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);
	pdf_continue_text($pdf,$mltxt[39]);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

//	It is also possible to think of responses to conflict not simply as constructive or destructive, but as
//	differing in terms of how active or passive they are. Active responses are those in which the individual
//	takes some overt action in response to the conflict or provocation. Such responses can be either
//	constructive or destructive--what makes them active is that they require some overt effort on the part
//	of the individual. Passive responses, in contrast, do not require much in the way of effort from the
//	person. Because they are passive, they primarily involve the person deciding to not take some kind of
//	action. Again, passive responses can be either constructive or destructive--that is, they can make
//	things better or they can make things worse.
	pdf_continue_text($pdf,$mltxt[41]);
	pdf_continue_text($pdf,$mltxt[42]);
	pdf_continue_text($pdf,$mltxt[43]);
	pdf_continue_text($pdf,$mltxt[44]);
	pdf_continue_text($pdf,$mltxt[45]);
	pdf_continue_text($pdf,$mltxt[46]);
	pdf_continue_text($pdf,$mltxt[47]);
	pdf_continue_text($pdf,$mltxt[48]);
	pdf_continue_text($pdf,$mltxt[49]);

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage4($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind4");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	
//	Guide to your Feedback Report
	$title=$mltxt[1];
	
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);
	
	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,$mltxt[11],50,640);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,$mltxt[32]);
	
	writeReportFooter($pdf,$name,$page,$lid);
}

function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyGraph(&$pdf,$x,$y,$width,$height,$data,$lid){
	$flid=getFLID("meta","indgraph");
	$mltxt=getMLText($flid,"3",$lid);

	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=count($data);
	$ystep=round($height/7);
	$xstep=round($width/($scales+1));
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10.0);
	
	// 1. the box outline 	
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);
	
	// 2. Horizontal divisions
	$i;
	for($i=0;$i<6;$i++){
		pdf_moveto($pdf,($x+$xstep),$y+$ystep+round($i*$ystep));
		pdf_lineto($pdf,($x+$width),$y+$ystep+round($i*$ystep));
	}

	// divide the first box
	pdf_moveto($pdf,($x+$xstep),($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));
	
	// little box that juts out
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x,$y);
	pdf_lineto($pdf,$x,$y+round($ystep/2));
	pdf_lineto($pdf,$x+$xstep,$y+round($ystep/2));
	
	
	// 3. Vertical divisions
	for($i=$x+round(2*$xstep);$i<$x+$width;$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,$y+$ystep);
	}
	pdf_stroke($pdf);
	
	// 4. Vertical Scale
	pdf_setfont($pdf,$font,10.0);

	$vs=array("35","40","45","50","55","60","");
	$j=0;
	for($i=($y+$ystep);($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		if(3==$j){
			pdf_setfont($pdf,$font,10.0);
			// Average
			pdf_show_xy($pdf,$mltxt[31],$x-15,$i);
			pdf_setfont($pdf,$font,10.0);
		}
		$j++;
	}

	pdf_show_xy($pdf,"65",($x+round(2*$xstep/3)),$y+$height-2);
	pdf_setfont($pdf,$font,10.0);
	
	//"Very","Low","","","High","Very"
	//"Low","","","","","High"
	$vs=array($mltxt[32],$mltxt[33],"","",$mltxt[34],$mltxt[32]);
	$vs1=array($mltxt[33],"","","","",$mltxt[34]);
	$j=0;
	for($i=($y+round(1.5*$ystep));($i<$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],$x-3,$i);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}

	// which scale do we have? we can determine from the SID
	switch($data[0][3]){
		case 1:
			//"Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out","Reflective Thinking","Delay Responding","Adapting"
			$vs=array($mltxt[1],$mltxt[2],$mltxt[3],$mltxt[4],$mltxt[5],$mltxt[6],$mltxt[7]);
			break;
		case 8:
			//"Winning","Displaying Anger","Demeaning Others","Retaliating","Avoiding","Yielding","Hiding Emotions","Self- Criticizing"
			$vs=array($mltxt[11],$mltxt[12],$mltxt[13],$mltxt[14],$mltxt[15],$mltxt[16],$mltxt[17],$mltxt[18]);
			break;
		case 22:
			//"Unrely","Over Analyze","Unapp","Aloof","Micro Manage","Self Centered","Abrasive","Untrust","Hostile"
			$vs=array($mltxt[21],$mltxt[22],$mltxt[23],$mltxt[24],$mltxt[25],$mltxt[26],$mltxt[27],$mltxt[28],$mltxt[29]);
			break;
	}
	
	// 5. Draw the scale
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$offset=round(0.45*$ystep);	
		pdf_show_boxed($pdf,$vs[$i],$j,($y+round($ystep/2)),$xstep,$offset,"center","");
		$j+=$xstep;
	}

	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,$y,$xstep,round(0.35*$ystep),"center","");
		$j+=$xstep;
	}

	// Value
	pdf_show_xy($pdf,$mltxt[35],$x+5,$y+5);
	
	// 7. Draw the graph
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
		// 01-25-2005: Fixing a border condition
		$n1=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
		// 01-25-2005: Fixing a border condition
		$n1=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
			$n1=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=31;
			$n1=31;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			pdf_moveto($pdf,$j,$y+$ystep+($n1*($ystep/5)));
		}
		
		$yc=$y+$ystep+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			pdf_lineto($pdf,$j,$yc);
		}
	}

	pdf_moveto($pdf,$x+5,$y+round($ystep/3));
	pdf_lineto($pdf,$x+$xstep-5,$y+round($ystep/3));
	pdf_stroke($pdf);	

	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+$ystep+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+$ystep+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	
	pdf_circle($pdf,$x+round($xstep/2),$y+round($ystep/3),4);
	pdf_fill_stroke($pdf);
}

function renderPage5($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Constructive Responses
	$title=$mltxt[51];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	Higher numbers are more desirable
	$title=$mltxt[52];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage6($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//  Destructive Responses
	$title=$mltxt[61];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=$mltxt[62];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyGraph($pdf,50,150,500,450,$rows,$lid);	
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage7($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind7");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	$ifont=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
		
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title=$mltxt[1];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);
	

	//This portion of the Conflict Dynamics Profile Feedback Report is a bit different from the others.
	//Instead of indicating how you typically respond to conflict situations, this section provides insight into
	//the kinds of people and situations which are likely to upset you and potentially cause conflict to occur:
	//in short, your hot buttons.
	//Below you will find a brief description of each of the hot buttons measured by the CDP, and on the
	//following page a graph which illustrates how upsetting--compared to people in general--you find each
	//situation. Obviously, these do not represent every possible hot button that people may have; they are
	//simply some of the most common ones. In each case, a higher score on the scale indicates that you
	//get especially irritated and upset by that particular situation.

	$size=10.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,$mltxt[11],70,660);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,$mltxt[19]);
	pdf_continue_text($pdf,$mltxt[20]);


	//You get especially irritated and upset when working with people who are
	//unreliable, miss deadlines, and cannot be counted on.
	//You get especially irritated and upset when working with people who are
	//perfectionists, overanalyze things, and focus too much on minor issues.
	//You get especially irritated and upset when working with people who fail to
	//give credit to others or seldom praise good performance.
	//You get especially irritated and upset when working with people who
	//isolate themselves, do not seek input from others, or are hard to approach.
	//You get especially irritated and upset when working with people who
	//constantly monitor and check up on the work of others.
	//You get especially irritated and upset when working with people who are
	//self-centered, or believe they are always correct.
	//You get especially irritated and upset when working with people who are
	//arrogant, sarcastic, and abrasive.
	//You get especially irritated and upset when working with people who
	//exploit others, take undeserved credit, or cannot be trusted.
	//You get especially irritated and upset when working with people who lose
	//their tempers, become angry, or yell at others.

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_xy($pdf,$mltxt[21],180,500);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[64]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[29]);
	pdf_continue_text($pdf,$mltxt[30]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);

	pdf_setfont($pdf,$bfont,$size);
	

	//Unreliable
	//Overly-Analytical
	//Unappreciative
	//Aloof
	//Micro-Managing
	//Self-Centered
	//Abrasive
	//Untrustworthy
	//Hostile

	pdf_show_xy($pdf,$mltxt[2],70,500);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[3]);
	pdf_continue_text($pdf,$mltxt[63]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[4]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[5]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[6]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[7]);
	pdf_continue_text($pdf,$mltxt[67]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[8]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[9]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[10]);
	writeReportFooter($pdf,$name,$page,$lid);
}

function renderPage8($cid,&$pdf,&$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid);

	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	Hot Buttons
	$title=$mltxt[81];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
//	(Lower numbers are more desirable)
	$title=$mltxt[82];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);

	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){			
		drawSelfOnlyGraph(&$pdf,75,225,450,375,$rows,$lid);	
	}
	else{			
		pdf_show_xy($pdf,"Cannot graph hot buttons",200,500);	
	}

	writeReportFooter($pdf,$name,$page,$lid);
}

?>
