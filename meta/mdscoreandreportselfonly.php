<?php
// Pulls in dbfns.php as well
require_once "multilingual.php";

// A standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


//----------------------------------------------------
//--- Scoring
//----------------------------------------------------

// Has the candidate responded to enough questions?
function isItComplete($cid,$pid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from RATERRESP where RID=$cid and TID=3 and VAL is not NULL");
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	// We allow 3 missing answers currently i.e. must have 96 of 99 to score
	return $row[0]>=96;
}

// get rid of all pre-existing scores for this candidate
function getRidOfOldScores($cid,$pid){
	$conn=dbConnect();
	$query="delete from SELFONLYREPORTSCORE where CID=$cid";
	mysql_query($query);
	$query="delete from SCALESCORE where RID=$cid and CID=$cid and PID=$pid";
	mysql_query($query);
}

// Compute the raw and standardized scale scores for a single self only rater
function computeSelfOnlyScaleScores($cid,$pid){
	$conn=dbConnect();

	// Raw score
	$query="insert into SCALESCORE (SID,RID,PID,CID,CATID,RAWSCORE,STDSCORE) ";
	$query=$query."select b.SID, a.RID, $pid, $cid, 1, AVG(VAL), 0 from RATERRESP a , SCALEITEM b, RATER c ";
	$query=$query."where a.ITEMID=b.ITEMID and a.RID=c.RID and a.RID=$cid and VAL is not NULL group by b.SID,a.RID";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}

	// Standardized score
	$rows=getScaleMeanAndStd($cid,$pid);
	if(!$rows){
		return false;
	}
	foreach($rows as $row){
		if(0!=$row[2]){
			$std=50+(10*(($row[0]-$row[1])/$row[2]));
			$query="update SCALESCORE set STDSCORE=$std where SID=$row[3] and RID=$cid and CID=$cid and PID=$pid";
			mysql_query($query);
		}
	}
	return "Calculated scale scores - OK";
}

// function to return the appropriate Mean and Standard deviation etc for a Scale
// for self only
function getScaleMeanAndStd($cid,$pid){
	$conn=dbConnect();
	$query="select b.RAWSCORE,a.MEANVAL1,a.DEVVAL1,a.SID from SCALE a,SCALESCORE b where a.SID=b.SID and b.RID=$cid and b.CID=$cid and b.RID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
		return false;
	}
	return dbRes2Arr($rs);
}

// calculates the appropriate report scores for self only
function calculateSelfOnlyReportScores($cid,$pid){
	$conn=dbConnect();
	// always calculate for Self
	$query="insert into SELFONLYREPORTSCORE (SID,CID,AVGSCORE) select SID,CID,STDSCORE from SCALESCORE where RID=$cid and CID=$cid and PID=$pid and CATID=1 and STDSCORE is not NULL";
	if(false===mysql_query($query)){
		return false;
	}
	return "Calculated individual report scores - OK";
}

// checks to see if a license has been used for this candidate
function alreadyConsumedLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("select count(*) from SELFLIC where CONID=$conid and CID=$cid and TID=$tid");
	$row=mysql_fetch_row($rs);
	return $row[0]>0?true:false;
}

// Consumes a self-only license
function consumeSelfOnlyLicense($cid,$conid,$tid){
	$conn=dbConnect();
	$rs=mysql_query("insert into SELFLIC (CONID,CID,TID,TS) values ($conid,$cid,$tid,NOW())");
	return $rs;
}

// Score all the candidates in a program
function scoreProgram($pid){
	$conn=dbConnect();
	$msg="Scoring...";

	$rs=mysql_query("select CONID from PROGCONS where PID=$pid");
	$row=mysql_fetch_row($rs);
	$conid=$row[0];

	$rs=mysql_query("select CID,FNAME,LNAME from CANDIDATE where PID=$pid");
	$rows=dbRes2Arr($rs);
	if(false==$rows){
		return "<font color=\"#ff0000\">Unable to score program: unable to retrive candidates</font>";
	}

	$ok=false;
	foreach($rows as $row){
		// we really don't need to score here since there's no group report
		// so let's just iterate over each candidate
		$msg=$msg."<br> $row[1] $row[2]";
		$cid=$row[0];
		consumeSelfOnlyLicense($cid,$conid,3);
		$ok=true;
	}

	if(false==$ok){
		return "<font color=\"#ff0000\">Unable to score program: no candidates</font>";
	}

	// Set status to "Scored"
	$query="update PROGRAM set EXPIRED='S' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);
	return $msg."<br>";
}

//----------------------------------------------------
//------ Reporting
//----------------------------------------------------
function getCandidateName($cid){
	$conn=dbConnect();
	$query="select FNAME, LNAME from CANDIDATE where CID=$cid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0]." ".$row[1];
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function writeHeader($cid,&$pdf,$lid,$pos=false){
	$name=getCandidateName($cid);
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);

	if(false!=$pos){
		// If we're here we should draw a band on top
		// Why do we have to divide by 2 ????
		// start 1 1/8 from top
		$y=PAGE_HEIGHT-(72*1.125/2);
		// 1/4 inch tall
		$height=(72*.25)/2;
		// the width is 1 3/4 less than the page
		$width=PAGE_WIDTH-(72*1.75/2);
		// Use font size 10.0
		$size=10.0;

		$flid=getFLID("meta","mdi0");
		$mltxt=getMLText($flid,"3",$lid);

		if("left"==$pos){
			//a left justified band
			$x=0;
			pdf_setcolor($pdf,'both','rgb',0,0,0,0);
			pdf_rect($pdf,$x,$y,$width,$height);
			pdf_fill_stroke($pdf);
			$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
			pdf_setfont($pdf,$font,$size);
			pdf_setcolor($pdf,'both','rgb',1,1,1,0);
			$what=strtoupper($mltxt[1]);
			$x=(72*1.75)/2;
			pdf_show_xy($pdf,$what,$x,$y-1);
		}
		elseif("right"==$pos){
			// a right justified band
			$x=(72*1.75)/2;
			pdf_setcolor($pdf,'both','rgb',0,0,0,0);
			pdf_rect($pdf,$x,$y,$width,$height);
			pdf_fill_stroke($pdf);
			$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
			pdf_setfont($pdf,$font,$size);
			pdf_setcolor($pdf,'both','rgb',1,1,1,0);
			$what=strtoupper("$mltxt[2] $name");
			$w=pdf_stringwidth($pdf,$what,$font,$size);
			// make it end 1 inch from the right edge
			// and it should sort of stick out of the band at the bottom
			$x=PAGE_WIDTH-$w-(72/2);
			pdf_show_xy($pdf,$what,$x,$y-1);
		}
	}
	return $name;
}

// These are some canned functions to get fonts etc.
function getH1(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,17);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",20.4);
	return $font;
}

function getH2(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,16);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",19.2);
	return $font;
}

function getH3(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,13);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",15.6);
	return $font;
}

function getH4(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,12);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.4);
	return $font;
}

function getSubtext(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getBody(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getQuote(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Oblique","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",12.0);
	return $font;
}

function getAttribution(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,8.5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",10.2);
	return $font;
}

function getTableHead(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTableSide(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,9.5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTableText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTableTextSm(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,9.5);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyRedText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0.718,0.0,0.02,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getTinyGreenText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$font,6);
	pdf_setcolor($pdf,'both','rgb',0.608,0.667,0.098,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getSmallText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,8);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function getSkinnyText(&$pdf){
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,24);
	pdf_setcolor($pdf,'both','rgb',0,0,0,0);
	pdf_set_value($pdf,"leading",14.0);
	return $font;
}

function writeReportFooter(&$pdf,$page,$lid,$pos=false){
	if(false!=$pos){
		// .75 inches from bottom
		$y=72*0.75;
		$x=0;
		if("right"==$pos){
			// 1 1/4 inch from right edge
			$x=PAGE_WIDTH-(72*1.25);
		}
		elseif("left"==$pos){
			// 1 inch from left edge
			$x=72;
		}

		$flid=getFLID("meta","mdi0");
		$mltxt=getMLText($flid,"3",$lid);

		$size=6.5;
		pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
		$font=pdf_findfont($pdf,"Helvetica","host",0);
		pdf_setfont($pdf,$font,$size);
		//$what=$mltxt[3];
		$tempTxt=$GLOBALS['tempTxt'];
		$what = $tempTxt[$lid]['footer'];
		pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,$size),$y);

		$size=10.0;
		$font=pdf_findfont($pdf,"Helvetica-Bold","host",0);
		pdf_setfont($pdf,$font,$size);
		pdf_show_xy($pdf,$page,$x,$y);
	}
	pdf_end_page($pdf);
}

function renderStatusPage($cid,&$pdf,$msg){
	$name=writeHeader($cid,$pdf,"1");

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Unable to generate Feedback Report";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Candidate: ".$name,75,675);
	pdf_continue_text($pdf," ");
	foreach($msg as $msgline){
		pdf_continue_text($pdf,$msgline);
	}
	writeReportFooter($pdf,$name,"","1");
}


// This just inserts some blank pages
function renderBlankPage($cid,&$pdf,$page,$pid,$lid){
	writeHeader($cid,$pdf,$lid,false);
	writeReportFooter($pdf,$page,$lid,false);
}

// This is the cover page "i"
// We don't have the spec yet
function renderPageI($cid,&$pdf,$page,$pid,$lid){
  $tempTxt=$GLOBALS['tempTxt'];
	$name=writeHeader($cid,$pdf,$lid,false);

	$flid=getFLID("meta","mdi0");
	$mltxt=getMLText($flid,"3",$lid);

	$x=(72*4.875);
	$y=PAGE_HEIGHT-(72*0.875);
	$font=getSkinnyText($pdf);
//	pdf_show_xy($pdf,"MERCER DELTA",$x,$y);

	$x=(72*6.125);
	$y=PAGE_HEIGHT-(72*1.125);
	$font=getBody($pdf);
//	pdf_show_xy($pdf,"Organizational Consulting",$x,$y);

  // place new background image here...
  $iType=$tempTxt[$lid]['cover_image_type'];
  $iFile=$tempTxt[$lid]['cover_image_file'];
  $iRatio=$tempTxt[$lid]['cover_image_ratio'];
  $img=pdf_open_image_file($pdf,$iType,$iFile,"",0);
  pdf_place_image($pdf,$img,418,725,$iRatio);   

/*
	$x=(72*5.0);
	$y=PAGE_HEIGHT-(72*2.6125);
	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.15);

	// draw "the band"
	pdf_setcolor($pdf,"both","rgb",(129/255),(171/255),(186/255),0);
	$x=(72*0.25);
	$sz=(72*0.28);
	$sp=(72*0.125);
	$y=PAGE_HEIGHT-(72*3.95);
	for($i=0;$i<20;$i++){
		pdf_rect($pdf,$x+($i*($sz+$sp)),$y,$sz,$sz);
		pdf_rect($pdf,$x+($i*($sz+$sp)),$y-($sz+$sp),$sz,$sz);
		pdf_rect($pdf,$x+($i*($sz+$sp)),$y-(2*($sz+$sp)),$sz,$sz);
		if($i==5||$i==6||$i==7){
			pdf_fill_stroke($pdf);
		}
		else{
			pdf_stroke($pdf);
		}
	}
*/
	//$x=(72*2.25);
	$x=(102*2.25);
	$y=PAGE_HEIGHT-(72*4.95);
	$font=getBody($pdf);
	//pdf_show_xy($pdf,"EXECUTIVE LEARNING CENTER",$x,$y);

	$y=PAGE_HEIGHT-(72*5.60);
	$font=getH4($pdf);
	pdf_show_xy($pdf,$mltxt[13],$x,$y); // 'prepared for:' text?

	//$y=PAGE_HEIGHT-(72*5.90);
	//$font=getH1($pdf);
	$font=getH3($pdf);
	pdf_show_xy($pdf,strtoupper($name),$x+75,$y);

	$y=PAGE_HEIGHT-(72*5.90);
	$font=getBody($pdf);
	$rptdt=date('M d, Y');
	pdf_show_xy($pdf,$rptdt,$x,$y);

	//$y=PAGE_HEIGHT-(72*7.0);
	$y=PAGE_HEIGHT-(72*6.2);
	$font=getH4($pdf);
	pdf_show_xy($pdf,"Individual Version Feedback Report",$x,$y);

	$y=PAGE_HEIGHT-(72*6.9);
	$font=getH4($pdf);
	pdf_show_xy($pdf,"Conflict Dynamics Profile",$x,$y);

	$y=PAGE_HEIGHT-(72*7.20);
	$font=getBody($pdf);
	pdf_show_xy($pdf,"Sal Capobianco, Ph. D | Mark Davis, Ph. D. | Linda Kraus, Ph. D.",$x,$y);
/*
	$font=getSmallText($pdf);
	$y=PAGE_HEIGHT-(72*9.0);
	pdf_show_xy($pdf,"1631 NW Thurman Street, Suite 100",$x,$y);
	$y=PAGE_HEIGHT-(72*9.25);
	pdf_show_xy($pdf,"Portland, OR 97209 USA",$x,$y);
	$y=PAGE_HEIGHT-(72*9.5);
	pdf_show_xy($pdf,"+ 1 866.237.4685; + 503.223.5678",$x,$y);
	$y=PAGE_HEIGHT-(72*9.75);
	pdf_show_xy($pdf,"HTTP://elc.oliverwyman.com",$x,$y);
*/
	$y=PAGE_HEIGHT-(72*10.5);

	//$img=pdf_open_image_file($pdf,"JPEG","../images/mmclogo.jpg","",0);
	//pdf_place_image($pdf,$img,$x,$y,0.3);

	writeReportFooter($pdf,$page,$lid,false);
}

// This is the credits page "ii"
function renderPageII($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,false);
  $tempTxt=$GLOBALS['tempTxt'];
	$x=(72*1.75)/2;
	$y=PAGE_HEIGHT-(72*10.5);
	$font=getBody($pdf);
	$txtA=$tempTxt[$lid]['pageii_1'];
	$txtB=$tempTxt[$lid]['pageii_2'];	
	pdf_show_xy($pdf,$txtA, $x, $y);
	pdf_continue_text($pdf,$txtB);
	writeReportFooter($pdf,$page,$lid,false);
}

// This is the content page "iii"
function renderPageIII($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$flid=getFLID("meta","mdi0");
	$mltxt=getMLText($flid,"3",$lid);

	$x=(72*1.75)/2;
	$y=PAGE_HEIGHT-(72*2.75);
	$font=getH1($pdf);

	pdf_show_xy($pdf,strtoupper($mltxt[21]),$x,$y);

	$y=PAGE_HEIGHT-(72*3.5);
	$font=getSubtext($pdf);

	pdf_show_xy($pdf,$mltxt[22],$x,$y);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,"");
	pdf_continue_text($pdf,$mltxt[26]);

	writeReportFooter($pdf,$page,$lid,"right");
}


function renderPage1zzzzzz($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,false);

/*	$flid=getFLID("meta","ind0");
	$mltxt=getMLText($flid,"3",$lid);

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,600,0.2);

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
//	$title="Individual Version";
	$title=$mltxt[11];

	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),525);
	$title="Feedback Report";
	$title=$mltxt[12];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),500);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="Sal Capobianco, Ph.D.               Mark Davis, Ph.D.               Linda Kraus, Ph.D.";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),450);

	// 6 inches form top of 11 inch page
	$size=14.0;
	$ypos=round((5.25*PAGE_HEIGHT)/11);
	$linehgth=1.5*$size;

	pdf_setfont($pdf,$font,$size);
//	$title="Prepared for:";
	$title=$mltxt[13];
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos);

	pdf_show_xy($pdf,strtoupper($name),calcCenterStartPos($pdf,strtoupper($name),$font,$size),$ypos-$linehgth);

	$title=date("F j, Y");
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),$ypos-$linehgth-$linehgth);
*/
	writeReportFooter($pdf,$name,$page,$lid,false);
}

// This is the introduction page, right jsutified
function renderPage1($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.25);

	$flid=getFLID("meta","mdi3");
	$mltxt=getMLText($flid,"3",$lid);

	$font=getH2($pdf);
	// Introduction
	$title=$mltxt[1];

	pdf_show_xy($pdf,$title,$x,$y);

	$font=getBody($pdf);
//	Conflict refers to any situation in which people have incompatible interests, goals, principles, or
//	feelings. This is, of course, a broad definition and encompasses many different situations. A conflict
//	could arise, for instance, over a long-standing set of issues, a difference of opinion about strategy or
//	tactics in the accomplishment of some business goal, incompatible beliefs, competition for resources,
//	and so on. Conflicts can also result when one person acts in a way that another individual sees as
//	insensitive, thoughtless, or rude. A conflict, in short, can result from anything that places you and
//	another person in opposition to one another.

//	Thus, conflict in life is inevitable. Despite our best efforts to prevent it, we inevitably find ourselves
//	in disagreements with other people at times. This is not, however, necessarily bad. Some kinds of
//	conflict can be productive--differing points of view can lead to creative solutions to problems. What
//	largely separates useful conflict from destructive conflict is how the individuals respond when the
//	conflict occurs. Thus, while conflict itself is inevitable, ineffective and harmful responses to conflict can
//	be avoided, and effective and beneficial responses to conflict can be learned. That proposition is at
//	the heart of the Conflict Dynamics Profile (CDP) Feedback Report you have received.

	$y=PAGE_HEIGHT-(72*1.75);

	pdf_show_xy($pdf,$mltxt[11],$x,$y);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	if(isset($mltxt[18])){
		pdf_continue_text($pdf,$mltxt[18]);
	}

	pdf_continue_text($pdf,$mltxt[21]);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf,"   ");

//	Some responses to conflict, whether occurring at its earliest stages or after it develops, can be
//	thought of as constructive responses. That is, these responses have the effect of not escalating the
//	conflict further. They tend to reduce the tension and keep the conflict focused on ideas, rather than
//	personalities. Destructive responses, on the other hand, tend to make things worse--they do little to
//	reduce the conflict, and allow it to remain focused on personalities. If conflict can be thought of as a
//	fire, then constructive responses help to put the fire out, while destructive responses make the fire
//	worse. Obviously, it is better to respond to conflict with constructive rather than destructive responses.
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);
	pdf_continue_text($pdf,$mltxt[39]);
	pdf_continue_text($pdf,"   ");

//	It is also possible to think of responses to conflict not simply as constructive or destructive, but as
//	differing in terms of how active or passive they are. Active responses are those in which the individual
//	takes some overt action in response to the conflict or provocation. Such responses can be either
//	constructive or destructive--what makes them active is that they require some overt effort on the part
//	of the individual. Passive responses, in contrast, do not require much in the way of effort from the
//	person. Because they are passive, they primarily involve the person deciding to not take some kind of
//	action. Again, passive responses can be either constructive or destructive--that is, they can make
//	things better or they can make things worse.
	pdf_continue_text($pdf,$mltxt[41]);
	pdf_continue_text($pdf,$mltxt[42]);
	pdf_continue_text($pdf,$mltxt[43]);
	pdf_continue_text($pdf,$mltxt[44]);
	pdf_continue_text($pdf,$mltxt[45]);
	pdf_continue_text($pdf,$mltxt[46]);
	pdf_continue_text($pdf,$mltxt[47]);
	pdf_continue_text($pdf,$mltxt[48]);
	pdf_continue_text($pdf,$mltxt[49]);

	$flid=getFLID("meta","mdi4");
	$mltxt=getMLText($flid,"3",$lid);

	$font=getH2($pdf);
	$y=PAGE_HEIGHT-(72*8.75);

//	Guide to your Feedback Report
	$title=$mltxt[1];

	pdf_show_xy($pdf,$title,$x,$y);

	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*9.125);

	pdf_show_xy($pdf,$mltxt[11],$x,$y);

	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*9.375);

	pdf_show_xy($pdf,$mltxt[12],$x,$y);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);

	writeReportFooter($pdf,$page,$lid,"right");
}


// This is the second page, two boxes + some text left justified
function renderPage2($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,"left");

	// define RGB values for some colors
	// light red
	$r1=239/255;
	$g1=193/255;
	$b1=175/255;

	// light orange
	$r2=254/255;
	$g2=232/255;
	$b2=174/255;

	// light green
	$r3=235/255;
	$g3=241/255;
	$b3=195/255;

	// light blue
	$r4=228/255;
	$g4=236/255;
	$b4=237/255;

	// gray
	$r5=230/255;
	$g5=230/255;
	$b5=230/255;

	// Graph width should be 6.25 inches
	$width=72*6.25;
	// Each box should be a multiple of .25 inches
	$ystep=72*0.25;

	// This is the left margin
	$x=(72*1.75/2);

	// Get the text for the page
	$flid=getFLID("meta","mdi4");
	$mltxt=getMLText($flid,"3",$lid);

	// Graphics + text for "Constructive"
	//========================================================================
	$y=PAGE_HEIGHT-(72*4.5);
	$height=14*$ystep;

	// Gray rectangle on top
	pdf_setcolor($pdf,"both","rgb",$r5,$g5,$b5,0);
	pdf_rect($pdf,$x,$y+(13*$ystep),$width,$ystep);
	pdf_fill_stroke($pdf);

	// green recatngle on top
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r3,$g3,$b3,0);
	pdf_rect($pdf,$x,$y+(5*$ystep),$xstep,8*$ystep);
	pdf_fill_stroke($pdf);

	// blue rectangle on top
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r4,$g4,$b4,0);
	pdf_rect($pdf,$x,$y,$xstep,5*$ystep);
	pdf_fill_stroke($pdf);

	// Horizontal and vertical lines
	pdf_setcolor($pdf,"stroke","rgb",0,0,0,0);
	pdf_setlinewidth($pdf,0.25);
	$draw=array(false,true,false,true,false,true,false,true,false,true,false,true,false,true);
	for($i=0;$i<=13;$i++){
		if($draw[$i]){
			pdf_moveto($pdf,$x,$y+($i*$ystep));
			pdf_lineto($pdf,$x+$width,$y+($i*$ystep));
		}
	}
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x+$xstep,$y+$height);
	pdf_stroke($pdf);

	// Outer rectangle
	pdf_setlinewidth($pdf,0.5);
	pdf_rect($pdf,$x,$y,$width,$height);
	pdf_stroke($pdf);

	// Headers
	$padding=5;
	$font=getTableHead($pdf);
	pdf_show_boxed($pdf,$mltxt[53],$x+$padding,$y+(13*$ystep),$xstep,$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[54],$x+($width/2),$y+(13*$ystep),$xstep,$ystep,"left","");

	$font=getTableSide($pdf);
	$data=array($mltxt[43],"",$mltxt[42],"",$mltxt[41],"",$mltxt[40],"",$mltxt[39],"",$mltxt[38],"",$mltxt[37]);
	$offs=0.25;
	for($i=0;$i<=12;$i++){
		pdf_show_xy($pdf,$data[$i],$x+$padding,$y+(($i+$offs)*$ystep));
		$offs=-0.25;
	}

	$font=getTableText($pdf);
	$data=array($mltxt[93],$mltxt[92],"",$mltxt[91],"",$mltxt[90],"",$mltxt[89],"",$mltxt[88],"",$mltxt[87]);
	$h=$ystep;
	for($i=0;$i<=11;$i++){
		pdf_show_boxed($pdf,$data[$i],$x+$xstep+$padding,$y+($i*$ystep),$width-$xstep-(2*$padding),$h,"left","");
		$h=2*$ystep;
	}

	// Graphics + text for "Destructive"
	//=================================================================
	$y=PAGE_HEIGHT-(72*9.125);
	$height=12*$ystep;

	// Gray rectangle on top
	pdf_setcolor($pdf,"both","rgb",$r5,$g5,$b5,0);
	pdf_rect($pdf,$x,$y+(11*$ystep),$width,$ystep);
	pdf_fill_stroke($pdf);

	// green recatngle on top
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r1,$g1,$b1,0);
	pdf_rect($pdf,$x,$y+(5*$ystep),$xstep,6*$ystep);
	pdf_fill_stroke($pdf);

	// blue rectangle on top
	$xstep=$width*(2/6.25);
	pdf_setcolor($pdf,"both","rgb",$r2,$g2,$b2,0);
	pdf_rect($pdf,$x,$y,$xstep,5*$ystep);
	pdf_fill_stroke($pdf);

	// Horizontal and vertical lines
	pdf_setcolor($pdf,"stroke","rgb",0,0,0,0);
	pdf_setlinewidth($pdf,0.25);
	$draw=array(false,false,true,true,true,true,false,true,false,true,true,true);
	for($i=0;$i<=11;$i++){
		if($draw[$i]){
			pdf_moveto($pdf,$x,$y+($i*$ystep));
			pdf_lineto($pdf,$x+$width,$y+($i*$ystep));
		}
	}
	pdf_moveto($pdf,$x+$xstep,$y);
	pdf_lineto($pdf,$x+$xstep,$y+$height);
	pdf_stroke($pdf);

	// Outer rectangle
	pdf_setlinewidth($pdf,0.5);
	pdf_rect($pdf,$x,$y,$width,$height);
	pdf_stroke($pdf);

	// text
	$padding=5;
	$fond=getTableHead($pdf);
	pdf_show_boxed($pdf,$mltxt[55],$x+$padding,$y+(11*$ystep),$xstep,$ystep,"left","");
	pdf_show_boxed($pdf,$mltxt[56],$x+($width/2),$y+(11*$ystep),$xstep,$ystep,"left","");

	$font=getTableSide($pdf);
	$data=array("",$mltxt[51],$mltxt[50],$mltxt[49],$mltxt[48],"",$mltxt[47],"",$mltxt[46],$mltxt[45],$mltxt[44]);
	$offs=0.25;
	for($i=0;$i<=12;$i++){
		if($i==1||$i==6||$i==8){
			$offs=-0.25;
		}
		pdf_show_xy($pdf,$data[$i],$x+$padding,$y+(($i+$offs)*$ystep));
		$offs=0.25;
	}

	$font=getTableTextSm($pdf);
	$padding=3;
	$data=array($mltxt[101],"",$mltxt[100],$mltxt[99],$mltxt[98],$mltxt[97],"",$mltxt[96],"",$mltxt[95],$mltxt[94]);
	$h=$ystep;
	for($i=0;$i<=12;$i++){
		if($i==0||$i==5||$i==7){
			$h=2*$ystep;
		}
		pdf_show_boxed($pdf,$data[$i],$x+$xstep+$padding,$y+($i*$ystep),$width-$xstep,$h,"left","");
		$h=$ystep;
	}

	// The explanatory text
	//=========================================================================
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*5);
	$title=$mltxt[21];
	pdf_show_xy($pdf,$title,$x,$y);

	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*5.25);
	pdf_show_xy($pdf,$mltxt[22],$x,$y);
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[25]);

	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*9.5);
	$title=$mltxt[31];
	pdf_show_xy($pdf,$title,$x,$y);

	$font=getBody($pdf);
	$y=PAGE_HEIGHT-(72*9.75);
	pdf_show_xy($pdf,$mltxt[32],$x,$y);

	writeReportFooter($pdf,$page,$lid,"left");
}

// Returns graph data
// Note we start with the highest SID and step upwards
function getSelfOnlyResponses($cid,$loSid,$hiSid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CID, a.SID from SELFONLYREPORTSCORE a, SCALE b where a.CID=$cid and a.SID=b.SID and b.SID between $loSid and $hiSid order by a.SID desc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function drawSelfOnlyBarGraph(&$pdf,$data,$n,$startx,$starty,$constructive=false){
	$xbase=$startx+(72*1.65);
	$ybase=$starty+(72*0.75);
	$ystep=(72*0.37);
	$xstep=(72*0.7125/5);
	$h=(72*0.0625);

	for($i=0;$i<$n;$i++){
		$val=round($data[$i][0]);
		if($val<35){
			if($constructive){
				$font=getTinyRedText($pdf);
			}
			else{
				$font=getTinyGreenText($pdf);
			}
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// Backward box
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase-$xstep-1,$ybase+($i*$ystep),$xstep-2.5,$h);
			pdf_fill_stroke($pdf);
		}
		elseif($val>65){
			if($constructive){
				$font=getTinyGreenText($pdf);
			}
			else{
				$font=getTinyRedText($pdf);
			}
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// Box to the limit
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase,$ybase+($i*$ystep),(31*$xstep),$h);
			pdf_fill_stroke($pdf);
		}
		elseif($val==35){
			$font=getTinyText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// just fill in the line with a narrow box
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase-3,$ybase+($i*$ystep),2,$h);
			pdf_fill_stroke($pdf);
		}
		else{
			$font=getTinyText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			pdf_rect($pdf,$xbase,$ybase+($i*$ystep),(($val-35)*$xstep),$h);
			pdf_fill_stroke($pdf);
		}
	}
}

// Third page, two graphs, right justified
function renderPage3($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);

	$flid=getFLID("meta","mdi4");
	$mltxt=getMLText($flid,"3",$lid);

	// Put the graphs in place
	$y=PAGE_HEIGHT-(72*5.5);
	$yCon=$y;
//	$img=pdf_open_image_file($pdf,"JPEG","../images/01_Individual_Constructive300.jpg","",0);
//  pdf_place_image($pdf,$img,$x,$y,0.2);
	$img=pdf_open_image_file($pdf,"JPEG","../images/01_Individual_Constructive150.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.4);

	$y=PAGE_HEIGHT-(72*10.0);
	$yDes=$y;
//	$img=pdf_open_image_file($pdf,"JPEG","../images/02_Individual_Destructive300.jpg","",0);
//  pdf_place_image($pdf,$img,$x,$y,0.2);
	$img=pdf_open_image_file($pdf,"JPEG","../images/02_Individual_Destructive150.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.4);

	// Add the static text
	$font=getH2($pdf);
	$y=PAGE_HEIGHT-(72*1.25);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);

	$font=getH3($pdf);
	$y=PAGE_HEIGHT-(72*1.75);
	pdf_show_xy($pdf,$mltxt[33],$x,$y);
	$y=PAGE_HEIGHT-(72*6.0);
	pdf_show_xy($pdf,$mltxt[35],$x,$y);

	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*2);
	pdf_show_xy($pdf,$mltxt[34],$x,$y);
	$y=PAGE_HEIGHT-(72*6.25);
	pdf_show_xy($pdf,$mltxt[36],$x,$y);


	// Ready to graph!
	// Get Constructive data
	$rows=getSelfOnlyResponses($cid,"1","7");
	if($rows){
		drawSelfOnlyBarGraph($pdf,$rows,7,$x,$yCon,true);
	}
	else{
		$font=getH4($pdf);
		pdf_show_text($pdf,"Can't graph constructive responses",$x,$yCon);
	}
	// Get Constructive data
	$rows=getSelfOnlyResponses($cid,"8","15");
	if($rows){
		drawSelfOnlyBarGraph($pdf,$rows,8,$x,$yDes,false);
	}
	else{
		$font=getH4($pdf);
		pdf_show_text($pdf,"Can't graph destructive responses",$x,$yDes);
	}
	// 22-30 are the hot buttons

	writeReportFooter($pdf,$page,$lid,"right");
}

// Fourth page:Hot Button Intro, left jsutified
function renderPage4($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,"left");
	$x=(72*1.75/2);
	$y=PAGE_HEIGHT-(72*1.125);

	$flid=getFLID("meta","mdiHB");
	$mltxt=getMLText($flid,"3",$lid);

	$font=getH3($pdf);
	$title=$mltxt[1];
	pdf_show_xy($pdf,$title,$x,$y);


	//This portion of the Conflict Dynamics Profile Feedback Report is a bit different from the others.
	//Instead of indicating how you typically respond to conflict situations, this section provides insight into
	//the kinds of people and situations which are likely to upset you and potentially cause conflict to occur:
	//in short, your hot buttons.
	//Below you will find a brief description of each of the hot buttons measured by the CDP, and on the
	//following page a graph which illustrates how upsetting--compared to people in general--you find each
	//situation. Obviously, these do not represent every possible hot button that people may have; they are
	//simply some of the most common ones. In each case, a higher score on the scale indicates that you
	//get especially irritated and upset by that particular situation.

	$y=PAGE_HEIGHT-(72*1.5);
	$font=getBody($pdf);
	pdf_show_xy($pdf,$mltxt[11],$x,$y);
	pdf_continue_text($pdf,$mltxt[12]);
	pdf_continue_text($pdf,$mltxt[13]);
	pdf_continue_text($pdf,$mltxt[14]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[15]);
	pdf_continue_text($pdf,$mltxt[16]);
	pdf_continue_text($pdf,$mltxt[17]);
	pdf_continue_text($pdf,$mltxt[18]);
	pdf_continue_text($pdf,$mltxt[19]);
	pdf_continue_text($pdf,$mltxt[20]);


	//You get especially irritated and upset when working with people who are
	//unreliable, miss deadlines, and cannot be counted on.
	//You get especially irritated and upset when working with people who are
	//perfectionists, overanalyze things, and focus too much on minor issues.
	//You get especially irritated and upset when working with people who fail to
	//give credit to others or seldom praise good performance.
	//You get especially irritated and upset when working with people who
	//isolate themselves, do not seek input from others, or are hard to approach.
	//You get especially irritated and upset when working with people who
	//constantly monitor and check up on the work of others.
	//You get especially irritated and upset when working with people who are
	//self-centered, or believe they are always correct.
	//You get especially irritated and upset when working with people who are
	//arrogant, sarcastic, and abrasive.
	//You get especially irritated and upset when working with people who
	//exploit others, take undeserved credit, or cannot be trusted.
	//You get especially irritated and upset when working with people who lose
	//their tempers, become angry, or yell at others.

	$y=PAGE_HEIGHT-(72*3.75);

	pdf_show_xy($pdf,$mltxt[21],$x+(72*1.75),$y);
	pdf_continue_text($pdf,$mltxt[22]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[23]);
	pdf_continue_text($pdf,$mltxt[24]);
	pdf_continue_text($pdf,$mltxt[64]);

	pdf_continue_text($pdf,$mltxt[25]);
	pdf_continue_text($pdf,$mltxt[26]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[27]);
	pdf_continue_text($pdf,$mltxt[28]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[29]);
	pdf_continue_text($pdf,$mltxt[30]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[31]);
	pdf_continue_text($pdf,$mltxt[32]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[33]);
	pdf_continue_text($pdf,$mltxt[34]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[35]);
	pdf_continue_text($pdf,$mltxt[36]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[37]);
	pdf_continue_text($pdf,$mltxt[38]);



	//Unreliable
	//Overly-Analytical
	//Unappreciative
	//Aloof
	//Micro-Managing
	//Self-Centered
	//Abrasive
	//Untrustworthy
	//Hostile

	$font=getSubtext($pdf);
	pdf_show_xy($pdf,$mltxt[2],$x,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[3]);
	pdf_continue_text($pdf,$mltxt[63]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[4]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[5]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[6]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[7]);
	pdf_continue_text($pdf,$mltxt[67]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[8]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[9]);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,$mltxt[10]);

	writeReportFooter($pdf,$page,$lid,"left");
}

function plotSeaUrchin(&$pdf,$x,$y){
	$font=pdf_findfont($pdf,"ZapfDingbats","builtin",0);
	pdf_setfont($pdf,$font,28.0);
	pdf_show_xy($pdf,"W",$x,$y);
//	pdf_show_xy($pdf,"WHXVM",$x,$y);
}

function drawSelfHotButtonBarGraph(&$pdf,$data,$n,$startx,$starty){
	$xbase=$startx+(72*1.65);
	$ybase=$starty+(72*0.375);
	$ystep=(72*0.2875);
	$xstep=(72*0.7125/5);
	$h=(72*0.0625);
	$d=(72*0.0125);

	for($i=0;$i<$n;$i++){
		$val=round($data[$i][0]);
		if($val<35){
			$font=getTinyGreenText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// Backward box
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase-$xstep-1,$ybase+$d+($i*$ystep),$xstep-2.5,$h);
			pdf_fill_stroke($pdf);
		}
		elseif($val>65){
			$font=getTinyRedText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// Box to the limit
			pdf_setcolor($pdf,"both","rgb",0,0,0,0);
			pdf_rect($pdf,$xbase,$ybase+$d+($i*$ystep),31*$xstep,$h);
			pdf_fill_stroke($pdf);
			plotSeaUrchin($pdf,$xbase+(30.5*$xstep)-4,$ybase+($i*$ystep)-7);
		}
		elseif($val==35){
			$font=getTinyText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			// nothing
		}
		else{
			$font=getTinyText($pdf);
			pdf_show_xy($pdf,$val,$xbase-(2*$xstep)-2, $ybase+($i*$ystep));
			pdf_rect($pdf,$xbase,$ybase+$d+($i*$ystep),(($val-35)*$xstep),$h);
			pdf_fill_stroke($pdf);
			plotSeaUrchin($pdf,$xbase+(($val-35)*$xstep)-5,$ybase+($i*$ystep)-7);
		}
	}
}


// Hot buttons, one graph, right justified
function renderPage5($cid,&$pdf,$page,$pid,$lid){
	$name=writeHeader($cid,$pdf,$lid,"right");

	$x=(72*1.75/2);

	$flid=getFLID("meta","mdi4");
	$mltxt=getMLText($flid,"3",$lid);

	// Put the graphs in place
	$y=PAGE_HEIGHT-(72*4.75);
	$yCon=$y;
//	$img=pdf_open_image_file($pdf,"JPEG","../images/03_Individual_Hot_Button300.jpg","",0);
//	pdf_place_image($pdf,$img,$x,$y,0.2);
	$img=pdf_open_image_file($pdf,"JPEG","../images/03_Individual_Hot_Button150.jpg","",0);
	pdf_place_image($pdf,$img,$x,$y,0.4);

	// Add the static text
	$font=getH3($pdf);
	$y=PAGE_HEIGHT-(72*1.125);
	pdf_show_xy($pdf,$mltxt[3],$x,$y);

	$font=getH4($pdf);
	$y=PAGE_HEIGHT-(72*1.375);
	pdf_show_xy($pdf,$mltxt[36],$x,$y);


	// Ready to graph!
	// Get HotButtons data
	$rows=getSelfOnlyResponses($cid,"22","30");
	if($rows){
		drawSelfHotButtonBarGraph($pdf,$rows,9,$x,$yCon);
	}
	else{
		$font=getH4($pdf);
		pdf_show_text($pdf,"Can't graph hot buttons",$x,$yCon);
	}

	writeReportFooter($pdf,$page,$lid,"right");
}
?>

