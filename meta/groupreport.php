<?php
include_once "dbfns.php";

// a standard Letter page is 8.5 x 11 and 72 dpi
define("PAGE_WIDTH",612);
define("PAGE_HEIGHT",792);

// RGB values for the report
define("R_",0.85);
define("G_",0.85);
define("B_",1.0);


function getNumRaters($cid){
	$conn=dbConnect();
	$query="select a.CATID, count(distinct a.RID) from GRPRATER a, RATERRESP b where a.RID=b.RID and a.CATID<>1 and a.GID=$cid and b.VAL is not NULL group by a.CATID order by a.CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}


function getRatersByCat($gid){
	$rows=getRaterCount($gid);
	if(!$rows){
		return false;
	}
	$data=array(0,0,0,0,0,0,0,0);
	foreach($rows as $row){
		$data[$row[0]]=$row[1];
	}
	$data[5]=($data[3]+$data[4]);
	return $data;
}

function getGroupName($gid){
	$conn=dbConnect();
	$query="select DESCR from GRPRPT where GID=$gid";
	$rs=mysql_query($query);
	$row=mysql_fetch_row($rs);
	return $row[0];
}

function getRaterCount($gid){
	$conn=dbConnect();
	$query="select a.CATID, count(a.RID) from GRPRATER a where a.GID=$gid group by a.CATID order by a.CATID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function calcCenterStartPos(&$pdf,$txt,$font,$size){
		$width=pdf_stringwidth($pdf,$txt,$font,$size);
		$delta=(PAGE_WIDTH-$width);
		return (int)($delta/2);
}

function renderPage1($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$img=pdf_open_image_file($pdf,"JPEG","../images/cdp.jpg","",0);
	pdf_place_image($pdf,$img,150,600,0.2);

	$font=pdf_findfont($pdf,"Times-Roman","host",0);
	$bfont=pdf_findfont($pdf,"Times-Bold","host",0);
	$size=36.0;
	pdf_setfont($pdf,$bfont,$size);
	$title="Group Profile";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),500);

	$size=24.0;
	$ypos=350;

	$linehgth=1.5*$size;
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,strtoupper($name),calcCenterStartPos($pdf,strtoupper($name),$font,$size),$ypos-$linehgth);

	$raters=getRatersByCat($gid);
	if($raters){
		// Build the appropriate text
		// the counts and interaction levels
		$descr=array("","Self","Boss","Peers","Direct Reports");
		$counts=array(0,0,0,0,0,0,0,0);
		// Display the results
		$y=200;
		$size=10.0;
		pdf_setfont($pdf,$bfont,$size);
		pdf_show_xy($pdf,"Rater",250,$y);
		pdf_show_xy($pdf,"Number",350,$y);

		$size=10.0;
		pdf_setfont($pdf,$font,$size);
		for($i=1;$i<5;$i++){
			$y-=20;
			pdf_show_xy($pdf,$descr[$i],250,$y);
			pdf_show_xy($pdf,$raters[$i],350,$y);
		}
	}

	writeFooter($pdf,$name,$page);
}

function renderPage2($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Introduction";

	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);

	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"    The proposition at the heart of the Conflict Dynamics Profile (CDP) is that while conflict itself is",50,600);
	pdf_continue_text($pdf,"inevitable, ineffective and harmful responses to conflict can be avoided, and effective and beneficial");
	pdf_continue_text($pdf,"responses to conflict can be learned. What largely separates useful conflict from destructive conflict is");
	pdf_continue_text($pdf,"how the individuals respond when the conflict occurs.");

	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"    One of the most powerful features of the CDP is that it provides a picture of each of the different ways");
	pdf_continue_text($pdf,"of responding to conflict -- constructive and destructive, active and passive. Constructive responses,");
	pdf_continue_text($pdf,"whether occurring at conflict's earliest stages or after it develops, have the effect of not escalating the");
	pdf_continue_text($pdf,"conflict further. They tend to reduce the tension and keep the conflict focused on ideas, rather than");
	pdf_continue_text($pdf,"personalities. Destructive responses, on the other hand, tend to make things worse -- they do little to");
	pdf_continue_text($pdf,"reduce the conflict, and allow it to remain focused on personalities. If conflict can be thought of as a fire,");
	pdf_continue_text($pdf,"then constructive responses help to put the fire out, while destructive responses make the fire worse.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"    It is also possible to think of responses to conflict not simply as constructive or destructive, but as");
	pdf_continue_text($pdf,"differing in terms of how active or passive they are. Active responses are those in which the individual");
	pdf_continue_text($pdf,"takes some overt action in response to the conflict or provocation. Such responses can be either");
	pdf_continue_text($pdf,"constructive or destructive--what makes them active is that they require some overt effort on the part of");
	pdf_continue_text($pdf,"the individual. Passive responses, in contrast, do not require much in the way of effort from the person.");
	pdf_continue_text($pdf,"Because they are passive, they primarily involve the person deciding to not take some kind of action.");
	pdf_continue_text($pdf,"Again, passive responses can be either constructive or destructive--that is, they can make things better");
	pdf_continue_text($pdf,"or they can make things worse.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");


	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Overview of the Group Profile");

	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"    The Group Profile is a broad look at how the organization as a whole views itself, and provides two");
	pdf_continue_text($pdf,"kinds of information. First, it reveals how individuals within the organization tend to view themselves (self-");
	pdf_continue_text($pdf,"data.) Second, it provides some insight as to how this organization's bosses, in general, view their");
	pdf_continue_text($pdf,"subordinates; how direct reports, in general view their supervisors; and how employees, in general, view");
	pdf_continue_text($pdf,"their peers. This information is created by averaging together the scores of all the individual self reports,");
	pdf_continue_text($pdf,"all of the bosses ratings, all of the peer ratings, and all of the direct reports ratings.");

	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_continue_text($pdf,"    The individual CDP Feedback Report, in contrast, presents a relatively focused view of how a single");
	pdf_continue_text($pdf,"employee perceives himself/herself, and how those perceptions match up with the view held by that");
	pdf_continue_text($pdf,"person's boss, peers, and direct reports. While this kind of focused feedback can be extremely useful to");
	pdf_continue_text($pdf,"the individual, it is not equipped to provide a larger picture of the organization as a whole. That is the");
	pdf_continue_text($pdf,"purpose of this Group Profile.");
	writeFooter($pdf,$name,$page);
}

function renderPage3($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-BoldOblique","host",0);

	$size=11.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,"Responses to Conflict",50,660);
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"    The first part of this report describes how, as a group, the individuals taking the CDP see themselves ");
	pdf_continue_text($pdf,"-- and how others see them -- when responding to conflict. Because the Conflict Dynamics Profile measures ");
	pdf_continue_text($pdf,"fifteen different conflict behaviors, self-ratings and the ratings by the bosses, peers, and direct reports");
	pdf_continue_text($pdf,"are compared for each kind of behavior. The Constructive Responses are presented first, followed by the");
	pdf_continue_text($pdf,"Destructive Responses.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Organizational Perspective on Conflict");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"    This second portion of the Group Profile is based on the fact that organizations differ in terms of");
	pdf_continue_text($pdf,"which particular responses to conflict are especially valued and which are especially frowned upon. The");
	pdf_continue_text($pdf,"Organizational Perspective on Conflict describes what the individuals, bosses, peers, and direct reports");
	pdf_continue_text($pdf,"feel are the most \"toxic\" responses to conflict in your organization, that is, the responses which will do");
	pdf_continue_text($pdf,"the most to damage one's career with the organization.");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");

	pdf_setfont($pdf,$bfont,$size);
	pdf_continue_text($pdf,"Interpreting the Group Profile");
	pdf_continue_text($pdf,"   ");
	pdf_continue_text($pdf,"   ");
	pdf_setfont($pdf,$font,$size);
	pdf_continue_text($pdf,"    The way in which the information in this Group Profile is usually presented is through standardized");
	pdf_continue_text($pdf,"scores. This method takes the responses of the individuals, bosses, peers, and direct reports, and");
	pdf_continue_text($pdf,"compares them to the responses of thousands of people who have also completed the Conflict Dynamics");
	pdf_continue_text($pdf,"Profile. By doing so, this provides a standard by which to evaluate conflict behavior. These");
	pdf_continue_text($pdf,"standardized scores take the form of numbers ranging from 0 to 100, although most scores will fall");
	pdf_continue_text($pdf,"between 35 and 65. Whenever such scores are presented, there will also be some indication as to");
	pdf_continue_text($pdf,"whether -- compared to thousands of others -- that score is very low, low, average, high, or very high.");

	writeFooter($pdf,$name,$page);
}

function renderPage4($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="GUIDE TO RESPONSES TO CONFLICT";

	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),700);

	// the bold headings
	$size=13.0;
	pdf_setfont($pdf,$bfont,$size);

	pdf_show_xy($pdf,"Active-Constructive:",50,660);

	// the explanatory text
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Four ways of responding to conflict which require some effort on the part of",195,660);
	pdf_continue_text($pdf,"the individual, and which have the effect of reducing conflict:");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,">  Perspective Taking", 50,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Creating Solutions");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Expressing Emotions");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Reaching Out");

	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Putting oneself in the other person's position and trying to understand that",195,$y);
	pdf_continue_text($pdf,"person's point of view");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Brainstorming with the other person, asking questions, and trying to create");
	pdf_continue_text($pdf,"solutions to the problem");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Talking honestly with the other person and expressing thoughts and feelings");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Reaching out to the other person, making the first move, and trying to make");
	pdf_continue_text($pdf,"amends");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	$size=13.0;
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Passive-Constructive:",50,$y);

	// the explanatory text
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Three ways of responding to conflict which have the effect of dampening",195,$y);
	pdf_continue_text($pdf,"the conflict, or preventing escalation, but which do not require any active");
	pdf_continue_text($pdf,"response from the individual:");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,">  Reflective Thinking", 50,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Delay Responding");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Adapting");

	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Analyzing the situation, weighing the pros and cons, and thinking about the",195,$y);
	pdf_continue_text($pdf,"best response");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Waiting things out, letting matters settle down, or taking a \"time out\" when");
	pdf_continue_text($pdf,"emotions are running high");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Staying flexible, and trying to make the best of the situation");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	$size=13.0;
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Active-Destructive:",50,$y);

	// the explanatory text
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Four ways of responding to conflict which through some effort on the part",195,$y);
	pdf_continue_text($pdf,"of the individual have the effect of escalating the conflict:");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,">  Winning", 50,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Displaying Anger");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Demeaning Others");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Retaliating");

	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Arguing vigorously for your own position and trying to win at all costs",195,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Expressing anger, raising your voice, and using harsh, angry words");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Laughing at the other person, ridiculing the other's ideas, and using sarcasm");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Obstructing the other person, retaliating against the other, and trying to get");
	pdf_continue_text($pdf,"revenge later");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	$size=13.0;
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Passive-Destructive:",50,$y);

	// the explanatory text
	$size=11.0;
	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Four ways of responding to conflict which due to lack of effort or action by", 195, $y);
	pdf_continue_text($pdf,"the individual cause the conflict to either continue, or to be resolved in an");
	pdf_continue_text($pdf,"unsatisfactory manner:");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf," ");

	$y=pdf_get_value($pdf, "texty", 0);
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,">  Avoiding", 50,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Yielding");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Hiding Emotions");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,">  Self-Criticizing");

	pdf_setfont($pdf,$font,$size);
	pdf_show_xy($pdf,"Avoiding or ignoring the other person, acting distant and aloof",195,$y);
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Giving in to the other person in order to avoid further conflict");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Concealing your true emotions even though feeling upset");
	pdf_continue_text($pdf," ");
	pdf_continue_text($pdf,"Replaying the incident over in your mind later, and criticizing yourself for");
	pdf_continue_text($pdf,"not handling it better");

	writeFooter($pdf,$name,$page);
}

// categories 1 & 6 constructive only (1-7)
function getGroupConstructiveResponses($gid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.GID, a.SID from GRPRPTSCORE a, SCALE b where a.GID=$gid and a.SID=b.SID and b.SID<8 and a.CATID in (1,6) order by a.CATID, a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Bosses, Peers and Direct Reports only
function getGroupConstructiveResponsesOthers($gid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.GID, a.SID from GRPRPTSCORE a, SCALE b where a.GID=$gid and a.SID=b.SID and b.SID<8 and a.CATID in (2,3,4) order by a.CATID, a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// categories 1 & 6, destructive only (8-15)
function getGroupDestructiveResponses($gid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.GID, a.SID from GRPRPTSCORE a, SCALE b where a.GID=$gid and a.SID=b.SID and b.SID<16 and b.SID>7 and a.CATID in (1,6) order by a.CATID, a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// Bosses, Peers, reports only
function getGroupDestructiveResponsesOthers($gid){
	$conn=dbConnect();
	$query="select a.AVGSCORE, b.DESCR, a.CATID, a.GID, a.SID from GRPRPTSCORE a, SCALE b where a.GID=$gid and a.SID=b.SID and b.SID<16 and b.SID>7 and a.CATID in (2,3,4) order by a.CATID, a.SID";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

// This is the box that's common for page 5 and 7
function drawBigGraph(&$pdf,$x,$y,$width,$height,$data){
	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=(count($data)/2);
	$ystep=round($height/8);
	$xstep=round($width/($scales+1));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$size=9.0;
	pdf_setfont($pdf,$font,$size);

	// 1. the box outline
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);

	// 2. Horizontal divisions
	$i;
	for($i=round($y+$ystep);$i<($y+round($ystep*8));$i=round($i+$ystep)){
		pdf_moveto($pdf,($x+$xstep),$i);
		pdf_lineto($pdf,($x+$xtsep+$width),$i);
	}
	// little box that juts out on the left
	pdf_moveto($pdf,$x,$y);
	pdf_lineto($pdf,($x+$xstep),$y);
	pdf_moveto($pdf,$x,$y);
	pdf_lineto($pdf,$x,($y+$ystep));
	pdf_moveto($pdf,$x,($y+$ystep));
	pdf_lineto($pdf,($x+$xstep),($y+$ystep));
	// divide the first box
	pdf_moveto($pdf,$x,($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));

	// 3. Vertical divisions
	for($i=($x+$xstep);$i<($x+$width);$i+=$xstep){
		pdf_moveto($pdf,$i,$y);
		pdf_lineto($pdf,$i,($y+(2*$ystep)));
	}

	// draw the legend box
	pdf_rect($pdf,$x+125,$y-50,250,25);
	pdf_stroke($pdf);

	// 4. Vertical Scale
	$vs=array("35","40","45","50","55","60");
	$j=0;
	for($i=($y+(2*$ystep));($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$j++;
	}
	pdf_show_xy($pdf,"65",($x+round(2*$xstep/3)),$y+$height-2);

	$vs=array("Very","Low","","","High","Very");
	$vs1=array("Low","","","","","High");
	$j=0;
	for($i=($y+round(2.5*$ystep));($i<$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],$x,$i);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}
	pdf_show_xy($pdf,"Average",$x,$y+(5*$ystep));

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_boxed($pdf,"Self",$x,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
	pdf_show_boxed($pdf,"Others",$x,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");

	// Legend
	pdf_show_xy($pdf,"Self",$x+210,$y-round(0.75*$ystep));
	pdf_show_xy($pdf,"Others",$x+310,$y-round(0.75*$ystep));

	// 5. Draw the scale
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$sTxt=$data[$i][1];
		if("Self-Criticizing"==$sTxt){
			$sTxt="Self- Criticizing";
		}
		pdf_show_boxed($pdf,$sTxt,$j,($y+$ystep),round(0.95*$xstep),round(0.75*$ystep),"center","");
		$j+=$xstep;
	}

	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		pdf_show_boxed($pdf,round($data[$i][0]),$j,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
		pdf_show_boxed($pdf,round($data[($i+$scales)][0]),$j,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");
		$j+=$xstep;
	}

	// 7. Draw the graph for Self
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$prevval=$va;
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			// where were we before?
			$n=($data[$i-1][0]-35);
			$n=$n<0?-10:10;
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5))+$n);
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			// where are we going?
			$n=($data[$i][0]-35);
			if($n<0||$n>30){
				$n=$n<0?-10:10;
			}
			else{
				$n=0;
			}
			pdf_lineto($pdf,$j,$yc+$n);
		}
	}

	// Legend
	pdf_moveto($pdf,$x+150,$y-round(0.7*$ystep));
	pdf_lineto($pdf,$x+200,$y-round(0.7*$ystep));
	pdf_moveto($pdf,$x+250,$y-round(0.7*$ystep));
	pdf_lineto($pdf,$x+300,$y-round(0.7*$ystep));
	pdf_stroke($pdf);


	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	// Legend
	pdf_circle($pdf,$x+175,$y-round(0.7*$ystep),3.5);
	pdf_fill_stroke($pdf);

	// 8. Draw the graph for Others
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=round($data[$scales][0]);
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i+$scales][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			// where were we before?
			$n=($data[$i+$scales-1][0]-35);
			$n=$n<0?-10:10;
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5))+$n);
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			// where are we going?
			$n=($data[$i+$scales][0]-35);
			if($n<0||$n>30){
				$n=$n<0?-10:10;
			}
			else{
				$n=0;
			}
			pdf_lineto($pdf,$j,$yc+$n);
		}
	}

	pdf_stroke($pdf);

	pdf_setcolor($pdf,'both','rgb',0.3,0.6,0.3,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[$scales][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_rect($pdf,$j-4.5,$yc-4.5,9,9);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i+$scales][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_rect($pdf,($j-4.5),($yc-4.5),9,9);
		}
	}

	// Legend
	pdf_rect($pdf,$x+275,($y-round(0.7*$ystep)-2),6,6);
	pdf_fill_stroke($pdf);
}

function renderPage5($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Constructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Higher numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getGroupConstructiveResponses($gid);
	if($rows){
		drawBigGraph($pdf,50,150,500,450,$rows);
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeFooter($pdf,$name,$page);
}

function renderPage7($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Destructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Lower numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getGroupDestructiveResponses($gid);
	if($rows){
		drawBigGraph($pdf,50,150,500,450,$rows);
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeFooter($pdf,$name,$page);
}


// This is the box that's common for page 6 and 8
function drawBigGraphTriple(&$pdf,$x,$y,$width,$height,$data){
	// 0. get the metrics
	// we need these many horizontal divisions
	$scales=(count($data)/3);
	$ystep=round($height/8);
	$xstep=round($width/($scales+1));

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$size=9.0;
	pdf_setfont($pdf,$font,$size);

	// 1. the box outline
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,($x+$xstep),$y,($width-$xstep),$height);

	// 2. Horizontal divisions
	$i;
	for($i=round($y+$ystep);$i<($y+round($ystep*8));$i=round($i+$ystep)){
		pdf_moveto($pdf,($x+$xstep),$i);
		pdf_lineto($pdf,($x+$xtsep+$width),$i);
	}
	// little box that juts out on the left
	pdf_moveto($pdf,$x,$y);
	pdf_lineto($pdf,($x+$xstep),$y);
	pdf_moveto($pdf,$x,$y-(0.5*$ystep));
	pdf_lineto($pdf,$x,($y+$ystep));
	pdf_moveto($pdf,$x,($y+$ystep));
	pdf_lineto($pdf,($x+$xstep),($y+$ystep));
	// divide the first box
	pdf_moveto($pdf,$x,($y+($ystep/2)));
	pdf_lineto($pdf,($x+$width),($y+($ystep/2)));

	// 3. Vertical divisions
	for($i=($x+$xstep);$i<($x+$width);$i+=$xstep){
		pdf_moveto($pdf,$i,$y-(0.5*$ystep));
		pdf_lineto($pdf,$i,($y+(2*$ystep)));
	}
	pdf_moveto($pdf,$x+$width,$y-(0.5*$ystep));
	pdf_lineto($pdf,$x+$width,$y);

	// Bottom of the box
	pdf_moveto($pdf,$x,$y-(0.5*$ystep));
	pdf_lineto($pdf,$x+$width,$y-(0.5*$ystep));

	// draw the legend box
	pdf_rect($pdf,$x+75,$y-(1.25*$ystep),350,25);
	pdf_stroke($pdf);

	// 4. Vertical Scale
	$vs=array("35","40","45","50","55","60");
	$j=0;
	for($i=($y+(2*$ystep));($i<=$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],($x+round(2*$xstep/3)),$i);
		$j++;
	}
	pdf_show_xy($pdf,"65",($x+round(2*$xstep/3)),$y+$height-2);

	$vs=array("Very","Low","","","High","Very");
	$vs1=array("Low","","","","","High");
	$j=0;
	for($i=($y+round(2.5*$ystep));($i<$y+$height);$i+=$ystep){
		pdf_show_xy($pdf,$vs[$j],$x,$i);
		pdf_continue_text($pdf,$vs1[$j]);
		$j++;
	}
	pdf_show_xy($pdf,"Average",$x,$y+(5*$ystep));

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_boxed($pdf,"Bosses",$x,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
	pdf_show_boxed($pdf,"Peers",$x,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");
	pdf_show_boxed($pdf,"Reports",$x,($y-(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");

	// Legend
	pdf_show_xy($pdf,"Bosses",$x+150,$y-(1.05*$ystep)-2);
	pdf_show_xy($pdf,"Peers",$x+250,$y-(1.05*$ystep)-2);
	pdf_show_xy($pdf,"Reports",$x+350,$y-(1.05*$ystep)-2);

	// 5. Draw the scale
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		$sTxt=$data[$i][1];
		if("Self-Criticizing"==$sTxt){
			$sTxt="Self- Criticizing";
		}
		pdf_show_boxed($pdf,$sTxt,$j,($y+$ystep),round(0.95*$xstep),round(0.75*$ystep),"center","");
		$j+=$xstep;
	}

	// 6. Draw the numerical values
	$j=$x+$xstep;
	for($i=0;$i<$scales;$i++){
		if(round($data[$i][0])==0){
		  $bossData = "N/A";
		}
		else{
		  $bossData = round($data[$i][0]);
		}
		pdf_show_boxed($pdf,$bossData,$j,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
		//pdf_show_boxed($pdf,round($data[$i][0]),$j,($y+round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
		pdf_show_boxed($pdf,round($data[($i+$scales)][0]),$j,$y,round(0.95*$xstep),round(0.375*$ystep),"center","");
		pdf_show_boxed($pdf,round($data[($i+(2*$scales))][0]),$j,($y-round(0.5*$ystep)),round(0.95*$xstep),round(0.375*$ystep),"center","");
		$j+=$xstep;
	}

	// 7. Draw the graph for Bosses
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$prevval=$va;
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			// where were we before?
			$n=($data[$i-1][0]-35);
			$n=$n<0?-10:10;
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5))+$n);
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			// where are we going?
			$n=($data[$i][0]-35);
			if($n<0||$n>30){
				$n=$n<0?-10:10;
			}
			else{
				$n=0;
			}
			pdf_lineto($pdf,$j,$yc+$n);
		}
	}

	// Legend
	pdf_moveto($pdf,$x+90,$y-round(1.05*$ystep));
	pdf_lineto($pdf,$x+135,$y-round(1.05*$ystep));
	pdf_moveto($pdf,$x+200,$y-round(1.05*$ystep));
	pdf_lineto($pdf,$x+245,$y-round(1.05*$ystep));
	pdf_moveto($pdf,$x+300,$y-round(1.05*$ystep));
	pdf_lineto($pdf,$x+345,$y-round(1.05*$ystep));
	pdf_stroke($pdf);


	pdf_setcolor($pdf,'both','rgb',0.4,0.4,0.8,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[0][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}
	// Legend
	pdf_circle($pdf,$x+115,$y-(1.05*$ystep),3.5);
	pdf_fill_stroke($pdf);

	// 8. Draw the graph for Peers
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=round($data[$scales][0]);
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i+$scales][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			// where were we before?
			$n=($data[$i+$scales-1][0]-35);
			$n=$n<0?-10:10;
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5))+$n);
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			// where are we going?
			$n=($data[$i+$scales][0]-35);
			if($n<0||$n>30){
				$n=$n<0?-10:10;
			}
			else{
				$n=0;
			}
			pdf_lineto($pdf,$j,$yc+$n);
		}
	}

	pdf_stroke($pdf);

	pdf_setcolor($pdf,'both','rgb',0.3,0.6,0.3,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[$scales][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_rect($pdf,$j-4.5,$yc-4.5,9,9);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i+$scales][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_rect($pdf,($j-4.5),($yc-4.5),9,9);
		}
	}

	// Legend
	pdf_rect($pdf,$x+220,($y-(1.07*$ystep)-2),6,6);
	pdf_fill_stroke($pdf);

	// 9. Draw the graph for Others
	$clip1=false;
	$clip2=false;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1.5);
	$j=$x+round(1.5*$xstep);
	$val=round($data[2*$scales][0]);
	$val-=35;
	if($val<0){
		$clip2=true;
		$val=0;
	}
	elseif($val>30){
		$clip2=true;
		$val=30;
	}
	// We're not clipped, move to the point
	if(!$clip2){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_moveto($pdf,$j,$yc);
	}
	for($i=1;$i<$scales;$i++){
		// Save previous valuae and clip status
		$preval=$val;
		$clip1=$clip2;
		$val=round($data[$i+(2*$scales)][0]);
		$val-=35;
		if($val<0){
			$clip2=true;
			$val=0;
		}
		elseif($val>30){
			$clip2=true;
			$val=30;
		}
		else{
			$clip2=false;
		}
		// We now know if we're within the graph etc
		// If we were outside the graph, move in w/o drawing
		if($clip1){
			// where were we before?
			$n=($data[$i+(2*$scales)-1][0]-35);
			$n=$n<0?-10:10;
			pdf_moveto($pdf,$j,$y+(2*$ystep)+($preval*($ystep/5))+$n);
		}

		$yc=$y+(2*$ystep)+($val*($ystep/5));
		$j+=$xstep;
		// If we're inside, draw
		if(!$clip2){
			pdf_lineto($pdf,$j,$yc);
		}
		// If we were inside and are moving out, draw, except on the edge
		elseif(!$clip1 && $clip2 && $preval!=$val){
			// where are we going?
			$n=($data[$i+(2*$scales)][0]-35);
			if($n<0||$n>30){
				$n=$n<0?-10:10;
			}
			else{
				$n=0;
			}
			pdf_lineto($pdf,$j,$yc+$n);
		}
	}

	pdf_stroke($pdf);

	pdf_setcolor($pdf,'both','rgb',0.6,0.3,0.3,0);
	pdf_setlinewidth($pdf,1);
	$j=$x+round(1.5*$xstep);
	$val=round($data[2*$scales][0]);
	$val-=35;
	if($val>=0&&$val<=30){
		$yc=$y+(2*$ystep)+($val*($ystep/5));
		pdf_circle($pdf,$j,$yc,5);
	}
	for($i=1;$i<$scales;$i++){
		$j+=$xstep;
		$val=round($data[$i+(2*$scales)][0]);
		$val-=35;
		if($val>=0&&$val<=30){
			$yc=$y+(2*$ystep)+($val*($ystep/5));
			pdf_circle($pdf,$j,$yc,5);
		}
	}

	// Legend
	pdf_circle($pdf,$x+325,$y-(1.05*$ystep),3.5);
	pdf_fill_stroke($pdf);
}

function renderPage6($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Constructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Higher numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getGroupConstructiveResponsesOthers($gid);
//	if($rows){
//		drawBigGraphTriple($pdf,50,150,500,450,$rows);
//	}

// new...
	if($rows){
	  if(count($rows) != 21){
	    // fix for zero bosses
	    // create temporary filler array for missing boss data
	    $str = array("Perspective Taking","Creating Solutions","Expressing Emotions","Reaching Out",
             "Reflective Thinking","Delay Responding","Adapting");

      for($x=0; $x < count($str); $x++){
        $temp[$x][0]="N/A";
        $temp[$x][1]=$str[$x];;
      }
      // add to beginning of existing data
      $rows = array_merge($temp,$rows);
	  }
		drawBigGraphTriple($pdf,50,150,500,450,$rows);
	}
	else{
		pdf_continue_text($pdf,"Can't graph constructive responses");
	}

	writeFooter($pdf,$name,$page);
}

function renderPage8($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=24.0;
	pdf_setfont($pdf,$font,$size);
	$title="Destructive Responses";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=12.0;
	pdf_setfont($pdf,$font,$size);
	$title="(Lower numbers are more desirable)";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),660);


	$rows=getGroupDestructiveResponsesOthers($gid);
//	if($rows){
//		drawBigGraphTriple($pdf,50,150,500,450,$rows);
//	}

//new...
	if($rows){
	  if(count($rows) != 24){
	    // fix for zero bosses
	    // create temporary filler array for missing boss data
	    $str = array("Winning","Displaying Anger","Demeaning Others","Retaliating",
             "Avoiding","Yielding","Hiding Emotions","Self-Criticizing");

      for($x=0; $x < count($str); $x++){
        $temp[$x][0]="N/A";
        $temp[$x][1]=$str[$x];;
      }
      $rows = array_merge($temp,$rows);
	  }
		drawBigGraphTriple($pdf,50,150,500,450,$rows);
	}
	else{
		pdf_continue_text($pdf,"Can't graph destructive responses");
	}

	writeFooter($pdf,$name,$page);
}

function drawOrgGraph(&$pdf,$x,$y,$width,$height,$data,$gid){
	// 0. get the metrics
	$ystep=$height/17;
	$xstep=$width/10;
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);
	pdf_setfont($pdf,$bfont,10.0);

	// The item text
	$txt=array();
	$txt[0]="Being insensitive to the other person's point of view";
	$txt[1]="Failing to work with the other person to create solutions";
	$txt[2]="Failing to communicate honestly with the other person by expressing thoughts and feelings";
	$txt[3]="Ignoring opportunities to reach out to the other person and repair things";
	$txt[4]="Reacting impulsively rather than analyzing the situation and thinking about the best response";
	$txt[5]="Responding immediately to conflict rather than letting emotions settle down";
	$txt[6]="Failing to adapt and be flexible during conflict situations";
	$txt[7]="Arguing vigorously for one's own position, trying to win at all costs";
	$txt[8]="Expressing anger, raising one's voice, using harsh, angry words";
	$txt[9]="Laughing at the other person, ridiculing the other, using sarcasm";
	$txt[10]="Obstructing or retaliating against the other, trying to get revenge later";
	$txt[11]="Avoiding or ignoring the other person, acting distant and aloof";
	$txt[12]="Giving in to the other person in order to avoid further conflict";
	$txt[13]="Concealing one's true emotions even though feeling upset";
	$txt[14]="Replaying the incident over in one's mind, criticizing oneself for not handling it better";

	// 1. How many bosses, peers and DRs do we have - all depends on that
	$bossNo=0;
	$peerNo=0;
	$drNo=0;
	$raters=getNumRaters($gid);
	if($raters){
		foreach($raters as $rater){
			if(3==$rater[0]){
				$peerNo=$rater[1];
			}
			elseif(4==$rater[0]){
				$drNo=$rater[1];
			}
			elseif(2==$rater[0]){
				$bossNo=$rater[1];
			}
		}
	}

	// 2. Examine the data and put it in appropriate places
	$selfData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$bossData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$peerData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$drData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	$comboData=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

	$hasPeers=false;
	$hasDRs=false;
	$doCombo=false;
	foreach($data as $row){
		// the rows are returned in this order: a.ITEMID,a.SCORE,a.CATID
		$item=$row[0];
		$score=$row[1];
		$cat=$row[2];
		if(1==$cat){
			$selfData[$item-100]=$score;
		}
		elseif(2==$cat){
			$bossData[$item-64]=$score;
		}
		elseif(3==$cat){
			$peerData[$item-64]=$score;
			$hasPeers=true;
		}
		elseif(4==$cat){
			$drData[$item-64]=$score;
			$hasDRs=true;
		}
		elseif(5==$cat){
			$comboData[$item-64]=$score;
			$doCombo=true;
			$hasPeers=false;
			$hasDRs=false;
		}
	}

	// 3. Draw the outline - depends on how many rows we're to display
	// we'll always display Self and Boss even if Boss has no data at all
	$columns=2;
	if($hasPeers){
		$columns++;
	}
	if($hasDRs){
		$columns++;
	}
	if($doCombo){
		$columns++;
		if($drNo==0||$peerNo==0){
			$columns++;
		}
	}

	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	pdf_setlinewidth($pdf,1);
	pdf_rect($pdf,$x,$y+$ystep,(6+$columns)*$xstep,$height-$ystep);
	// 1-3 vertical lines
	for($i=6;$i<10;$i++){
		pdf_moveto($pdf,$x+($i*$xstep),$y+$height);
		pdf_lineto($pdf,$x+($i*$xstep),$y+$ystep);
	}
	// horizontal lines
	for($i=1;$i<16;$i++){
		pdf_moveto($pdf,$x,$y+$height-($i*$ystep));
		pdf_lineto($pdf,$x+((6+$columns)*$xstep),$y+$height-($i*$ystep));
	}
	pdf_stroke($pdf);

	// 4. Draw static text
	// categories
	pdf_show_xy($pdf,"Responses to Conflict",$x+5,$y+$height-round($ystep/2));
	pdf_show_boxed($pdf,"Self",$x+(6*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	pdf_show_boxed($pdf,"Bosses",$x+(7*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");

	if(!$doCombo){
		pdf_show_boxed($pdf,"Peers",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
		pdf_show_boxed($pdf,"Direct Reports",$x+(9*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
	}

	if($doCombo){
		if(0==$peerNo||0==$drNo){
			pdf_show_boxed($pdf,"Peers",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
			pdf_show_boxed($pdf,"Direct Reports",$x+(9*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
		}
		else{
			pdf_show_boxed($pdf,"Peers/ Reports*",$x+(8*$xstep),$y+$height-$ystep,$xstep,$ystep,"center","");
		}
	}

	// Item text
	pdf_setfont($pdf,$font,10.0);
	for($i=0;$i<16;$i++){
		pdf_show_boxed($pdf,$txt[$i],$x+3,$y+$height-round(($i+2)*$ystep),round(6*$xstep),$ystep,"left","");
	}

	// 5. Show data
	// The darned things are displayed out of order for no apparent reason
	// Thus we do this
	// Start with the last 7
	$hfont=pdf_findfont($pdf,"Times-Bold","host",0);
	pdf_setfont($pdf,$hfont,14.0);
	for($i=8;$i<15;$i++){
		// First do self
		$val=$selfData[$i];
		$severity="";
		if($val>=2.0){
			$severity="M";
		}
		if($val>=2.5){
			$severity="S";
		}
		pdf_show_boxed($pdf,$severity,$x+round(6*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");

		// Boss
		$val=$bossData[$i];
		$severity="";
		if($val>=2.0){
			$severity="M";
		}
		if($val>=2.5){
			$severity="S";
		}

		pdf_show_boxed($pdf,$severity,$x+round(7*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");

		// Peers
		if($hasPeers){
			$val=$peerData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>=2.5){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(8*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Direct Reports
		if($hasPeers){
			$val=$drData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>=2.5){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(9*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Combined Peers/DRs
		if($doCombo){
			$whichCol=8;
			if($peerNo==0){
				$whichCol=9;
			}
			$val=$comboData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>=2.5){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round($whichCol*$xstep),$y+$height-round(($i-8+2)*$ystep),$xstep,$ystep,"center","");
		}
	}

	// Then do the first 8
	for($i=0;$i<8;$i++){
		// First do self
		$val=$selfData[$i];
		$severity="";
		if($val>=2.0){
			$severity="M";
		}
		if($val>=2.5){
			$severity="S";
		}
		pdf_show_boxed($pdf,$severity,$x+round(6*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");

		// Boss
		$val=$bossData[$i];
		$severity="";
		if($val>=2.0){
			$severity="M";
		}
		if($val>=2.5){
			$severity="S";
		}
		pdf_show_boxed($pdf,$severity,$x+round(7*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");

		// Peers
		if($hasPeers){
			$val=$peerData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>=2.5){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(8*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Direct Reports
		if($hasPeers){
			$val=$drData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>=2.5){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round(9*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}

		// Combined Peers/DRs
		if($doCombo){
			$whichCol=8;
			if($peerNo==0){
				$whichCol=9;
			}
			$val=$comboData[$i];
			$severity="";
			if($val>=2.0){
				$severity="M";
			}
			if($val>=2.5){
				$severity="S";
			}
			pdf_show_boxed($pdf,$severity,$x+round($whichCol*$xstep),$y+$height-round(($i+7+2)*$ystep),$xstep,$ystep,"center","");
		}
	}

	// footer
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,"\"severe negative impact\" reflects a mean response of 2.5 or higher, and \"moderate negative impact\"",$x,$y+round((2*$ystep)/3));
	pdf_continue_text($pdf,"reflects a mean response between 2.0 and 2.49. (on a 1 - 3 scale)");

}

function getGroupOrgPersp($gid){
	$conn=dbConnect();
	$query="select a.ITEMID,a.SCORE,a.CATID from GRPORGPERSP a where a.GID=$gid order by a.CATID asc, a.ITEMID asc";
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function renderPage9($gid,&$pdf,&$page){
	$name=writeHeader($gid,$pdf);

	$font=pdf_findfont($pdf,"Helvetica","host",0);
	$bfont=pdf_findfont($pdf,"Helvetica-Bold","host",0);

	// draw title
	$size=16.0;
	pdf_setfont($pdf,$font,$size);
	$title="Organizational Perspective on Conflict";
	pdf_show_xy($pdf,$title,calcCenterStartPos($pdf,$title,$font,$size),680);

	$size=10.0;
	pdf_setfont($pdf,$font,$size);

	pdf_show_xy($pdf,"Everyone in the group (individuals, bosses, peers and direct reports) were asked to indicate which",75,660);
	pdf_continue_text($pdf,"kinds of responses to conflict within their organization have the most negative effect on a person's");
	pdf_continue_text($pdf,"career -- that is, the responses to conflict which are most frowned upon within their organization. The");
	pdf_continue_text($pdf,"grid below displays what the group believes are the behaviors which, in their organization, have either a");
	pdf_continue_text($pdf,"severe, or moderate negative impact on one's career.");

	pdf_continue_text($pdf," ");
	pdf_setfont($pdf,$bfont,$size);
	pdf_show_xy($pdf,"Behaviors Seen As Having Severe(S)",350,575);
	pdf_continue_text($pdf,"or Moderate(M) Impact on Careers");

	$rows=getGroupOrgPersp($gid);
	if($rows){
		drawOrgGraph(&$pdf,75,100,450,450,$rows,$gid);
	}
	else{
		pdf_show_xy($pdf,"Cannot graph organizational perspective",200,500);
	}

	writeFooter($pdf,$name,$page);
}

function writeHeader($gid,&$pdf){
	$name=getGroupName($gid);
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,750,515,15);
	pdf_fill_stroke($pdf);
	// Note: (R) is hex AE, so well use sprintf to format properly
	$what="Conflict Dynamics Profile ".sprintf("%c",0xae);
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$what,calcCenterStartPos($pdf,$what,$font,10.0),754);

	return $name;
}

function writeFooter(&$pdf,$name,&$page){
	pdf_setcolor($pdf,'both','rgb',R_,G_,B_,0);
	pdf_rect($pdf,50,35,515,15);
	pdf_fill_stroke($pdf);
	$rptdt=date('D M d, Y');
	$what="Page ".$page;
	pdf_setcolor($pdf,'both','rgb',0.0,0.0,0.0,0);
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,10.0);
	pdf_show_xy($pdf,$rptdt,55,39);
	pdf_show_xy($pdf,$what,475,39);
	pdf_end_page($pdf);
	// Increment the page number
	$page+=1;
}
?>
