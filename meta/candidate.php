<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" candidates
*
* 11-21-2005: Mulitlingual support
*=============================================================================*/
require_once 'dbfns.php';
require_once 'mailfns.php';

// Insert a new candidate
function candInsert($candData){
	$conn=dbConnect();
	// This looks funny, but is correct since self is also a RATER
	$i=getKey("RATER","RID");
	// Insert the CANDIDATE row
	//$query="insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],'$candData[1]','$candData[2]','$candData[3]','N','N','$candData[4]','$candData[5]')";
	// wbs 12/7/2006
	$query="insert into CANDIDATE (CID,PID,FNAME,LNAME,EMAIL,EXPIRED,ARCHFLAG,SELF,OTH) values ( $i,$candData[0],".quote_smart($candData[1]).",".quote_smart($candData[2]).",".quote_smart($candData[3]).",'N','N','$candData[4]','$candData[5]')";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// Insert the CANDCONS row
	$query="insert into CANDCONS (CID,CONID) values ( $i,$candData[6])";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// Language support: Insert the RATERLANG row
	$query="insert into RATERLANG (RID,PID,LID) values ( $i,$candData[0],$candData[7])";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}

	return insertSelfRater($i);
}

// insert the self rater
function insertSelfRater($cid){
	$conn=dbConnect();
	$query="insert into RATER (RID,CID,CATID,FNAM,LNAM,EMAIL,STARTDT,ENDDT,EXPIRED) select CID,CID,1,FNAME,LNAME,EMAIL,STARTDT,ENDDT,EXPIRED from CANDIDATE where CID=$cid";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return insertSelfSurvey($cid);
}

// insert the self survey
function insertSelfSurvey($cid){
	$conn=dbConnect();
	$query="insert into RATERINSTR (RID,TID,EXPIRED) select RID,1,EXPIRED from RATER where CID=$cid and CATID=1";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return $cid;
}

// save candidate info
function saveCandidate($candData){
	$conn=dbConnect();
	// Save changes to CANDIDATE row
	//$query="update CANDIDATE  set FNAME='$candData[1]',LNAME='$candData[2]',EMAIL='$candData[3]' where CID=$candData[0]";
	// wbs 12/7/2006
	$query="update CANDIDATE  set FNAME=".quote_smart($candData[1]).",LNAME=".quote_smart($candData[2]).",EMAIL=".quote_smart($candData[3])." where CID=$candData[0]";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	// but remember a CANDIDATE is also a RATER - best to update that row as well
	$query="update RATER set FNAM='$candData[1]',LNAM='$candData[2]',EMAIL='$candData[3]' where CID=$candData[0] and RID=$candData[0]";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}

	// and finally udate the langauge
	$query="update RATERLANG set LID=$candData[4] where RID=$candData[0]";
	if(!mysql_query($query)){
	    echo $query."<br>";
	    return false;
	}
	return true;
}

// check that email is unique within a program
function checkCandEmail($email,$pid,$cid=NULL){
	if("0"==$pid){
		// A self-only candidate
		// we can re-use the email as much as we like
		return true;
	}
//
// 01-25-2005: Patti Viscomi wants this to be disabled while testing
//
return true;

	$conn=dbConnect();
	$query="select EMAIL from CANDIDATE where PID=$pid and EMAIL='$email'";
	if(!is_null($cid)){
	    $query=$query." and CID<>$cid";
	}
	//echo $query;
	$rs=mysql_query($query);
	if(!$rs){
	    return false;
	}
	return !mysql_fetch_row($rs);
}

// return data for a single candidate
function getCandInfo($cid){
	$conn=dbConnect();
	// added language support
	$query="select a.CID,a.FNAME,a.LNAME,a.EMAIL,a.EXPIRED,a.ARCHFLAG,a.SELF,a.OTH,a.STARTDT,a.ENDDT,c.LID,c.DESCR from CANDIDATE a,RATERLANG b, LANG c where a.CID=$cid and a.CID=b.RID and b.LID=c.LID";
	$rs=mysql_query($query);
	if(!$rs){
	    echo $query."<br>";
	    return false;
	}
	return mysql_fetch_array($rs);
}

// return a list of all candidates for a program
// added language support
function candidateList($pid,$arch="N"){
	$conn=dbConnect();
	$query="select a.CID,a.FNAME,a.LNAME,a.EMAIL,a.EXPIRED,a.ARCHFLAG,a.SELF,a.OTH,a.STARTDT,a.ENDDT,c.LID,c.DESCR from CANDIDATE a,RATERLANG b,LANG c where a.PID=$pid and a.ARCHFLAG='$arch' and a.CID=b.RID and b.LID=c.LID order by a.LNAME";
//echo $query;
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// render a list of all candidates for a program
// Added language support
function listCandidates($pid,$frm,$tid="1"){
	$rs=candidateList($pid);
	foreach($rs as $row){
		$useUrl="../usr/conshome.php";
		if($tid=="3"){
			// jump straight into the self only survey in a separate window
//			$useUrl="../assess/selfonly.php';$frm.target='selfonlysurvey";
			$useUrl="../assess/conshome.php";
		}
	    echo "<tr>";
	    echo "<td><small>$row[2], $row[1]</small></td>";
	    echo "<td><small>$row[3]</small></td><td><small>$row[0]</small></td>";
	    echo "<td><small>".utf8_decode($row[11])."</small></td>";
	    echo "<td>";
	    echo "<input type='button' value='Edit' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='edit';$frm.action='canddetail.php';$frm.submit();\">";
	    echo "<input type='button' value='Delete' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='del';verifyDelete();\">";
	    echo "<input type='button' value='Use' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.pin.value='$row[0]';$frm.uid.value='$row[3]';$frm.action='$useUrl';$frm.submit();\">";
	    echo "</td></tr>";
	}
}

// return a list of all self-only candidates for a consultant
// Note: we drive off the RATER table
function selfOnlyCandidateList($conid,$arch="N"){
	$conn=dbConnect();
	$query="select a.RID,a.FNAM,a.LNAM,a.EMAIL,a.EXPIRED,IFNULL(a.STARTDT,'Pending'),IFNULL(a.ENDDT,'Incomplete') from RATER a,CANDCONS b, CANDIDATE c  where a.RID=b.CID and a.RID=c.CID and b.CONID=$conid and ARCHFLAG='$arch' and OTH='N' order by LNAM";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// render a list of all self-only candidates
function listSelfOnlyCandidates($conid,$frm){
	$rs=selfOnlyCandidateList($conid);
	foreach($rs as $row){
	    echo "<tr>";
	    echo "<td><small>$row[2], $row[1]</small></td>";
	    echo "<td><small>$row[3]</small></td><td><small>$row[0]</small></td>";
		echo "<td><u>Start date:</u> $row[5]<br><u>Complete date:</u> $row[6]</td>";
	    echo "<td>";
	    echo "<input type='button' value='Edit' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='edit';$frm.action='selfcanddetail.php';$frm.submit();\">";
	    echo "<input type='button' value='Email' onClick=\"javascript:$frm.cid.value='$row[0]';$frm.what.value='email';$frm.submit();\">";
		if($row[6]!="Incomplete"){
			//-- we let the consultant generate the report whenever it suits her
			//-- this requires that we score and consume licenses accordingly
			//-- and that we must make sure she has enough licenses to do it
			echo "<br>";
			echo "<a href='showselfonlyreport.php?cid=$row[0]' target='selfonlyreport'>View report</a>";
		}
	    echo "</td></tr>";
	}
}

// render a  mail list of all candidates for a program
function candidateMailList($pid){
	$rs=candidateList($pid);
	$count=0;
	foreach($rs as $row){
	    echo "<tr>";
	    echo "<td><small>$row[2], $row[1]</small></td>";
	    echo "<td><small>$row[3]</small></td><td><small>";
	    //echo "<input type='checkbox' name='rcp[]' value='$row[0]'>";
	    echo "Yes <input type='radio' name='rcp$count' value='$row[0]'>";
	    echo "No <input type='radio' name='rcp$count' value='N' checked>";
	    echo "</small></td>";
	    echo "</tr>";
	    $count++;
	}
	echo "<input type='hidden' name='count' value='$count'>";
}

// deletes a consumed license for a candidate
function deleteCandLicense($cid,$conid){
	$conn=dbConnect();
	$query="delete from CANDLIC where CONID=$conid and CID=$cid";
	//echo $query."<br>";
	return mysql_query($query);
}

// deletes a candidate that hasn't used his link yet
function deleteCandidate($cid,$conid){
	$conn=dbConnect();
	// we should get rid of all the raters and their answers, too.
	// since this is 3.3 we cand do the multiple table deletes, so we have to do it the hard way
	if(!deleteRaterResponseForCandidate($cid)){
	    return false;
	}
	$query="delete from RATER where CID=$cid";
	if(!mysql_query($query)){
	    return false;
	}
	$query="delete from CANDCONS where CID=$cid";
	if(!mysql_query($query)){
	    return false;
	}

	// delete the language row
	$query="delete from RATERLANG where RID=$cid";
	if(!mysql_query($query)){
	    return false;
	}

	// Send an email to the candidate before deleting
	$query="select EMAIL from CONSULTANT where CONID=$conid";

	//echo $query."<br>";
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$from=$row[0];
		$query="select a.FNAME, a.LNAME, a.EMAIL, b.DESCR from CANDIDATE a, PROGRAM b where a.PID=b.PID and a.CID=$cid";
		//echo $query."<br>";
		$rs=mysql_query($query);
		if($rs){
			$row=mysql_fetch_row($rs);
			$body="You have been removed from the Conflict Dynamics Profile.".getDisclaimer();
			sendGenericMail($row[2],$from,"Conflict Dynamics Profile Deletion",$body,$row[0]." ".$row[1]);
		}
	}

	$query="delete from CANDIDATE where CID=$cid ";
	//echo $query."<br>";
	return mysql_query($query);

	// License will most probably be consumed when the report is generated
	//return mysql_query($query)?deleteCandLicense($cid,$conid):false;
}

// a clunky 3.3 way to delete the responses and comments
function deleteRaterResponseForCandidate($cid){
	$conn=dbConnect();
	$query="select RID from RATER where CID=$cid";
	$rs=mysql_query($query);
	if(!$rs){
	    return false;
	}
	$rows=mysql_fetch_array($rs);
	foreach ($rows as $row){
	    $query="delete from RATERRESP where RID=$row[0]";
	    if(!mysql_query($query)){
	        return false;
	    }
	    $query="delete from RATERCMT where RID=$row[0]";
	    if(!mysql_query($query)){
		return false;
	    }
	    $query="delete from RATERINSTR where RID=$row[0]";
	    if(!mysql_query($query)){
		return false;
	    }
		// delete the language row as well
	    $query="delete from RATERLANG where RID=$row[0]";
	    if(!mysql_query($query)){
		return false;
	    }
	}
	return true;
}

function sendCandidateEmail($rcp,$type,$mailbody,$conid){
	// get a comma separated list of CID
	$rcps=implode($rcp,",");
	if(!$rcps){
	    return false;
	}
	// use this email address as sender
	$conn=dbConnect();
	$query="select EMAIL from CONSULTANT where CONID=$conid";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	$sender=$rw[0];

	// get all the recipients
	$rc=array();
	$query="select a.EMAIL,a.FNAME,a.LNAME,a.CID,b.LID from CANDIDATE a, RATERLANG b where a.CID in ($rcps) and a.CID=b.RID";
	//echo "<br>".$query."<br>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	$rw=mysql_fetch_row($rs);
	while($rw){
	    $rc[]=$rw[0];
	    // build an email and send it
	    $to=$rw[0];
	    $subject="Conflict Dynamics Profile";
	    $name=$rw[1]." ".$rw[2];
	    $cid=$rw[3];
		$lid=$rw[4];
	    $body="";
	    if("I"==$type){
			// Initial 360
			$body=initialEmail($cid,$lid);
	    }
	    elseif("R"==$type){
			// Reminder 360
			$body=reminderEmail($cid,$lid);
	    }
	    elseif("C"==$type){
			// Custom 360
			$body=customEmail($mailbody,$cid,$lid);
	    }
	    elseif("S"==$type||"SI"==$type){
			// Initial S.O.
			$body=selfOnlyEmail($cid,$lid);
	    }
	    elseif("SR"==$type){
			// Reminder S.O.
			$body=soReminderEmail($cid,$lid);
	    }
	    elseif("SC"==$type){
			// Custom S.O.
			$body=soCustomEmail($mailbody,$cid,$lid);
	    }
	    sendGenericMail($to,$sender,$subject,$body,$name,$lid);
	    $rw=mysql_fetch_row($rs);
	}
	// Ok, now they have emails
	mysql_query("update CANDIDATE set SENT='Y' where CID in ($rcps)");
	return implode($rc,",");
}

// This is the Initial email for self-only
// We've added multilingual support
function selfOnlyEmail($cid,$lid){
	if("8"==$lid){
		$rc="Se le está pidiendo que llene un cuestionario sobre el Perfil de las Dinámicas del Conflicto.  Para acceder al cuestionario, usted debe ir a:\n\n";
		$rc=$rc.getURLRoot()."/assess/spsurvey.php \n\nSe le pedirá su correo electrónico y número de clave.\n\nSu número de Clave es $cid \n\n";
		$rc=$rc."El cuestionario tomará aproximadamente de 20-25 minutos para ser completado.  Usted debe guardar este correo electrónico para tener su clave en caso de que tenga que regresar al cuestionario.\n\n";
	}
	else{
		$rc="You are being asked to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
		$rc=$rc.getURLRoot()."/assess/survey.php \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $cid \n\n";
		$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	}
	return utf8_decode($rc.getDisclaimer($lid));
}

function soReminderEmail($cid,$lid){
	if("8"==$lid){
		$rc="Este es un correo electrónico recordatorio para que llene un cuestionario sobre el Perfil de Las Dinámicas del Conflicto.  Para acceder a este cuestionario usted debe ir a:\n\n";
		$rc=$rc.getURLRoot()."/assess/spsurvey.php \n\nSe le pedirá su correo electrónico y número de clave.\n\nSu número de Clave es $cid \n\n";
		$rc=$rc."El cuestionario tomará aproximadamente de 20-25 minutos para ser completado.  Usted debe guardar este correo electrónico para tener su clave en caso de que tenga que regresar al cuestionario.\n\n";
	}
	else{
		$rc="This is a reminder email to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
		$rc=$rc.getURLRoot()."/assess/survey.php \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $cid \n\n";
		$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	}
	return utf8_decode($rc.getDisclaimer($lid));
}

function soCustomEmail($body,$cid,$lid){
	if("8"==$lid){
		return utf8_decode("$body\n\nSu enlace es:\n\n".getURLRoot()."/assess/spsurvey.php \n\nSu clave es $cid \n\n".getDisclaimer($lid));
	}
	return utf8_decode("$body\n\nYour link is:\n\n".getURLRoot()."/assess/survey.php \n\nYour pin is $cid \n\n".getDisclaimer($lid));
}

// This is the emails that are sent to 360
function initialEmail($cid,$lid){
	$rc="You are being asked to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
	$rc=$rc.getURLRoot()."/usr/ \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $cid \n\n";
	$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	return $rc.getDisclaimer();
}

function reminderEmail($cid,$lid){
	$rc="This is a reminder email to fill out a questionnaire on the Conflict Dynamics Profile. To access the questionnaire you should go to:\n\n";
	$rc=$rc.getURLRoot()."/usr/ \n\nYou will be asked to provide your email address and a PIN number.\n\nYour PIN number is $cid \n\n";
	$rc=$rc."The questionnaire will take about 20-25 minutes to complete.  You should save this email so you will have your PIN number in case you need to return to the questionnaire.\n\n";
	$rc=$rc."If you have already completed the questionnaire, you can disregard this message.\n\n";
	return $rc.getDisclaimer();
}

function customEmail($body,$cid,$lid){
	return "$body\n\nYour link is:\n\n".getURLRoot()."/usr/ \n\nYour pin is $cid \n\n".getDisclaimer();
}

function candidateSummary($cid){
    $conn=dbConnect();
    // first see if we've completed the survey
    $rs=mysql_query("select * from RATER where RID=$cid and CID=$cid and CATID=1 and EXPIRED='N' and ENDDT is NULL");
    if(mysql_num_rows($rs)){
		echo "<tr><td><small>You have not completed the self assessment.</small></td></tr>";
		echo '<tr><td onClick="javascript:frm1.action=\'../assess/self.php\';frm1.submit();"><small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp;Click here to Complete Self Assessment</small><td></tr>';
    }
    else{
		echo "<tr><td><small><img src='../images/g.gif'>&nbsp;You have completed the self assessment.</small></td></tr>";
    }

    // See how many raters we have
    $rs1=mysql_query("select CATID from RATER where CID=$cid and CATID<>1");
    $cnt=mysql_num_rows($rs1);
    echo "<tr><td><small>You have  assigned $cnt Raters. You may assign up to 30 Raters.</small></td></tr>";

	// We can still add raters
	if(11>$cnt){
		echo '<tr><td onClick="javascript:frm1.action=\'raterhome.php\';frm1.submit();">';
		echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click here to Add Raters</small><td></tr>';
    }

	// We can manage/email raters
    if(0<$cnt){
		echo '<tr><td onClick="javascript:frm1.action=\'raterhome.php\';frm1.submit();">';
		echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click here to Manage Raters</small> <td></tr>';

		echo '<tr><td onClick="javascript:frm1.action=\'emailrater.php\';frm1.submit();">';
		echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
		echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click here to send Email to Raters</small> <td></tr>';
    }

    // Check the completion status
    $rs2=mysql_query("select CATID from RATER where CID=$cid and CATID<>1 and EXPIRED='Y' and ENDDT is not NULL");
    $cmp=mysql_num_rows($rs2);

	// Only show if any raters have completed survey
    if(0<$cmp){
		// some are still pending
		if($cnt!=$cmp){
			echo "<tr><td><small> $cmp of $cnt Raters have completed the assessment.</small></td></tr>";
			echo "<tr><td onClick=\"javascript:frm1.action='checkcomp.php';frm1.submit();\">";
			echo '<small><img src="../images/r.gif" onMouseOver="this.src=\'../images/b.gif\';"';
			echo ' onMouseOut="this.src=\'../images/r.gif\';" border=0> &nbsp; Click Here to Check Completion Status</small> <td></tr>';
		}
		// all done
		else{
			echo "<tr><td><small><img src='../images/g.gif'>&nbsp;$cmp of $cnt Raters have completed the assessment.</small></td></tr>";
		}
    }
}

// Shows completion date for a Candidate's Raters (including self)
function checkCompletionStatus($cid){
	$conn=dbConnect();
	$query="select CID,CATID,LNAM, FNAM, IFNULL(ENDDT,'Incomplete') from RATER where CID=$cid group by CID,CATID,LNAM, FNAM order by CID,CATID,LNAM";
	$rs=mysql_query($query)
		or die(mysql_error());
	return dbRes2Arr($rs);
}

// Shows completion sttaus for all raters in a program
function checkProgramCompletionStatus($pid){
	$conn=dbConnect();
	$query="select a.CID,a.CATID,a.LNAM,a.FNAM, IFNULL(a.ENDDT,'Incomplete') from RATER a, CANDIDATE b,PROGRAM c where c.PID=$pid and a.CID=b.CID and b.PID=c.PID group by a.CID,a.CATID,a.LNAM,a.FNAM order by a.CID,a.CATID,a.LNAM";
	$rs=mysql_query($query)
		or die(mysql_error());   
	return dbRes2Arr($rs);
}

function ckProgramCompletionStatus($pid){
	$conn=dbConnect();

	$qryA="select a.CID, a.LNAME 
           from CANDIDATE a
          where a.PID = $pid
      order by a.LNAME, a.FNAME";
  $rs=fetchArray($qryA);
  $raters = array();
  foreach($rs as $row){ 
    $cid = $row['CID']; 
/*           
  	$qryB="select d.participantID,
                   a.CID,
                   a.RID, 
                   CONCAT(a.FNAM,' ',a.LNAM) as RNAME, 
                   a.EMAIL,
                   a.CATID,
                   IF(a.ENDDT <> '','Y','N') as COMPLETE,
                   IFNULL(DATE_FORMAT(a.STARTDT,'%m/%d/%Y %r'),' ') as STARTDT,
                   IFNULL(DATE_FORMAT(a.ENDDT,'%m/%d/%Y %r'),' ') as ENDDT
              from RATER a, CANDIDATE b, PROGRAM c, MDParticipant d
             where a.CID=b.CID and b.PID=c.PID and a.CID = d.CID AND c.PID=$pid and a.CID = $cid 
            group by a.CID,a.CATID,a.LNAM,a.FNAM 
            order by a.CID,a.CATID,a.LNAM";
*/
  	$qryB="select a.CID AS participantID,
                   a.CID,
                   a.RID, 
                   CONCAT(a.FNAM,' ',a.LNAM) as RNAME, 
                   a.EMAIL,
                   a.CATID,
                   IF(a.ENDDT <> '','Y','N') as COMPLETE,
                   IFNULL(DATE_FORMAT(a.STARTDT,'%m/%d/%Y %r'),' ') as STARTDT,
                   IFNULL(DATE_FORMAT(a.ENDDT,'%m/%d/%Y %r'),' ') as ENDDT
              from RATER a, CANDIDATE b, PROGRAM c, MDParticipant d
             where a.CID=b.CID and b.PID=c.PID AND c.PID=$pid and a.CID = $cid 
            group by a.CID,a.CATID,a.LNAM,a.FNAM 
            order by a.CID,a.CATID,a.LNAM";
  	$rs2=fetchArray($qryB);
  	foreach($rs2 as $rec){
  	  $raters[]=$rec;
  	}
  }
	return $raters;
}

function showCompletion($pid){
	$data=checkProgramCompletionStatus($pid);
	$cat=array("","Self","Boss","Peer","Direct Report");
	foreach($data as $row){
		echo "<tr>";
		echo "<td>$row[2], $row[3]</td>";
		echo "<td>".$cat[$row[1]]."</td>";
		echo "<td>$row[4]</td>";
		echo "</tr>";
	}

}

// Will return all active candidates in a program
function getAllCandidatesInProgram($pid){
	$conn=dbConnect();
	$query="select CID from CANDIDATE where PID=$pid and EXPIRED<>'Y'";
	//echo $query;
	$rs=mysql_query($query);
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		$rc[]=$row[0];
	}
	return $rc;
}

// Send emails to all xcandidates that haven't already gotten one
function getAllUnmailedCandidatesInProgram($pid){
	$conn=dbConnect();
	$query="select CID from CANDIDATE where PID=$pid and EXPIRED<>'Y' and SENT<>'Y'";
	//echo $query;
	$rs=mysql_query($query);
	$rc=array();
	$rows=dbRes2Arr($rs);
	foreach($rows as $row){
		$rc[]=$row[0];
	}
	return $rc;
}

// Send a scoring request email to Admin
function sendScoreRequestEmail($pid){
	$conn=dbConnect();
	// Get admin's email
//	$admid=$_SESSION['admid'];
//	$query="select EMAIL from CONSULTANT where CONID=$admid";
	$query="select EMAIL from CONSULTANT, ADMINS where CONID=ADMID";
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$to=$row[0];
		// Get consultant and program data
		$query="select a.EMAIL, a.FNAME, a.LNAME, c.DESCR from CONSULTANT a, PROGCONS b, PROGRAM c where a.CONID=b.CONID and b.PID=c.PID and b.PID=$pid";
		$rs=mysql_query($query);
		if($rs){
			$row=mysql_fetch_row($rs);
			$body="$row[1] $row[2] has indicated that the Conflict Dynamics Program '".$row[3]."' is ready to be scored.\n\n".getDisclaimer();
			sendGenericMail($to,$row[0],"Conflict Dynamics Profile Scoring Request",$body,"Administrator");
			return "Sent the following message:<br>".$body;
		}

	}
	return "Requested action taken, but unable to send email";
}

// Sends an initial email to all candidates that are part of the program
// that haven't received an email before
function sendInitialEmailToCandidates($pid,$conid,$type){
	$cids=getAllUnmailedCandidatesInProgram($pid);
	return $cids==false?"<font color=\"#aa0000\">Emails already sent to all candidates</font>":"<font color=\"#00aa00\"> Sent initial emails to ".sendCandidateEmail($cids,"$type","",$conid)."</font>";
}

?>
