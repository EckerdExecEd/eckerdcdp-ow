<?php
// Multilingual support added
function sendGenericMail($to,$from,$subject,$body,$name,$lid="1"){
    $hdrs="From: administrator@onlinecdp.org\n";
    $hdrs.= "Bcc: pcheely@thediscoverytec.com\n";
     
    $bod;
    if(!is_null($name)){
		if($lid=="8"){
			$bod="Estimado(a)  ".$name.",\n\n".$body;
		}
		else{
			$bod="Dear ".$name.",\n\n".$body;
		}
    }
    else{
		$bod=$body;
    }
	// But we always include the sender in the email address
	if($lid=="8"){
		mail($to,$subject,$bod."\n\n( Emisor : ".$from." )\n\n",$hdrs);
	}
	else{
		mail($to,$subject,$bod."\n\n( Sender : ".$from." )\n\n",$hdrs);
	}
    // next line for debugging
    error_log("[MAIL][TO: $to]");
    return true;
}

function getURLRoot(){
	// production
	// temporary Rackspace URL
	$rc= "https://www.onlinecdp.org/ow";
	//$rc= "https://72.32.210.144/ow";
	return $rc;
}

// Same as above, but for port 80, not port 443
// This has to do with an IE bug for reporting
function getReportURLRoot(){
	// production
	// temporary Rackspace URL
	$rc= "http://www.onlinecdp.org/ow/cons/";	
	//$rc= "http://72.32.210.144/ow/cons/";
	return $rc;
}

// multilingual support added
function getDisclaimer($lid="1"){
	if($lid=="8"){
		return "  POR FAVOR NO RESPONDA A ESTE MENSAJE.\n\n  LAS RESPUESTAS A ESTE MENSAJE POR VIA ELECTRONICA, SON ENVIADAS A UNA DIRECCION DE CORREOS NO FUNCIONAL, Y NO RECIBIRAN RESPUESTA.";
	}
	return "  PLEASE DO NOT REPLY TO THIS MESSAGE.\n\n  REPLIES TO THIS MESSAGE ARE SENT TO A NON-OPERATIONAL MAIL LOCATION AND WILL NOT RECEIVE A RESPONSE.";
}
?>
