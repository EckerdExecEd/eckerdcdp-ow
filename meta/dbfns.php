<?php
//require_once 'DB.php'; ??????

// Uses/Creates a pooled connection to database
function dbConnect(){
    // host,user,password
    $conn = mysql_pconnect("localhost", "cdptest", "ucanwin1$")
	or die(mysql_error());

    // database

    $db=mysql_select_db("cdpow")
	or die(mysql_error());

    return $conn;
}
function dbiConnect(){
	// host,user,password
	$mysqli = new mysqli("localhost", "cdptest", "ucanwin1$", "cdpow");
	if ($mysqli->connect_errno) {
		die("Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
	}

	return $mysqli;
}

// Converts a query result to an array of rows
function dbRes2Arr($result){
   $rc = array();
   for ($count=0; $row = mysql_fetch_array($result); $count++){
     $rc[$count] = $row;
    }
   return $rc;
}

// create a random key
function getKey($table,$column){
    $conn=dbConnect();
    // Generate a random key, and check if it exists already
    // Keep on until we find a good one
    $i=mt_rand(1,9999999);
    $query="select * from $table where $column=$i";
    $rs=mysql_query($query);
    if(!$rs){
	return false;
    }
    while(mysql_fetch_row($rs)){
	$i=mt_rand(1,9999999);
	$query="select * from $table where $column=$i";
	$rs=mysql_query($query);
	if(!$rs){
	    return false;
	}
    }
    return $i;
}

function quote_smart($v){
  // Stripslashes
  if(get_magic_quotes_gpc()){
    $v = stripslashes($v);
  }
  // Quote if not a number or a numeric string
  if(!is_numeric($v)){
    $v = "'" . mysql_escape_string($v) . "'";
  }
  return $v;
}

function executeQurey($qry,$affected=false){
  $conn =dbConnect(); 
    
  if(!$res=mysql_query($qry))
    die(mysql_error());
 
  if($affected){
     $rowsAffected=mysql_affected_rows();
     mysql_close($conn);
     return $rowsAffected;
  }
  
  mysql_close($conn);
  return $res;
  
}

function fetchArray($query,$mode='A'){
	//die("<hr>$query<hr>");
  $conn = dbConnect(); 
  $rs=mysql_query($query);
  $retVal=array();

  switch($mode){
    case 'N' : //Numbered
      while ($row = mysql_fetch_array($rs, MYSQL_NUM)){ $retVal[]=$row; }
      break;
    case 'B' : //Both numbered and associative
      while ($row = mysql_fetch_array($rs, MYSQL_BOTH)){ $retVal[]=$row; }    
      break;
    default : //Default to associative
      while ($row = mysql_fetch_array($rs, MYSQL_ASSOC)){ $retVal[]=$row; }      
      break;   
  }

  mysql_free_result($rs);
  mysql_close($conn);
  return $retVal;
}

function fetchOne($query,$ret='O'){
  /**
   *  Function runs query and either the first item $ret='O' or first row 'A'
   **/        
  $conn = dbConnect(); 
  $rs=mysql_query($query);
  $row=mysql_fetch_row($rs);
  $retVal =($ret=='A')? $row : $row[0];
  mysql_close($conn);
  return $retVal;
}
?>
