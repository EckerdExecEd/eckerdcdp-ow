<?php
require_once 'dbfns.php';
require_once 'mailfns.php';

// Insert a new program
function pgmInsert($pgmData){
	$conn=dbConnect();
	// Generate a random key, and check if it exists already
	// Keep on until we find a good one
	$i=mt_rand(1,9999999);
	$query="select * from PROGRAM where PID=".$i;
	$rs=mysql_query($query);
	while(mysql_fetch_row($rs)){
	    $i=mt_rand(1,9999999);
	    $query="select * from PROGRAM where PID=".$i;
	    $rs=mysql_query($query);
	}

	// Insert the PROGRAM row
	//$query="insert into PROGRAM (PID,DESCR,EXPIRED,ARCHFLAG,STARTDT,ENDDT) values ($i,'$pgmData[0]','N','N','$pgmData[1]','$pgmData[2]')";
	// wbs 12/07/2006
	// the 'quote_smart' function prevents SQL injection attacks
	$query="insert into PROGRAM (PID,DESCR,EXPIRED,ARCHFLAG,STARTDT,ENDDT) values ($i,".quote_smart($pgmData[0]).",'N','N',".quote_smart($pgmData[1]).",".quote_smart($pgmData[2]).")";
	if(!mysql_query($query)){
	    //echo $query."<br>";
	    return false;
	}

	// Insert the PROGCONS row
	//$query="insert into PROGCONS (PID,CONID) values ($i,$pgmData[3])";
	// wbs 12/07/2006
	$query="insert into PROGCONS (PID,CONID) values ($i,".quote_smart($pgmData[3]).")";
	if(!mysql_query($query)){
	    //echo $query."<br>";
	    return false;
	}

	// Insert PROGLANG rows
	// Allow all languages for the program
	$query="insert into PROGLANG (PID,LID) select $i,LID from LANG";
	if(!mysql_query($query)){
	    //echo $query."<br>";
	    return false;
	}

	return $i;
}

// Insert a new program instrument association
function instrInsert($pid,$tid){
	$conn=dbConnect();

	// Insert the PROGINSTR row
	$query="insert into PROGINSTR (PID,TID,EXPIRED,EXPDT) values ($pid,$tid,'N',NULL)";
	if(!mysql_query($query)){
	    //echo $query."<br>";
	    return false;
	}

	return true;
}

// Returns an array of arrays with program records
function pgmList($narrow,$active,$conid,$tid){
	$crit=$narrow."%";
	$conn=dbConnect();
	//$query="select a.PID,DESCR,STARTDT,ENDDT,a.EXPIRED,ARCHFLAG from PROGRAM a,PROGCONS b,PROGINSTR c where a.PID=b.PID and a.PID=c.PID and c.TID=$tid and b.CONID=$conid and DESCR like '$crit' and ARCHFLAG='$active' order by DESCR";
	// wbs 12/7/2006
	$query="select a.PID,DESCR,STARTDT,ENDDT,a.EXPIRED,ARCHFLAG from PROGRAM a,PROGCONS b,PROGINSTR c where a.PID=b.PID and a.PID=c.PID and c.TID=$tid and b.CONID=$conid and DESCR like ".quote_smart($crit)." and ARCHFLAG='$active' order by DESCR";
	//echo $query."<br>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// Returns an array containg a program record
function pgmInfo($pid){
	$conn=dbConnect();
	$query="select PID,DESCR,STARTDT,ENDDT,EXPIRED,ARCHFLAG from PROGRAM where PID=$pid";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return mysql_fetch_array($rs);
}

// Returns an array containg a program record
function showPgm($pid){
	$data=pgmInfo($pid);
	if(!$data){
	    return false;
	}
	$descr=stripslashes($data[1]);
	$begdt=date('m/d/Y',strtotime($data[2]));
	$enddt=date('m/d/Y',strtotime($data[3]));
	$exp=$data[4];
	echo "<tr><td colspan=2>Progam ID: $data[0]</td></tr>";
	echo "<tr><td>Progam Name</td><td><input type='text' name='descr' value='$descr'> </td></tr>";
	echo "<tr><td>Start Date</td><td><input type='text' name='startdt' value='$begdt'> </td></tr>";
	echo "<tr><td>End date</td><td><input type='text' name='enddt' value='$enddt'> </td></tr>";
	if($exp=='N'){
	    echo "<tr><td>Closed?</td><td> No <input type='radio' name='exp' value='N' checked> Yes <input type='radio' name='exp' value='Y' ></td></tr>";
	}
	elseif($exp=='Y'){
	    echo "<tr><td>Closed?</td><td> No <input type='radio' name='exp' value='N' > Yes <input type='radio' name='exp' value='Y' checked></td></tr>";
	}
	else{
	    echo "<tr><td>Scored</td><td></td></tr>";
	}
	return true;
}

// displays a list of all porgrams meeting specific criteria
function listPrograms($narrow,$archived,$frm,$conid,$tid){
	$rows=pgmList($narrow,$archived,$conid,$tid);

	echo "<tr><td><small>Name/ID</small></td><td><small>Start date</small></td><td><small>End date</small></td><td><small>Status</small></td><td><small>Actions</small></td></tr>";
	foreach($rows as $row){
	    // determine program status
	    $status="Open";
	    switch($row[4]){
			case "N":
			$status="Open";
			break;
			case "Y":
			$status="Closed";
			break;
			case "S":
			$status="Scored";
			break;
			default:
			$status="Error";
	    }

	    echo "<tr><td><small>".stripslashes($row[1])."<br>(ID: $row[0])</small></td><td><small>".date('m/d/Y',strtotime($row[2]))."</small></td><td><small>".date('m/d/Y',strtotime($row[3]))."</small></td><td><small>".$status."</small></td><td>";
	    if("N"==$archived){
			// the program is not archived
			echo "<input type='Button' value='Edit' onClick=\"javascript:$frm.what.value='edit';$frm.pid.value='$row[0]';$frm.action='pgmdetail.php';$frm.submit();\">&nbsp;";
			echo "<input type='Button' value='Completion Status' onClick=\"javascript:$frm.action='checkcomp.php';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";

			// this would depend on the status
			if($status=="Open"){
				echo "<input type='Button' value='Candidates' onClick=\"javascript:$frm.action='candhome.php';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";
				echo "<input type='Button' value='Email' onClick=\"javascript:$frm.action='emailcand.php';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";
				echo "<input type='Button' value='Request Score' onClick=\"javascript:$frm.action='requestscore.php';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";
			}
//			elseif($status=="Closed"){
//				// Nothing to do here, since this is done by Admin
//			}
			elseif($status=="Scored"){
				//Reports are available i.e. it has been scored
				echo "<input type='Button' value='Reports' onClick=\"javascript:$frm.action='reports.php';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";
			}

			echo "<input type='Button' value='Archive' onClick=\"javascript:$frm.what.value='archive';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";
	    }
	    else{
			// the program is archived
			echo "<input type='Button' value='Restore' onClick=\"javascript:$frm.what.value='restore';$frm.pid.value='$row[0]';$frm.submit();\">&nbsp;";
	    }
	    echo "</td></tr>";
	}
	return true;
}

// Toggle between Active and Archived status
function chgProgramStatus($pid,$status){
	$conn=dbConnect();
	$qry="update PROGRAM set ARCHFLAG='$status' where PID=$pid";
	return mysql_query($qry);
}

// Changes editable attributes
function updatePgm($pgmData){
	$conn=dbConnect();
	// update the program itself
	//$query="update PROGRAM set DESCR='$pgmData[1]',STARTDT='$pgmData[2]',ENDDT='$pgmData[3]',EXPIRED='$pgmData[4]' where PID=$pgmData[0]";
	// wbs 12/7/2006
	$query="update PROGRAM set DESCR=".quote_smart($pgmData[1]).",STARTDT='$pgmData[2]',ENDDT='$pgmData[3]',EXPIRED='$pgmData[4]' where PID=$pgmData[0]";
	if(!mysql_query($query)){
	    return false;
	}
	// and the associated instruments
	$query="update PROGINSTR set EXPDT='$pgmData[3]',EXPIRED='$pgmData[4]' where PID=$pgmData[0]";
	if(!mysql_query($query)){
	    return false;
	}
	// the candidate follows the status of a program
	$query="update CANDIDATE set EXPIRED='$pgmData[4]' where PID=$pgmData[0]";
	return mysql_query($query);
}

//---------------- EDIT COMMENTS ----------------------------------
// List all Expired programs, to retrieve comments
// Changed to include all active programs that aren't scored yet
// but excludes self-only
function getAllProgramsWithComments(){
	$conn=dbConnect();
	$query="select a.PID,DESCR,FNAME,LNAME,a.EXPIRED from PROGRAM a, PROGCONS b, CONSULTANT c, PROGINSTR d where a.PID=b.PID and b.CONID=c.CONID and a.PID=d.PID and d.TID<>3 and a.ARCHFLAG<>'Y' and a.EXPIRED<>'S' order by LNAME";
	$rs=mysql_query($query);
	return !$rs?false:dbRes2Arr($rs);
}

function listAllProgramsWithComments($frm){
	$data=getAllProgramsWithComments();
	if(!$data){
		return false;
	}
	echo "<tr><td colspan=3><select name='prog' onChange=\"$frm.pid.value=this.value;$frm.what.value='find';$frm.submit();\">";
	echo "<option value=''>-- Select a Program -- </option>";
	foreach ($data as $row){
		echo "<option value=\"$row[0]\"> ";
		echo htmlentities(stripslashes($row[1])." (ID: $row[0] Consultant: ".stripslashes($row[2])." ".stripslashes($row[3]).")");
		echo "</option>";
	}
	echo "</select></td></tr>";
	
	return true;
}

// Lists all comments for a program
function getAllComments($pid){
	$conn=dbConnect();
	$query="select a.ITEMID, a.RID, a.VAL, b.CATID, e.DESCR from RATERCMT a, RATER b, CANDIDATE c, PROGRAM d, ITEMTXT e where a.RID=b.RID and b.CID=c.CID and c.PID=d.PID and a.ITEMID=e.ITEMID and a.TID=e.TID and d.PID=$pid order by b.CATID asc, a.ITEMID asc";
//	echo $query;	
	$rs=mysql_query($query);
	return $rs?dbRes2Arr($rs):false;
}

function listAllComments($pid,$frm){
	$data=getAllComments($pid);
	if($data){
		$i=0;
		foreach($data as $row){
			echo "<tr><td>";
			echo "<input type=\"hidden\" name=\"itemid$i\" value=\"".$row[0]."\">";
			echo "<input type=\"hidden\" name=\"rid$i\" value=\"".$row[1]."\">";
			$whom="";
			switch($row[3]){
				case 2:
					$whom="Boss";
					break;
				case 3:
					$whom="Peer";
					break;
				case 4:
					$whom="Direct Report";
					break;
			}
			echo "$whom answer to question<br>$row[4]";
			echo "<br><textarea cols=40 rows=5 name='val$i'>".stripslashes($row[2])."</textarea>";
			echo "</td></tr>";
			$i++;
		}
		echo "<tr><td>";
		echo "<input type='hidden' name='count' value='$i'>";
		echo "<input type='button' value='Save' onClick=\"javascript:$frm.exp.value='N';$frm.what.value='save';$frm.submit();\">";
		echo "</td></tr>";
	}
}

// Saves all comments for a program
function saveAllComments($pid,$rid,$itemid,$val){
	$conn=dbConnect();
	// Close the rpogram, if not already Closed
//	$query="update PROGRAM set EXPIRED='Y' where PID=$pid";
//	//echo $query."<br>";
//	mysql_query($query);
	$i=0;
	foreach($rid as $rd){
		$query="update RATERCMT  set VAL='".$val[$i]."' where RID=$rd and ITEMID=".$itemid[$i];
//		echo $query."<br>";
		mysql_query($query);
		$i++;
	}
}

// displays a list from which you can view reports
// This is NOT a multilingual list
// Nor does it take Mercer-Delta into account
function listAllReports($pid,$tid){
	$conn=dbConnect();
	$query="select a.CID,a.PID,a.FNAME,a.LNAME,IFNULL(b.ENDDT,'Incomplete') from CANDIDATE a, RATER b where a.PID=$pid and a.CID=b.CID and a.CID=b.RID order by LNAME";
	$rs=mysql_query($query);
	$rows=dbRes2Arr($rs);
	if(!$rows){
		echo "No Candidates in program";
	}

	$page="showreport.php";
	if($tid=="3"){
		$page="showselfonlyreport.php";
	}
	echo "<tr><td>Name</td><td>Completion Status</td></tr>";
	foreach($rows as $row){
		if("Incomplete"==$row[4]){
			echo "<td>$row[2]&nbsp;$row[3]";
		}
		else{
			echo "<td><a href='$page?pid=$row[1]&cid=$row[0]' target='Report'>$row[2]&nbsp;$row[3]</a>";
		}

		echo "</td><td>$row[4]</td></tr>";
	}
	echo "<tr><td colspan=2>Click on name to view report</td></tr>";
	return true;
}

// displays a list from which you can view reports
// Multilingual version - the only difference from the "old" version is
// that we get a link per language
// and also a special report for Mercer-Delta
function listAllReportsML($pid,$tid){
	$lids=getAllLanguages($tid);
	$conn=dbConnect();

	// Is this a Mercer-Delta program?
	// If so, use the Mercer-Delta report style
	// English only for now
	$query="select ORGID from CONSULTANT a, PROGCONS b where a.CONID=b.CONID and b.PID=$pid";
	$rs=mysql_query($query);
	$rw=mysql_fetch_row($rs);
	if(!$rw){
		echo "Error looking up program data";
	}
	$orgid=$rw[0];

	$query="select a.CID,a.PID,a.FNAME,a.LNAME,IFNULL(b.ENDDT,'Incomplete') from CANDIDATE a, RATER b where a.PID=$pid and a.CID=b.CID and a.CID=b.RID order by LNAME";
	$rs=mysql_query($query);
	$rows=dbRes2Arr($rs);
	if(!$rows){
		echo "No Candidates in program";
	}

	$page="showreport.php";
	if($tid=="3"){
		$page="showselfonlyreport.php";
	}


	echo "<tr><td>Name</td><td>Completion Status</td></tr>";
	foreach($rows as $row){
		if("Incomplete"==$row[4]){
			echo "<td>$row[2]&nbsp;$row[3]";
		}
		else{
			echo "<td>";
			foreach($lids as $lid){
				if($orgid>1&&"1"==$lid[0]&&$tid=="3"){
					// It's Mercer Delta, English, and Individual
					$usepage=getReportURLRoot()."md$page";
					echo utf8_decode("<a href='$usepage?pid=$row[1]&cid=$row[0]&lid=$lid[0]' target='Report'>$row[2]&nbsp;$row[3]&nbsp;&nbsp;($lid[1])</a><br>");
				}
				elseif($orgid>1&&"1"==$lid[0]&&$tid=="1"){
					// It's Mercer Delta, English, and 360
					$usepage=getReportURLRoot()."md$page";
					echo utf8_decode("<a href='$usepage?pid=$row[1]&cid=$row[0]&lid=$lid[0]' target='Report'>$row[2]&nbsp;$row[3]&nbsp;&nbsp;($lid[1])</a><br>");
				}
				else{
					// Everything and everyone else
					$usepage=getReportURLRoot()."$page";
					echo utf8_decode("<a href='$usepage?pid=$row[1]&cid=$row[0]&lid=$lid[0]' target='Report'>$row[2]&nbsp;$row[3]&nbsp;&nbsp;($lid[1])</a><br>");
				}
			}
		}

		echo "</td><td>$row[4]</td></tr>";
	}
	echo "<tr><td colspan=2>Click on name to view report</td></tr>";
	return true;
}

//------------------------ SCORING -------------------------------
// displays all programs which are in a state where they can be scored
// i.e. status is NOT 'S' and they're not archived
function listScoreablePrograms(){
	$conn=dbConnect();
	$query="select a.PID,DESCR,FNAME,LNAME,EXPIRED from PROGRAM a, PROGCONS b, CONSULTANT c where a.PID=b.PID and b.CONID=c.CONID and a.ARCHFLAG<>'Y' and a.EXPIRED<>'S' and a.DESCR<>'' order by LNAME";
	$rs=mysql_query($query);
	return !$rs?false:dbRes2Arr($rs);
}

// Shows all program that are available to be scored
function listProgramsToScore($frm){

	$data=listScoreablePrograms();

	if($data){
		$question="Are you really, really, really sure you want to Score this program? Once you Score a program it is closed, and Candidates and Raters cannot enter additional data.";
		echo "<tr><td colspan=3><select name='prog' onChange=\"$frm.pid.value=this.value;\">";
		echo "<option value=''>-- Select a Program -- </option>";
		foreach ($data as $row){
			echo "<option value='$row[0]'> ";
			echo htmlentities(stripslashes($row[1])." (ID: $row[0] Consultant: ".stripslashes($row[2])." ".stripslashes($row[3]).")");
			echo "</option>";
		}
		echo "</select></td></tr>";
		echo "<tr><td>";
		echo "<input type='hidden' name='count' value='$i'>";
		echo "<input type='button' value='Score Selected Program' onClick=\"";
		echo "javascript:if(confirm('$question')){";
		echo "$frm.exp.value='S';$frm.what.value='score';$frm.action='scoreprogram.php';$frm.submit();";
		echo "}";
		echo "\">";
		echo "</td></tr>";
		return true;
	}
	return false;
}

// Sends an email for a scored program
function sendScoreEmail($pid){
	$conn=dbConnect();
	// Send an email if requested "Score"
	// Get admin's email
	$admid=$_SESSION['admid'];
	$query="select EMAIL from CONSULTANT where CONID=$admid";
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$from=$row[0];
		// Get consultant and program data
		$query="select a.EMAIL, a.FNAME, a.LNAME, c.DESCR from CONSULTANT a, PROGCONS b, PROGRAM c where a.CONID=b.CONID and b.PID=c.PID and b.PID=$pid";
		$rs=mysql_query($query);
		if($rs){
			$row=mysql_fetch_row($rs);
			$body="The reports for the Conflict Dynamics Program '".$row[3]."' are ready.\n\n".getDisclaimer();
			sendGenericMail($row[0],$from,"Conflict Dynamics Profile Reports Ready",$body,$row[1]." ".$row[2]);
		}
	}
}

// Shows what type of survey it is i.e. 360 or Individual
function getTypeOfScore($pid){
	$conn=dbConnect();
	$query="select TID from PROGINSTR where PID=$pid and TID in (1,3)";
	//echo $query;
	$rs=mysql_query($query);
	if(false==$rs){
		return false;
	}
	$row=mysql_fetch_row($rs);
	return (false==$row)?false:$row[0];
}

//------------------------ RE OPEN -------------------------------
// displays all programs which are in a state where they can be reopened
// i.e. status is 'S' and they're not archived
function listScoredPrograms(){
	$conn=dbConnect();
	$query="select a.PID,DESCR,FNAME,LNAME,EXPIRED from PROGRAM a, PROGCONS b, CONSULTANT c where a.PID=b.PID and b.CONID=c.CONID and a.ARCHFLAG<>'Y' and a.EXPIRED='S' order by LNAME";
	$rs=mysql_query($query);
	return !$rs?false:dbRes2Arr($rs);
}

// Shows all program that are available to be repopened
function listProgramsToReopen($frm){
	$data=listScoredPrograms();
	if($data){
		$question="Are you really, really, really sure you want to re-open this program?";
		echo "<tr><td colspan=3><select name='prog' onChange=\"$frm.pid.value=this.value;\">";
		echo "<option value=''>-- Select a Program -- </option>";
		foreach ($data as $row){
			echo "<option value='$row[0]'> ".stripslashes($row[1])." (Consultant: ".stripslashes($row[2])." ".stripslashes($row[3]).")</option>";
		}
		echo "</select></td></tr>";
		echo "<tr><td>";
		echo "<input type='hidden' name='count' value='$i'>";
		echo "<input type='button' value='Re-Open Selected Program' onClick=\"";
		echo "javascript:if(confirm('$question')){";
		echo "$frm.exp.value='N';$frm.what.value='open';$frm.submit();";
		echo "}";
		echo "\">";
		echo "</td></tr>";
		return true;
	}
	return false;
}

// Sends an email for a scored program
function sendReopenEmail($pid){
	$conn=dbConnect();
	// Set status to "Open"
	$query="update PROGRAM set EXPIRED='N' where PID=$pid";
	//echo $query."<br>";
	mysql_query($query);

	// Send an email
	// Get admin's email
//	$admid=$_SESSION['admid'];
//	$query="select EMAIL from CONSULTANT where CONID=$admid";
	$query="select EMAIL from CONSULTANT, ADMINS where CONID=ADMID";
	$rs=mysql_query($query);
	$rs=mysql_query($query);
	if($rs){
		$row=mysql_fetch_row($rs);
		$from=$row[0];
		// Get consultant and program data
		$query="select a.EMAIL, a.FNAME, a.LNAME, c.DESCR from CONSULTANT a, PROGCONS b, PROGRAM c where a.CONID=b.CONID and b.PID=c.PID and b.PID=$pid";
		$rs=mysql_query($query);
		if($rs){
			$row=mysql_fetch_row($rs);
			$body="The Conflict Dynamics Program '".$row[3]."' has been reopened.\n\n".getDisclaimer();
			sendGenericMail($row[0],$from,"Conflict Dynamics Profile Program Reopened",$body,$row[1]." ".$row[2]);
			return $body;
		}

	}
	return "Program re-opened, but unable to send email";
}


// Get all available languages
function getAllLanguages($tid){
	$conn=dbConnect();
	$query="select LID,DESCR from LANG";
	// Currently, Spanish is only available for Self only
	if($tid<>"3"){
		$query=$query." where LID=1";
	}
	$query=$query." order by LID";
	$rs=mysql_query($query);
	return !$rs?false:dbRes2Arr($rs);
}

// draws a language selection box
function languageBox($tid,$lid="1"){
	$rows=getAllLanguages($tid);
	$rc="<select name='lid'>";
	foreach($rows as $row){
		$sel=($lid==$row[0])?"selected":"";
		$rc=$rc."<option value='$row[0]' $sel> ".utf8_decode($row[1])." </option>";
	}
	return $rc."</select>";
}

?>
