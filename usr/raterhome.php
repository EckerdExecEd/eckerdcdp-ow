<?php 
$msg="";
session_start();
if(empty($_SESSION['cid'])){
    die("Not Logged in.");
}
require_once "../meta/candidate.php";
require_once "../meta/rater.php";
require_once "candfn.php";

$msg="";
$rid=$_POST['rid'];
$cid=$_SESSION['cid'];
$catid=$_POST['catid'];
$what=$_POST['what'];

if("add"==$what){
	$idx=$_POST['idx'];
	$email=addslashes($_POST["email$idx"]);
	if(!raterEmailExists($cid,$email)){
		$ratData=array();
		$ratData[0]=$cid;
		$ratData[1]=$catid;
		$ratData[2]=addslashes($_POST["fname$idx"]);
		$ratData[3]=addslashes($_POST["lname$idx"]);
		$ratData[4]=$email;
		$ratData[5]="2";
		$rid=addRater($ratData);
		if(!$rid){
			$msg="<font color=\"#aa0000\">Unable to add rater</font>";
		}
		else{
			$msg="<font color=\"#00aa000\">Successfully added rater</font>";
		}
	}
	else{
		$msg="<font color=\"#aa0000\">Email address already exists</font>";
	}
}
// Delete an existing rater
if("del"==$what){
	$i=deleteRater($rid);
	if(!$rid){
		$msg="<font color=\"#aa0000\">Unable to delete rater</font>";
	}
	else{
		$msg="<font color=\"#00aa000\">Successfully deleted rater</font>";
	}
}

// Save rater data
if("save"==$what){
	$idx=$_POST['idx'];
	$email=addslashes($_POST["email$idx"]);
	if(!raterEmailExists($cid,$email,$rid)){
		$ratData=array();
		$ratData[0]=$cid;
		$ratData[1]=$catid;
		$ratData[2]=addslashes($_POST["fname$idx"]);
		$ratData[3]=addslashes($_POST["lname$idx"]);
		$ratData[4]=$email;
		$ratData[5]=$rid;
		if(!saveRater($ratData)){
			$msg="<font color=\"#aa0000\">Unable to save data</font>";
		}
		else{
			$msg="<font color=\"#00aa000\">Successfully saved data</font>";
		}
	}
	else{
		$msg="<font color=\"#aa0000\">Email address already exists</font>";
	}
}
// send initial rater emails
if("send"==$what){
	$msg=sendInitialEmailToAllRaters($cid);
}

writeHead("Conflict Dynamics Profile - Candidates",false);
writeBody("Rater Management Console",$msg);
?>
<form name="frm1" action="raterhome.php" method=POST>
<input type="hidden" name="cid" value="<?=$cid?>">
<input type="hidden" name="rid" value="">
<input type="hidden" name="catid" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="idx" value="">
<input type="button" value="Go Back" onClick="javascript:frm1.action='home.php';frm1.submit();">
<p>
You can add, edit and delete raters on this page. When you are done 
<input type=button onClick="javascript:frm1.what.value='send';frm1.submit();" value="Click Here"><br> to send initial email
to your raters.
</p>
<p><?=listRaterForms($cid,"frm1")?>
</form>
<script language="JavaScript">
function subForm(frm,catid){
    if(frm.lname.value.length<1||frm.fname.value.length<1||frm.email.value.length<1){
	alert("You must provide First and Last Names and Email address.");
    }
    else{
	frm.catid.value=catid;
	frm.what.value="add";
	frm.submit();
    }
}

function subEditForm(frm,rid){
    if(frm.lname.value.length<1||frm.fname.value.length<1||frm.email.value.length<1){
	alert("You must provide First and Last Names and Email address.");
    }
    else{
	frm.rid.value=rid;
	frm.what.value="save";
	frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>

