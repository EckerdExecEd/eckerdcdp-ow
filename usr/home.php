<?php 
$msg="";
session_start();
if("1"==$_POST['s']){
    // if we're here it's a login request
    require_once "../meta/dbfns.php";
    $uid=addslashes($_POST['uid']);
    $pwd=addslashes($_POST['pin']);
    /*$conn=dbConnect();
	$rs=mysql_query("select CID,FNAME,LNAME,EXPIRED,ARCHFLAG from CANDIDATE where EMAIL='$uid' and CID=$pwd and OTH='Y'");

    $row=mysql_fetch_row($rs);
    if(!$row){
	// Login error - go back to the login page
	header("Location: index.php?lerr=1");
    }
    else{
	if('Y'==$row[3]){
	    // Login error - go back to the login page
	    header("Location: index.php?lerr=2");
	}
	elseif('Y'==$row[4]){
	    // Login error - go back to the login page
	    header("Location: index.php?lerr=3");
	}
	else{
	    $_SESSION['cid']=$row[0];
	    $_SESSION['cname']=$row[1]." ".$row[2];
	}
    }*/
    $mysqli=dbiConnect();
    if (!($query = $mysqli->prepare("select CID,FNAME,LNAME,EXPIRED,ARCHFLAG from CANDIDATE where UPPER(EMAIL)=? and CID=? and OTH='Y'"))) {
    	echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_param("si", strtoupper($uid), $pwd)) {
    	echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    if (!$query->execute()) {
    	echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_result($out_CID, $out_FNAME, $out_LNAME, $out_EXPIRED, $out_ARCHFLAG)) {
    	echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $row = $query->fetch();
    $query->close();
    if(!$row){
    	// Login error - go back to the login page
    	header("Location: index.php?lerr=1");
    }else{
    	if('Y'==$out_EXPIRED){
    		// Login error - go back to the login page
    		header("Location: index.php?lerr=2");
    	}
    	elseif('Y'==$out_ARCHFLAG){
    		// Login error - go back to the login page
    		header("Location: index.php?lerr=3");
    	}
    	else{
    		$_SESSION['cid']=$out_CID;
    		$_SESSION['cname']=$out_FNAME." ".$out_LNAME;
    	}
    }
}
if(empty($_SESSION['cid'])){
    die("Not Logged in.");
}
$msg="Welcome ".$_SESSION['cname']."<br>";
require_once("candfn.php");
writeHead("Conflict Dynamics Profile - Candidate",false);
writeBody($msg,"");
$cid=$_SESSION['cid'];
menu($cid);
writeFooter(false);
?>

