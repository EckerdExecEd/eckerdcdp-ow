<?php
require_once "../meta/dbfns.php";

// returns a list of programs which are ready to be uploaded to
function getUploadablePrograms($conid,$tid){
	$conn=dbConnect();	
	$query="select a.PID,DESCR from PROGRAM a,PROGCONS b,PROGINSTR c where a.PID=b.PID and a.PID=c.PID and c.TID=$tid and b.CONID=$conid and ARCHFLAG='N' order by DESCR";
	//echo $query."<br>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

class DataReader {
	
	/**
	 *	External Methods
	 *		constructor( $dataOrFile, $pid, $tid, $conid)		- All parameters are optional
	 *		data( $data)				- sets data array
	 *		file( $filename )		- set data file and then reads it into the data array
	 *		processCandidate()
	 *		processRaters
	 *		cid()								- returns the candidate id
	 *		numRaters()					- returns the number of raters
	 *		selfIdx()						- returns the self rater, i.e. candidate, index
	 **/
	
	var $_pid;
	var $_tid;
	var $_conid;
	var $_prog;
	var $_cand;
	var $_filename;
	var $_data;
	var $_numRaters;
	var $_selfIdx = -1;
	var $_cid = -1;
	var $layout;
	
	function DataReader( $dataOrFile='', $pid=-99, $tid=-99, $conid=-99 ){
			
		// constructor
		$this->_prog=array();
		$this->_cand=array();
		$this->_filename = '';
		$this->_data = array();
		
		// Get parameters
		$this->_pid = $pid;			if ($this->_pid == -99) $this->_pid = $_POST["pid"];
		$this->_tid = $tid;			if ($this->_tid == -99) $this->_tid = $_POST["tid"];
		$this->_conid = $conid;	if ($this->_conid == -99) $this->_conid = $_SESSION['conid'];

		// Get _data, number of raters and self index	
		if (is_array($dataOrFile)) {
			// Data array passed
			$this->_data = $dataOrFile;
		} else {
			// Filename passed
			if (strlen($dataOrFile) > 0) {
				$this->_filename = $dataOrFile;
				$this->readDataFile();
			}
		}
		$this->_numRaters = count($this->_data);
		$this->_selfIdx = $this->findSelfIdx();

		// Define data layout		
		$this->setLayout();
		
	}

	function data( $data ) {
		/**
		 * Set the data array
		 **/
		$this->_filename = '';
		$this->_data = $data;
	}

	function file( $filename ) {
		/**
		 * Set the data file
		 **/
		$this->_filename = $filename;
		$this->readDataFile();
	}
	
	function cid( ) {
		return $this->_cid;
	}

	function numRaters( ) {
		return $this->_numRaters;
	}

	function selfIdx( ) {
		return $this->_selfIdx;
	}
	
	function readDataFile( ) {
		/**
		 *	Read the contents of a data file
		 **/
	
		if (!strlen($this->_filename)) {
			// No filename specified
			$this->_data = array();
			
		} else {
			// Read data file
			$fp = fopen($this->_filename, 'r');
			$contents;
			while(false != ($str = fgets($fp,1024))){
				// Discard empty CR-LF lines
				$str = trim($str);
				if (strlen($str) > 2){
					$contents[] = $str;
				}
			}
			//echo "Read $line lines<br>";
			// Close the temporary file
			fclose($fp);

			$this->_data = $contents;
		
		}
		
	}
	
	function findSelfIdx() {
		// Assume the self rater is the longest row
		$selfIdx = -1;
		$selfLen = 0;
		for ($i=0; $i<$this->_numRaters; $i++) {
			if (strlen($this->_data[$i]) > $selfLen) {
				$selfIdx = $i;
				$selfLen = strlen($this->_data[$i]);
			}
		}
		
		$this->_selfIdx = $selfIdx;
		
		return $this->_selfIdx;
		
	}

	function processCandidate() {

		// Define _prog[] array values
		$this->_prog['pid'] = $this->_pid;
		$this->_prog['instrument_Type'] = 1;
		$this->_prog['CONID'] = $this->_conid;
		
		// Define _cand[] array values
		$this->_cand['19'] = $this->getValue('fname', $this->_selfIdx);
		$this->_cand['20'] = $this->getValue('lname', $this->_selfIdx);
		$this->_cand['3'] = $this->getValue('email', $this->_selfIdx);
		$this->_cand['pid'] = $this->_pid;
		$this->_cand['language'] = 1;
		$this->_cand['clientidentification'] = $this->getValue('clientidentification', $this->_selfIdx);

		// Create the candidate
		$candidate = new CDPCandidate($this->_cand, $this->_prog);
		$this->_cid = $candidate->process();

		// If no email is specified then set email equal to cid/pin
		if (!strlen($this->_cand['3'])) {
			$conn = dbConnect();
			$query = 'update CANDIDATE set EMAIL = "' . $this->_cid . '" where CID = ' . $this->_cid;
			$rs = mysql_query($query);
		}

		return $this->_cid;

	}

	function processRaters( ) {
		/**
		 *	Loop through and process all of the raters
		 **/

		$msg = '';
		$raterCount = 0;

		$readyToScore = true;

		// First process the Self, in order to get the Candidate ID (CID)
		$rid = $this->processRater($this->_selfIdx);
		$msg .= '<br>Processing self rater';
		if ($rid == false) {
			$msg .= '<br><font color="#ff0000">Error 2: unable to process data for self rater</font>';
			// $msg .= '<br><font color="#ff0000">Error 2: unable to process data for self rater ' . $this->_selfIdx . '</font>';	// For debug
			$readyToScore = false;
		} else {
			$raterCount++;
			// $msg .= ' ('  . $rid . ') - OK!';	// use for debugging
			$msg .= ' - OK!';
		}

		// Process all the other raters
		for ($idx=0; $idx<$this->_numRaters; $idx++) {
			if ($idx != $this->_selfIdx) {
				$rid = $this->processRater($idx);
				$msg .= '<br>Processing rater ' . $raterCount++;
					
				if ($rid == false) {
					$msg .= '<br><font color="#ff0000">Error 2: unable to process data for rater ' . $raterCount . '</font>';
					//$msg .= '<br><font color="#ff0000">Error 2: unable to process data for rater ' . $idx . '</font>';	// For debugging
					$readyToScore = false;
				} else {
					// $msg .= ' ('  . $rid . ') - OK!';		// Use for debugging
					$msg .= ' - OK!';
				}
			}
		}

		if ($readyToScore) {
			//		echo "CID=$cid<br><br>";
			// Everything went well - we don't really score we just update the completion status
			if (setComplete($this->_cid) == false) {
				$msg .= '<br><font color="#ff0000">Error 3: unable to score - aborting</font>';
				$this->_cid = false;
			}
		} else {
			$msg .= '<br><font color=\"#ff0000\">Error 4: unable to score - aborting</font>';
			$this->_cid = false;
		}

		return $msg;
	}

	function processRater( $idx ) {
		/**
		 * Process a single rater that is specified by the index of that rater
		 **/

		// Get demographics
		$demographics = $this->getDemographics($idx);
		//echo '<hr><h4>DEMOGRAPHICS[' . $idx . ']</h4>'; var_dump($demographics);		// For debugging

		// Get rater responses
		$responses = $this->getResponses($idx);
		//echo '<hr><h4>RESPONSES[' . $idx . ']</h4>'; var_dump($responses);		// For debugging

		$rat = new CDPRater($this->_cid, $this->_prog['instrument_Type'], $demographics, $responses);
		$rid = $rat->process();

		$conn = dbConnect();
		if ($idx == $this->_selfIdx) {
			// Self Rater
			// Set email address
			$query = 'update RATER set EMAIL = "' . $rid . '" where CID = ' . $this->_cid . ' AND RID = ' . $rid;
			$rs = mysql_query($query);
		} else {
			// Non Self Rater
			// Set LNAM to RID for non-self raters because we need some value for FNAM or LNAM
			// Also set email to RID in case we need a value there for any reason
			$query = 'update RATER set LNAM = "' . $rid . '", EMAIL = "' . $rid . '" where CID = ' . $this->_cid . ' AND RID = ' . $rid;
			$rs = mysql_query($query);
		}
		
		return $rid;

	}

	function getValue( $fieldname, $idx=0 ) {
		/**
		 *	Return the value from the data string 
		 **/

		// Get position of the field in the line
		$pos1 = $this->layout[$fieldname][0];
		if ($pos1 < 0) {
			// The field is not in the data string
			$value = '';
			
		} else {
			// Retrieve the field
			$pos1 = $pos1 - 1;
			$pos2 = $this->layout[$fieldname][1]; 
				
			$value = substr($this->_data[$idx], $pos1, $pos2 - $pos1);
		}

		$value = trim($value);
		
		// Special processing for some fields
		switch ($fieldname) {
			case "ethnicity" :
				$value = '';
				$dataStr = $this->_data[$idx];
				if (substr($dataStr, $this->layout['ethnicity_nativeamerican'][0], 1) == 1) $value = 6;
					elseif (substr($dataStr, $this->layout['ethnicity_asian'][0], 1) == 1) $value = 7;
					elseif (substr($dataStr, $this->layout['ethnicity_black'][0], 1) == 1) $value = 8;
					elseif (substr($dataStr, $this->layout['ethnicity_hispanic'][0], 1) == 1) $value = 9;
					elseif (substr($dataStr, $this->layout['ethnicity_caucasian'][0], 1) == 1) $value = 10;
					elseif (substr($dataStr, $this->layout['ethnicity_other'][0], 1) == 1) $value = 11;
					else $value = '';
				break;

			case "sex" :
				if ($value == "Female") $value = "F";
					elseif ($value == "Male") $value = "M";
					else $value = substr($value, 0, 1);
				break;
		}
		
		$value = mysql_escape_string($value);
		//echo '<br />[Rater ' . $idx . '] ' . $fieldname . ' = ' . $value;		 // For debugging
	
		return $value;

	}

	function getResponses( $idx ) {
		/**
		 *	Returns an array of rater responses  (question # => response)
		 **/

		if ($idx == $this->_selfIdx) {
			// Self raters usually have extra questions
			$responseStr = $this->getValue('selfresponses', $idx);
		} else {
			$responseStr = $this->getValue('responses', $idx);
		}
		$responses = array();
		
		$numResponses = strlen($responseStr);
		for ($i=0; $i<$numResponses; $i++) {
			$qNum = $i+1;
			$responses[$qNum] = substr($responseStr, $i, 1);
		}
	
		return $responses;
		
	}

	function getDemographics( $idx ) {
		/**
		 *	Returns the demographics array
		 **/

		$demo = array();

		if ($idx == $this->_selfIdx) {
			// ----------------
			// Self rater
			// ----------------
			
			// RaterType (self is 1)
			$demo[0] = 1;
			
			// Organization Name  (free form)
			$demo[1] = $this->getValue('orgname', $idx);
			
			// Work Phone  (valid phone number format)
			$demo[2] = $this->getValue('workphone', $idx);
			
			// Email Address  (valid email format)
			$demo[3] = $this->getValue('email', $idx);
			
			// Sex  (M/F)
			$demo[4] = $this->getValue('sex', $idx);

			// Age  (number)
			$demo[5] = (int) $this->getValue('age', $idx);

			// Ethnicity  (6  - Native American or Alaskan
			//             7  - Asian or pacific islander
			//             8  - Black or african american
			//             9  - Hispanic
			//             10 - Caucasian
			//             11 - Other)
			$demo[6] = $this->getValue('ethnicity', $idx);
	
			// Ethnicity explanation, Only if "Other" (free form)
			$demo[12] = $this->getValue('ethnicityexplained', $idx);
	
			// Highest degree earned  (1 - High school or less
			//                         2 - Associate's
			//                         3 - bachelor's
			//                         4 - masters
			//                         5 - doctorate professional)
			$demo[13] = (int) $this->getValue('degree', $idx);
	
			// Organization Level  (1 - top executive
			//                      2 - executive
			//                      3 - upper middle management
			//                      4 - middle office manager ...
			//                      5 - first level foreperson
			//                      6 - Hourly employee
			//                      7 - N/A)
			$demo[14] = (int) $this->getValue('orglevel', $idx);
	
			// Type of Organization - A  (1 - For Profit, 2 - Not For Profit)
			$demo[15] = (int) $this->getValue('orgtypea', $idx);
	
			// Type of Organization - B  (1  - Manufacturing
			//                            2  - Transportation/Communications/Utilities
			//                            3  - Wholesale/Retail Sales
			//                            4  - Finance/Insurance/Banking
			//                            5  - Personal Services/Legal
			//                            6  - Business Services/Consulting
			//                            7  - Health Services
			//                            8  - Eductation Services
			//                            9  - Social Services
			//                            10 - Public Administration/Legal
			//                            11 - Military
			$demo[16] = (int) $this->getValue('orgtypeb', $idx);

			// Your function  (1  - Accounting
			//                 2  - Administion
			//                        .
			//                        .
			//                        .
			//                 23 - yop managment
			//                 24 - Other
			$demo[17] = (int) $this->getValue('function', $idx);

			// Specification of function (free form)
			$demo[18] = $this->getValue('functionspecific', $idx);

		} else {
			// ----------------
			// Non-self rater
			// ----------------
		
			// RaterType  (1 - self
			//             2 - boss
			//             3 - peer
			//             4 - direct report)
			// Note: We have to add 1 to raterType that is read in
			
			$demo[0] = (int) $this->getValue('relationship', $idx) + 1;
			
			// Organization name of the person being rated (free form)
			$demo[1] = $this->getValue('orgname', $idx);

			// How well do you know the person being rated (1 - I hardly know the person
			//                                              2 - I know the person somewhat
			//                                              3 - I know the person well
			//                                              4 - I know the person very well
			$demo[2] = (int) $this->getValue('familiarity', $idx);

			// Sex (M/F)
			$demo[3] = $this->getValue('sex', $idx);

			// Age (Numeric)
			$demo[4] = (int) $this->getValue('age', $idx);

		}

		return $demo;

	}

	function setLayout( ) {
		/**
		 *	This function defines the layout of the data
		 *	Each entry consists of a name for the field and an array of initial position and last position
		 **/
		 $this->layout['age'] = array(83, 84);
		 $this->layout['clientidentification'] = array(41, 48);
		 $this->layout['degree'] = array(91, 91);
		 $this->layout['email'] = array(-1, -1);
		 $this->layout['ethnicity_nativeamerican'] = array(85, 85);
		 $this->layout['ethnicity_asian'] = array(86, 86);
		 $this->layout['ethnicity_black'] = array(87, 87);
		 $this->layout['ethnicity_hispanic'] = array(88, 88);
		 $this->layout['ethnicity_caucasian'] = array(89, 89);
		 $this->layout['ethnicity_other'] = array(90, 90);
		 $this->layout['ethnicityexplained'] = array(-1, -1);
		 $this->layout['familiarity'] = array(99, 99);
		 $this->layout['fname'] = array(64, 75);
		 $this->layout['function'] = array(96, 97);
		 $this->layout['functionspecific'] = array(-1, -1);
		 $this->layout['lname'] = array(49, 63);
		 $this->layout['middleinitial'] = array(76, 76);
		 $this->layout['orglevel'] = array(95, 95);
		 $this->layout['orgname'] = array(-1, -1);
		 $this->layout['orgtypea'] = array(92, 92);		// Profit/Not-for-profit
		 $this->layout['orgtypeb'] = array(93, 94);
		 $this->layout['relationship'] = array(98, 98);
		 $this->layout['responses'] = array(116, 193);
		 $this->layout['selfresponses'] = array(116, 229);
		 $this->layout['sex'] = array(77, 82);
		 $this->layout['workphone'] = array(-1, -1);

			// These are not used
		 $this->layout['seqnumber'] = array(1, 9);
		 $this->layout['unknown'] = array(10, 18);
		 $this->layout['type'] = array(19, 21);
		 $this->layout['y'] = array(23, 23);
		 $this->layout['num1'] = array(25, 28);
		 $this->layout['num2'] = array(30, 34);
		 $this->layout['n'] = array(39, 39);
		 $this->layout['candidateid'] = array(100, 110);
	}

	function showData() {
		/** 
		 *	Returns an HTML display of the data
		 **/

		$contents = $this->_data;
		
		$str .= "\n" . '<table style="background-color:#f0f0f0;font:normal 9pt courier;">';
		$numLines = count($contents);
	
		// Headings
		$str .= "\n\t" . '<tr>';
		$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;">&nbsp;</td>';
		$count = 0;
		$dec = 0;
		for ($j=0; $j<230; $j++) {
			$count++;
			$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;width:5pxmargin-left:1px;margin-bottom:1px;">' . $dec . '</td>';
			if ($count == 9) {
				$count = -1;
				$dec++;
				if ($dec > 9) $dec = 0;
			}
		}
		$str .= "\n\t" . '</tr>';
		$str .= "\n\t" . '<tr>';
		$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;">&nbsp;</td>';
		$count = 0;
		for ($j=0; $j<230; $j++) {
			$count++;
			$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;width:5pxmargin-left:1px;margin-bottom:1px;">' . $count . '</td>';
			if ($count == 9) $count = -1;
		}
		$str .= "\n\t" . '</tr>';
	
		// Values
		for ($i=0; $i<$numLines; $i++) {
			$str .= "\n\t" . '<tr>';
			$str .= "\n\t\t" . '<td style="background-color:#f0f0f0;">' . $i . '</td>';
			$numChars = strlen($this->_data[$i]);
			for ($ch=0; $ch<$numChars; $ch++) {
				$char = substr($this->_data[$i], $ch, 1);
				$str .= "\n\t\t" . '<td style="background-color:#ffffff;width:5pxmargin-left:1px;margin-bottom:1px;">' . $char . '</td>';
			}
			$str .= "\n\t" . '</tr>';
		}
		$str .= "\n" . '</table>';
	
		return $str;

	}

}

function showQuery( $qry ) {
	// For debugging only
	$conn = mysql_pconnect("localhost", "cdptest", "ucanwin1$") or die(mysql_error());
	$db = mysql_select_db("cdpow") or die(mysql_error());
	echo '<h4 style="background-color:#e8e8e8;">BEGINING OF RESULTS</h4>';
	echo '<hr />QRY: ' . $qry;
	$rs = mysql_query($qry);
	if ($row=mysql_fetch_array($rs)) {
		echo '<hr>'; var_dump($row);
	}
	echo '<h4 style="background-color:#e8e8e8;">END OF RESULTS</h4>';
}
	
?>
