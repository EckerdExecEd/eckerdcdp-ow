<?php
require_once "../meta/dbfns.php";

// returns a list of programs which are ready to be uploaded to
function getUploadablePrograms($conid,$tid){
	$conn=dbConnect();	
	$query="select a.PID,DESCR from PROGRAM a,PROGCONS b,PROGINSTR c where a.PID=b.PID and a.PID=c.PID and c.TID=$tid and b.CONID=$conid and ARCHFLAG='N' order by DESCR";
	//echo $query."<br>";
	$rs=mysql_query($query)
	    or die(mysql_error());
	return dbRes2Arr($rs);
}

// Insert data for the Self Rater
function processCandidate($pid, $tid, $conid, $data, $layout){
	$dr = new DataReader();
	$dr->process();
showQuery("select * from WSProgram where  EXTID=1234 and CONID=1952490");
	//echo $data."<br>";
	return 12345;
}

// Insert data for the non-Self Raters
function processRater($pid,$tid,$cid,$data){
	//echo $data."<br>";
	return true;
}

// Insert the rater responses
function processResponses(){
	return true;
}

class DataReader{
	
	var $creds;
	var $prog;
	var $attr;
	var $cand;
	var $forms;
	var $form;
	var $demos;
	var $demo;
	var $catid = 0;
	var $bio = false;	
	var $cid = -1;
	var $req_key;	// requestor's id
	var $layout;
		
	// status set to 0 while we're processing
	// will be set to error code or 1 on success
	var $status = 0;
	var $wait = 0;
	var $error = false;
	
	function DataReader(){
		// constructor
		$this->creds=new Credentials();
		$this->prog=array();
		$this->cand=array();
		$this->forms=array();
		$this->demos=array();
		
		// Get CONID
		$this->creds->setUID('czearfoss@presensit.');
		$this->checkCredentials();
		$this->creds->setPWD('tublecane');
		$this->checkCredentials();

//showQuery("select *, count(*) from CONSULTANT where UID='czearfoss@presensit.' and PWD=PASSWORD('tublecane') group by UID,CONID");

		// Set program
echo '<li>CONID = ' . $this->prog['CONID'];	// This is set by checkCredentials()
		$this->prog['EventID'] = '1234';
		$this->prog['EventName'] = 'File Upload Test';
		$this->prog['EventLocation'] = 'Winston-Salem, NC';
		$this->prog['EventDate'] = date('Y-m-d H:i:s');
		if ("CDPIND" == '') {
			$this->prog['instrument_Type'] = 3;
		} else {
			$this->prog['instrument_Type'] = 1;
		}
		
		// Set layout		
		$this->getLayout();
		
	}

	function checkCredentials(){
		// check the credentials
		if ($this->creds->hasCredentials()) {
			$this->status = $this->creds->verify();
			if($this->status == 0) {
				// succesful login, get the Consultant ID
				$this->prog['CONID'] = $this->creds->getConid();
			}
		}
	}

	function process($data){
		// start with the program
		// This will either return the PID of an existing program, or
		// if it doesn't exist it will create it
		$program = new CDPProgram($this->prog);
		$pid = $program->process();

		if ($pid === false) {
			$this->status=$program->getError();
//			echo "Status (195): ".$this->status."\n";
		} else {
echo '<li>PID: ' . $pid;
			// create the candidate
			$this->cand['pid'] = $pid;
			$this->cand['language'] = 1;
			$this->cand['instrument_language'] = 1;
			$this->cand['clientidentification'] = $this->getValue('clientidentification', $data);
			$this->cand['3'] = '';
			$this->cand['19'] = $this->getValue('fname', $data);
			$this->cand['20'] = $this->getValue('lname', $data);

			$this->client['clientidentification'] = $this->cand['clientidentification']
			$this->cand['clientidentification'] = $this->cand['clientidentification']
			$this->req_key=$data;

			// demographics
			/*
			$idx=$this->attr['item'];
			$this->demo[$idx]=$data;
			if(1==$this->catid){
				if($idx==19||$idx==20||$idx==3){
					// This is the self, we need to save some data
					$idx=$this->attr['item'];
					$this->cand[$idx]=$data;
				}
			}
			*/
			
			// RaterType
			/*
			// Note: rater type goes into demo[0]
			$this->catid=$data;
			$this->demo[0]=$data;
			*/

			$cand = new CDPCandidate($this->cand, $this->prog);

			// First we should check if the candidate exists
			// if she does we can do a bio rescore, nothing else
			// 11-22-2006: We look for an existing CID, passed in via vendorkey
			if ($this->cid != -1) {
				// we already have one of these
				// do the biorescore here
//				echo "\nBio Rescore\n";
				//$this->cid=$cand->getCid();
				$bio=true;
				if ($cand->bioRescore($this->cid) == false) {
					$this->status=$cand->getError();
//					echo "Status (219):".$this->status."\n";
				}
				
			} else {
				// new candidate - we should do a full score
				// process the candidate and the raters
				$this->cid=$cand->process();
				if ($this->cid === false) {
					// an error of some kind occurred
					$this->status=$cand->getError();
//					echo "Status (229):".$this->status."\n";
				} else {
					// if we're here we have the program, the candidate so now it's time
					// to process the raters
					for ($i=0; $i<count($this->forms); $i++) {
						// iterate over each rater
						$rat = new CDPRater($this->cid, $this->prog['instrument_Type'], $this->demos[$i], $this->forms[$i]);
						$rid = $rat->process();
//						echo "RID: $rid\n";
						if($rid == false) {
							$this->status = $rat->getError();
							break;
						}
					}
				}
			}
			
echo '<h4>CANDIDATES</h4>' . showQuery("select CID from CANDIDATE where PID=" . $pid);

		}
	}

	function getValue( $fieldname, $data ) {
		// Get position of the field in the line
		$pos1 = $this->layout[$fieldname][0] - 1;
		$pos2 = $this->layout[$fieldname][1]; 

		$value = substr($data, $pos1, $pos2 - $pos1);
	
		return $value;
	}

	function getLayout( ){
		/**
		 *	This function defines the layout of the data
		 *	Each entry consists of a name for the field and an array of initial position and last position
		 **/
		$this->layout = array('seqnumber' => array(1, 9),
													'unknown' => array(10, 18),
													'type' => array(19, 21),
													'y' => array(23, 23),
													'num1' => array(25, 28),
													'num2' => array(30, 34),
													'n' => array(39, 39),
													'clientidentification' => array(41, 48),
													'lname' => trim(array(49, 63)),
													'fname' => trim(array(64, 75)),
													'middleinitial' => trim(array(76, 76)),
													'sex' => array(77, 82),
													'age' => array(83, 84),
													'race_nativeamerican' => array(85, 85),
													'race_asian' => array(86, 86),
													'race_black' => array(87, 87),
													'race_hispanic' => array(88, 88),
													'race_caucasian' => array(89, 89),
													'race_other' => array(90, 90),
													'degree' => array(91, 91),
													'profittype' => array(92, 92),
													'orgtype' => array(93, 94),
													'orglevel' => array(95, 95),
													'position' => array(96, 97),
													'relationship' => array(98, 98),
													'familiarity' => array(99, 99),
													'candidateid' => array(100, 110),
													'answers' => array(116, 129));
	}

}

function showQuery( $qry ) {
	// For debugging only
	$conn = mysql_pconnect("localhost", "cdptest", "ucanwin1$") or die(mysql_error());
	$db = mysql_select_db("cdpow") or die(mysql_error());
	echo '<h4 style="background-color:#e8e8e8;">BEGINING OF RESULTS</h4>';
	echo '<hr />QRY: ' . $qry;
	$rs = mysql_query($qry);
	if ($row=mysql_fetch_array($rs)) {
		echo '<hr>'; var_dump($row);
	}
	echo '<h4 style="background-color:#e8e8e8;">END OF RESULTS</h4>';
}
	
?>
