<?php
header("Content-Type: text/xml");
echo "<?xml version='1.0'?>
 			<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">				<soap:Body>
				<Score_Response>
					<data>
						<requestorkey>12345</requestorkey>
						<vendorkey>54321</vendorkey>
						<score_actual_key>
							<scored_data persistDB='True' persistFile='False' persistFileFormat='XML'>
								<report_scores>
									<scores scale='Perspective Taking' scorelength='1'>
										<score rater_type='1'>31.1</score> 
									</scores>
									<scores scale='Creating Solutions' scorelength='1'>
										<score rater_type='1'>33.2</score> 
									</scores>
									<scores scale='Expressing Emotions' scorelength='1'>
										<score rater_type='1'>58.7</score> 
									</scores>
									<scores scale='Reaching Out' scorelength='1'>
										<score rater_type='1'>27.3</score> 
									</scores>
									<scores scale='Reflective Thinking' scorelength='1'>
										<score rater_type='1'>31.4</score> 
									</scores>
									<scores scale='Delay Responding' scorelength='1'>
										<score rater_type='1'>23.1</score> 
									</scores>
									<scores scale='Adapting' scorelength='1'>
										<score rater_type='1'>20.2</score> 
									</scores>
									<scores scale='Winning' scorelength='1'>
										<score rater_type='1'>47.6</score> 
									</scores>
									<scores scale='Displaying Anger' scorelength='1'>
										<score rater_type='1'>66.0</score> 
									</scores>
									<scores scale='Demeaning Others' scorelength='1'>
										<score rater_type='1'>46.3</score> 
									</scores>
									<scores scale='Retaliating' scorelength='1'>
										<score rater_type='1'>63.0</score> 
									</scores>
									<scores scale='Avoiding' scorelength='1'>
										<score rater_type='1'>57.5</score> 
									</scores>
									<scores scale='Yielding' scorelength='1'>
										<score rater_type='1'>55.6</score> 
									</scores>
									<scores scale='Hiding Emotions' scorelength='1'>
										<score rater_type='1'>62.0</score> 
									</scores>
									<scores scale='Self-Criticizing' scorelength='1'>
										<score rater_type='1'>32.0</score> 
									</scores>
									<scores scale='Constructive Pre' scorelength='1'>
										<score rater_type='1'>31.8</score> 
									</scores>
									<scores scale='Destructive Pre' scorelength='1'>
										<score rater_type='1'>49.4</score> 
									</scores>
									<scores scale='Constructive During' scorelength='1'>
										<score rater_type='1'>24.4</score> 
									</scores>
									<scores scale='Destructive During' scorelength='1'>
										<score rater_type='1'>64.9</score> 
									</scores>
									<scores scale='Constructive Post' scorelength='1'>
										<score rater_type='1'>28.2</score> 
									</scores>
									<scores scale='Destructive Post' scorelength='1'>
										<score rater_type='1'>52.1</score> 
									</scores>
									<scores scale='Unreliable' scorelength='1'>
										<score rater_type='1'>33.8</score> 
									</scores>
									<scores scale='Overly Analytical' scorelength='1'>
										<score rater_type='1'>64.2</score> 
									</scores>
									<scores scale='Unappreciative' scorelength='1'>
										<score rater_type='1'>42.4</score> 
									</scores>
									<scores scale='Aloof' scorelength='1'>
										<score rater_type='1'>37.3</score> 
									</scores>
									<scores scale='Micro-Managing' scorelength='1'>
										<score rater_type='1'>61.0</score> 
									</scores>
									<scores scale='Self-Centered' scorelength='1'>
										<score rater_type='1'>35.0</score> 
									</scores>
									<scores scale='Abrasive' scorelength='1'>
										<score rater_type='1'>49.5</score> 
									</scores>
									<scores scale='Untrustworthy' scorelength='1'>
										<score rater_type='1'>37.9</score> 
									</scores>
									<scores scale='Hostile' scorelength='1'>
										<score rater_type='1'>24.9</score> 
									</scores>

								</report_scores>
							</scored_data>
							<binary_data persistDB='False' persistFile='True' persistFileFormat='PDF'></binary_data>
						</score_actual_key>
					</data>
				</Score_Response>
			</soap:Body>
		</soap:Envelope>";
?>