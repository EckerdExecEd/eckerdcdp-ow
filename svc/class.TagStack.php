<?php
class TagStack{
	
	var $cnt=-1;
	var $tags=array();
	
	// push another tag on the stack
	function push($tag){
		$this->cnt+=1;
		$this->tags[$this->cnt]=$tag;
	}
	
	// remove the uppermost tag from the stack
	function pop(){
		$rc=$this->top();
		if(false!=$rc){
			$this->cnt-=1;
		}
		return $rc;
	}
	
	// look at the uppermost tag w/o removing it
	function top(){
		return ($this->cnt >= 0)?$this->tags[$this->cnt]:false;
	}
	
	// return the number of elements on the stack
	function count(){
		return $this->cnt;	
	}

}
?>
