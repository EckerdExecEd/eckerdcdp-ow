<?php
require_once "soap/lib/nusoap.php";
require_once "../meta/mailfns.php";
$host=getURLRoot()."/svc/statusrequest.php";
$cl=new soapclient($host);

if($err=$cl->getError()){
	echo $err;
	exit;
}

$bod=getBody();

$rc=$cl->send($bod,$host);

// some debugging stuff
echo "<xmp>\n";
//echo $cl->request."\n\n";
echo $cl->response."\n";
//echo $cl->document."\n";
echo "\n</xmp>";

function getBody(){
	return "<?xml version=\"1.0\"?>
		<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
			<soap:Header>
				<Routing>
					<Login>CCLTest</Login>
					<Password>ccltest010</Password>
					<ClientKey>12345</ClientKey>
					<CustomerNumber>67890</CustomerNumber>
					<ShipTo>1</ShipTo>
					<BillTo>1</BillTo>
				</Routing>
			</soap:Header>
			<soap:Body>
				<Status_Request>
					<inStatusRequest>
						<requestorkey>570725</requestorkey>
						<vendorkey>6704676</vendorkey>
						</inStatusRequest>
					</Status_Request>
			</soap:Body>
			</soap:Envelope>";
}

?>