<?php
require_once "soap/lib/nusoap.php";
require_once "../meta/mailfns.php";
$host=getURLRoot()."/svc/initialrequest.php";
//$host="http://localhost/cdp/svc/initialrequest.php";
$cl=new soapclient($host);

if($err=$cl->getError()){
	echo $err;
	exit;
}

//$bod=getBodyIndividual();
$bod=newBody();

//$bod=$cl->serializeEnvelope($bod);

$rc=$cl->send($bod,$host);

// some debugging stuff
echo "<xmp>\n";
//echo $cl->request."\n\n";
echo $cl->response."\n";
//echo $cl->document."\n";
echo "\n</xmp>";

function getBodyIndividual(){
	$rc="<?xml version=\"1.0\"?>
		<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
		<soap:Header>
			<Routing>
				<Login>CCLTest</Login>
				<Password>ccltest010</Password>
				<ClientKey>12345</ClientKey>
				<CustomerNumber>67890</CustomerNumber>
				<ShipTo>1</ShipTo>
				<BillTo>1</BillTo>
			</Routing>
		</soap:Header>
		<soap:Body>
		<ReportContext>
				<EventID>5707</EventID>
			<EventName>A test program</EventName>
			<EventDate>11/21/2006</EventDate>
			<EventLocation>Greensboro, NC</EventLocation>
			<BudgetCode>12345</BudgetCode>
		</ReportContext>
		<SCORING_RESPONSE>
			<scoring_request>
				<report>
					<instrument_Type>CDPIND</instrument_Type>
					<report_type>3</report_type>
					<!-- will ignore for now -->
					<priority_key></priority_key>
					<request_date>10/19/2006</request_date>
					<report_options>
						<!-- 1=English 8=Spanish -->
						<language>1</language>  
						<output_format>PDF</output_format>
						<return_scores>YES</return_scores>
					</report_options>
					<scoring_options>
						<!-- 1=English 8=Spanish -->
						<instrument_language>1</instrument_language>
						<norm_group></norm_group> 
					</scoring_options>
				</report>
				<individuals>
					<individual> 
						<!-- This is CCLs identifier -->
						<requestorkey>581119</requestorkey>
						<!--  This is CDPs identifier -->
						<vendorkey></vendorkey> 
					</individual>
				</individuals>
				<forms>
					<form>
						<RaterType>1</RaterType>
						<demographics>
							<demographic item=\"1\" name=\"orgname\">bbbb</demographic>
							<demographic item=\"2\" name=\"workphone\">336-999-8888</demographic>
							<demographic item=\"3\" name=\"email\">aaa@bbbc.com</demographic>
							<demographic item=\"4\" name=\"sex\">F</demographic>
							<demographic item=\"5\" name=\"age\">67</demographic>
							<demographic item=\"6\" name=\"ethnicity\">10</demographic>
							<demographic item=\"12\" name=\"ethnexpl\"></demographic>
							<demographic item=\"13\" name=\"degree\">2</demographic>
							<demographic item=\"14\" name=\"orglevel\">5</demographic>
							<demographic item=\"15\" name=\"orgtypea\">1</demographic>
							<demographic item=\"16\" name=\"orgtypeb\">3</demographic>
							<demographic item=\"17\" name=\"function\">18</demographic>
							<demographic item=\"18\" name=\"funcexpl\"></demographic>
							<demographic item=\"19\" name=\"firstname\">Laura</demographic>
							<demographic item=\"20\" name=\"lastname\">Bush</demographic>
						</demographics>
						<responses items=\"99\">
							<response item=\"1\">1</response>
							<response item=\"2\">2</response>
							<response item=\"3\">1</response>
							<response item=\"4\">2</response>
							<response item=\"5\">4</response>
							<response item=\"6\">2</response>
							<response item=\"7\">5</response>
							<response item=\"8\">4</response>
							<response item=\"9\">3</response>
							<response item=\"10\">2</response>
							<response item=\"11\">1</response>
							<response item=\"12\">2</response>
							<response item=\"13\">1</response>
							<response item=\"14\">2</response>
							<response item=\"15\">4</response>
							<response item=\"16\">2</response>
							<response item=\"17\">5</response>
							<response item=\"18\">4</response>
							<response item=\"19\">3</response>
							<response item=\"20\">2</response>
							<response item=\"21\">1</response>
							<response item=\"22\">2</response>
							<response item=\"23\">1</response>
							<response item=\"24\">2</response>
							<response item=\"25\">4</response>
							<response item=\"26\">2</response>
							<response item=\"27\">5</response>
							<response item=\"28\">4</response>
							<response item=\"29\">3</response>
							<response item=\"30\">2</response>
							<response item=\"31\">1</response>
							<response item=\"32\">2</response>
							<response item=\"33\">1</response>
							<response item=\"34\">2</response>
							<response item=\"35\">4</response>
							<response item=\"36\">2</response>
							<response item=\"37\">5</response>
							<response item=\"38\">4</response>
							<response item=\"39\">3</response>
							<response item=\"40\">2</response>
							<response item=\"41\">1</response>
							<response item=\"42\">2</response>
							<response item=\"43\">1</response>
							<response item=\"44\">2</response>
							<response item=\"45\">4</response>
							<response item=\"46\">2</response>
							<response item=\"47\">5</response>
							<response item=\"48\">4</response>
							<response item=\"49\">3</response>
							<response item=\"50\">2</response>
							<response item=\"51\">1</response>
							<response item=\"52\">2</response>
							<response item=\"53\">1</response>
							<response item=\"54\">2</response>
							<response item=\"55\">4</response>
							<response item=\"56\">2</response>
							<response item=\"57\">5</response>
							<response item=\"58\">4</response>
							<response item=\"59\">3</response>
							<response item=\"60\">2</response>
							<response item=\"61\">1</response>
							<response item=\"62\">2</response>
							<response item=\"63\">1</response>
							<response item=\"64\">2</response>
							<response item=\"65\">4</response>
							<response item=\"66\">2</response>
							<response item=\"67\">5</response>
							<response item=\"68\">4</response>
							<response item=\"69\">3</response>
							<response item=\"70\">2</response>
							<response item=\"71\">1</response>
							<response item=\"72\">2</response>
							<response item=\"73\">1</response>
							<response item=\"74\">2</response>
							<response item=\"75\">4</response>
							<response item=\"76\">2</response>
							<response item=\"77\">5</response>
							<response item=\"78\">4</response>
							<response item=\"79\">3</response>
							<response item=\"80\">2</response>
							<response item=\"81\">1</response>
							<response item=\"82\">2</response>
							<response item=\"83\">1</response>
							<response item=\"84\">2</response>
							<response item=\"85\">4</response>
							<response item=\"86\">2</response>
							<response item=\"87\">5</response>
							<response item=\"88\">4</response>
							<response item=\"89\">3</response>
							<response item=\"90\">2</response>
							<response item=\"91\">1</response>
							<response item=\"92\">2</response>
							<response item=\"93\">1</response>
							<response item=\"94\">2</response>
							<response item=\"95\">4</response>
							<response item=\"96\">2</response>
							<response item=\"97\">5</response>
							<response item=\"98\">4</response>
							<response item=\"99\">3</response>
						</responses>
					</form>
				</forms>
			</scoring_request>
		</SCORING_RESPONSE>
		</soap:Body>
		</soap:Envelope>";
	return $rc;
}


function newBody(){
	$rc="<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <soap:Header>
    <Routing>
      <Login>CCLTest</Login>
      <Password>ccltest010</Password>
      <ClientKey>92284</ClientKey>
      <CustomerNumber>312049</CustomerNumber>
      <ShipTo>335704</ShipTo>
      <BillTo>335704</BillTo>
    </Routing>
  </soap:Header>
  <soap:Body>
    <SCORING_RESPONSE>
      <scoring_request>
        <ReportContext>
          <EventID>2002232</EventID>
          <EventName>Debugging</EventName>
          <EventDate>12/27/2006</EventDate>
          <EventLocation>Greensboro, NC</EventLocation>
          <BudgetCode>048-49484</BudgetCode>
        </ReportContext>
        <report>
          <instrument_Type>CDPIND</instrument_Type>
          <report_type>3</report_type>
          <priority_key>1</priority_key>
          <request_date>12/10/2006</request_date>
          <report_options>
            <language>1</language>
            <output_format>PDF</output_format>
            <return_scores>YES</return_scores>
          </report_options>
          <scoring_options>
            <instrument_language>1</instrument_language>
            <norm_group>1</norm_group>
          </scoring_options>
        </report>
        <individuals>
          <individual>
            <requestorkey>2222</requestorkey>
            <vendorkey>7077949</vendorkey>
          </individual>
        </individuals>
        <forms>
          <form>
            <RaterType>1</RaterType>
            <demographics>
              <demographic item=\"1\" name=\"orgname\">Eastridge</demographic>
              <demographic item=\"2\" name=\"workphone\">336-549-5951</demographic>
              <demographic item=\"3\" name=\"email\">jshannon@eastrige.net</demographic>
              <demographic item=\"4\" name=\"sex\">M</demographic>
              <demographic item=\"5\" name=\"age\">33</demographic>
              <demographic item=\"6\" name=\"ethnicity\">9</demographic>
              <demographic item=\"12\" name=\"ethnexpl\">
              </demographic>
              <demographic item=\"13\" name=\"degree\">2</demographic>
              <demographic item=\"14\" name=\"orglevel\">
              </demographic>
              <demographic item=\"15\" name=\"orgtypea\">
              </demographic>
              <demographic item=\"16\" name=\"orgtypeb\">
              </demographic>
              <demographic item=\"17\" name=\"function\">
              </demographic>
              <demographic item=\"18\" name=\"funcexpl\">
              </demographic>
              <demographic item=\"19\" name=\"firstname\">Jim</demographic>
              <demographic item=\"20\" name=\"lastname\">Brady</demographic>
            </demographics>
            <responses items=\"114\">
              <response item=\"1\">2</response>
              <response item=\"2\">2</response>
              <response item=\"3\">2</response>
              <response item=\"4\">2</response>
              <response item=\"5\">2</response>
              <response item=\"6\">2</response>
              <response item=\"7\">2</response>
              <response item=\"8\">2</response>
              <response item=\"9\">2</response>
              <response item=\"10\">2</response>
              <response item=\"11\">2</response>
              <response item=\"12\">2</response>
              <response item=\"13\">2</response>
              <response item=\"14\">2</response>
              <response item=\"15\">2</response>
              <response item=\"16\">2</response>
              <response item=\"17\">2</response>
              <response item=\"18\">2</response>
              <response item=\"19\">2</response>
              <response item=\"20\">2</response>
              <response item=\"21\">2</response>
              <response item=\"22\">2</response>
              <response item=\"23\">2</response>
              <response item=\"24\">2</response>
              <response item=\"25\">2</response>
              <response item=\"26\">2</response>
              <response item=\"27\">2</response>
              <response item=\"28\">2</response>
              <response item=\"29\">2</response>
              <response item=\"30\">2</response>
              <response item=\"31\">2</response>
              <response item=\"32\">2</response>
              <response item=\"33\">2</response>
              <response item=\"34\">2</response>
              <response item=\"35\">2</response>
              <response item=\"36\">2</response>
              <response item=\"37\">2</response>
              <response item=\"38\">2</response>
              <response item=\"39\">2</response>
              <response item=\"40\">2</response>
              <response item=\"41\">2</response>
              <response item=\"42\">2</response>
              <response item=\"43\">2</response>
              <response item=\"44\">2</response>
              <response item=\"45\">2</response>
              <response item=\"46\">2</response>
              <response item=\"47\">2</response>
              <response item=\"48\">2</response>
              <response item=\"49\">2</response>
              <response item=\"50\">2</response>
              <response item=\"51\">2</response>
              <response item=\"52\">2</response>
              <response item=\"53\">2</response>
              <response item=\"54\">2</response>
              <response item=\"55\">1</response>
              <response item=\"56\">1</response>
              <response item=\"57\">1</response>
              <response item=\"58\">1</response>
              <response item=\"59\">1</response>
              <response item=\"60\">1</response>
              <response item=\"61\">1</response>
              <response item=\"62\">1</response>
              <response item=\"63\">1</response>
              <response item=\"64\">1</response>
              <response item=\"65\">1</response>
              <response item=\"66\">1</response>
              <response item=\"67\">1</response>
              <response item=\"68\">1</response>
              <response item=\"69\">1</response>
              <response item=\"70\">1</response>
              <response item=\"71\">1</response>
              <response item=\"72\">1</response>
              <response item=\"73\">1</response>
              <response item=\"74\">1</response>
              <response item=\"75\">1</response>
              <response item=\"76\">1</response>
              <response item=\"77\">1</response>
              <response item=\"78\">1</response>
              <response item=\"79\">1</response>
              <response item=\"80\">1</response>
              <response item=\"81\">1</response>
              <response item=\"82\">1</response>
              <response item=\"83\">1</response>
              <response item=\"84\">1</response>
              <response item=\"85\">1</response>
              <response item=\"86\">1</response>
              <response item=\"87\">1</response>
              <response item=\"88\">1</response>
              <response item=\"89\">1</response>
              <response item=\"90\">1</response>
              <response item=\"91\">1</response>
              <response item=\"92\">1</response>
              <response item=\"93\">1</response>
              <response item=\"94\">1</response>
              <response item=\"95\">1</response>
              <response item=\"96\">1</response>
              <response item=\"97\">1</response>
              <response item=\"97\">1</response>
              <response item=\"98\">1</response>
              <response item=\"99\">1</response>
              <response item=\"100\">1</response>
              <response item=\"101\">1</response>
              <response item=\"102\">1</response>
              <response item=\"103\">1</response>
              <response item=\"104\">1</response>
              <response item=\"105\">1</response>
              <response item=\"106\">1</response>
              <response item=\"107\">1</response>
              <response item=\"108\">1</response>
              <response item=\"109\">1</response>
              <response item=\"110\">1</response>
              <response item=\"111\">1</response>
              <response item=\"112\">1</response>
              <response item=\"113\">1</response>
              <response item=\"114\">1</response>
            </responses>
          </form>
        </forms>
      </scoring_request>
    </SCORING_RESPONSE>
  </soap:Body>
</soap:Envelope>";
	return $rc;
}

?>