<?php
require_once "class.TagStack.php";
require_once "class.Credentials.php";
require_once "../meta/dbfns.php";

class ReportParser{
	var $tags;
	var $creds;
	var $status=0;
	var $vendorkey;
	var $requestorkey;
	var $clientid;
	var $rpt;
	var $tid;
	
	// constructor
	function ReportParser(){
		$this->tags= new TagStack();
		$this->creds=new Credentials();
	}

	
	// The three required parser functions
	function start_element($parser,$tag,$attributes){
		// as long aa we habve no errors
		if(0==$this->status){
			$this->tags->push($tag);
			$this->attr=$attributes;
			// debugging
//			echo "\nStart of: ".$this->tags->top()." nesting ".$this->tags->count();
		}
	}
	
	function end_element($parser,$tag){
		// as long as we have no errors
		if(0==$this->status){
			if("soap:Envelope"==$tag){
				// the entire message has been parsed
				// process the collected data
				$this->process();
			}
//			echo "\nEnd of: ".$this->tags->top()." nesting ".$this->tags->count();
			unset($this->attr);
			$this->tags->pop();
		}
	}
	
	function character_data($parser,$data){
		// as long aa we have no errors
		if(0==$this->status){
			$data=trim($data);
			$data=mysql_escape_string($data);
			$tag=$this->tags->top();
			if("Login"==$tag){
				$this->creds->setUID($data);
				$this->checkCredentials();
			}
			elseif("Password"==$tag){
				$this->creds->setPWD($data);
				$this->checkCredentials();
			}
			elseif("requestorkey"==$tag){
				$this->requestorkey=$data;
			}
			elseif("vendorkey"==$tag){
				$this->vendorkey=$data;
			}
			elseif("clientid"==$tag){
				$this->clientid=$data;	
			}
		}
	}

	function checkCredentials(){
		if($this->creds->hasCredentials()){
			$this->status=$this->creds->verify();
		}
	}

	function process(){
		// if everthing is ok
		if($this->status==0){
			// 1. Determine the PID and TID based on CID
			$conn=dbConnect();
			$query="select a.PID,a.TID from PROGINSTR a,CANDIDATE b where a.PID=b.PID and b.CID=".$this->vendorkey;
			$rs=mysql_query($query);
//			echo "$query \n";
			if(false===$rs||0==mysql_num_rows($rs)){
				// the query failed
				$this->status=810;
			}
			else{
				$row=mysql_fetch_row($rs);
				$pid=$row[0];
				$tid=$row[1];
				$cid=$this->vendorkey;
				//echo "******".$tid."******";				
				// we now have everything we need to process a report
				// select type of report we're going to retrun
				if(3==$tid){
					require_once "class.SelfOnlyReport.php";
					$this->rpt=new SelfOnlyReport($cid,$pid);
				}
				else{
					require_once "class.Report.php";
					$this->rpt=new Report($cid,$pid);
				}
				// generate the report
				$this->rpt->process();
				// check the result
				$this->status=$this->rpt->getStatus();
				// save the type of report here 
				$this->tid=$tid;
			}
		}
	}
	
	function showError(){
		unset($this->report);
		return "<?xml version=\"1.0\"?>
			<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">				
				<soap:Body>
					<Score_Response>
						<data>
							<vendorkey>".$this->vendorkey."</vendorkey>
							<requestorkey>".$this->requestorkey."</requestorkey>
							<statuscode>".$this->status."</statuscode>
						</data>
					</Score_Response>
				</soap:Body>
				</soap:Envelope>";	
	}

	function soapResponse(){
		if(0!=$this->status){
			return $this->showError();
		}
		// If we're here everything went OK, set status to 1
		$this->status=1;
		return "<?xml version=\"1.0\"?>
				<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">				
				<soap:Body>
					<Score_Response>
							<data>
								<requestorkey>".$this->requestorkey."</requestorkey>
								<vendorkey>".$this->vendorkey."</vendorkey>
								<score_actual_key>
									<scored_data persistDB='True' persistFile='False' persistFileFormat='XML'>
										<report_scores>
										".$this->rpt->getScales()."
										</report_scores>
									</scored_data>
									<binary_data>".$this->rpt->getPDF()."</binary_data>
								</score_actual_key>
							</data>
					</Score_Response>
			</soap:Body>
		</soap:Envelope>";
	}

}
?>
