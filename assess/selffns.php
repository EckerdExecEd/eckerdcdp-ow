<?php
/*=============================================================================*
* 04-25-2005 TRM: Modified to handle self only surveys.
* 11-2-2005 TRM: Modified for multilingual support 
*=============================================================================*/
// Functions for the self survey
require_once "../meta/dbfns.php";
require_once "../meta/survey.php";

// Write the page header for the survey
function writeHeader($lid="1"){
	echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title>Internet Assessment System - Self</title>";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">";
	echo "<link title=\"standard\" media=\"screen\" href=\"../index_files/eckerd-1.css\" type=\"text/css\" rel=\"stylesheet\">";
	echo "<style>.ivanC10400569321381 {";
	echo "VISIBILITY: hidden; POSITION: absolute";
	echo "}</style></head>";
    echo "<body class=\".body\">";
    echo "<table cellspacing=\"0\" cellpadding=\"0\" width=\"750\" border=\"0\">";
    echo "<tbody>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"top\" align=\"right\" bgcolor=\"#4f8d97\" height=\"22\"><td colspan=3></td></tr>";
	echo "<tr valign=\"top\" align=\"left\" height=\"1\">";
    echo "<td colspan=3><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td></tr>";
    echo "<tr valign=\"center\" height=\"42\" bgcolor=\"#253355\">";
    echo "<td align=\"left\"><img height=\"64\" src=\"../index_files/eclogotop.gif\" width=\"155\" border=\"0\"></td>";
    echo "<td><img height=\"1\" src=\"../index_files/pixel.gif\" width=\"1\" border=\"0\"></td>";
    echo "<td align=\"right\"><img height=\"64\" src=\"../index_files/ldi-title.gif\" width=\"430\" border=\"0\"></td></tr>";
    echo "</table><br>&nbsp;</br>"; 
}

// Write the page footer for the survey
function writeFooter($lid="1"){
	echo sprintf("%c",169)." 2005 Eckerd College</body></html>";
}

// Write the navigation bar
function writeNavigation($page,$frm,$tid,$lid="1"){
	$pic=array();
	$lnk=array();
	$whither="selfexit.php";
	if($tid=="3"){
		$whither="selfonlyexit.php";
	}
	// handle multiple languages here
	// Deafult is English
	$pE="../images/prevE.gif";
	$nE="../images/nextE.gif";
	$pD="../images/prevD.gif";
	$nD="";
	$svX="../images/savex.gif";
	if("8"==$lid){
		// Spanish here
		$pE="../images/spPrevE.gif";
		$nE="../images/spNextE.gif";
		$pD="../images/spPrevD.gif";
		$nD="";
		$svX="../images/spSavex.gif";
	}
	
	$pic[0]=$pE;
	$pic[1]="../images/1f.gif";
	$pic[2]="../images/2f.gif";
	$pic[3]="../images/3f.gif";
	$pic[4]="../images/4f.gif";
	$pic[5]="../images/5f.gif";
	$pic[6]="../images/6f.gif";
	$pic[7]="../images/7f.gif";
	if($tid=="3"){
		$pic[8]=$nE;
	}
	else{
		$pic[8]="../images/8f.gif";
		$pic[9]=$nE;
	}

	$pp=$page-1;
	$np=$page+1;
	
	$lnk[0]="javascript:$frm.page.value='$pp';$frm.submit();";
	$lnk[1]="javascript:$frm.page.value='1';$frm.submit();";
	$lnk[2]="javascript:$frm.page.value='2';$frm.submit();";
	$lnk[3]="javascript:$frm.page.value='3';$frm.submit();";
	$lnk[4]="javascript:$frm.page.value='4';$frm.submit();";
	$lnk[5]="javascript:$frm.page.value='5';$frm.submit();";
	$lnk[6]="javascript:$frm.page.value='6';$frm.submit();";
	$lnk[7]="javascript:$frm.page.value='7';$frm.submit();";
	if($tid=="3"){
		// next page
		$lnk[8]="javascript:$frm.page.value='$np';$frm.submit();";
	}
	else{
		$lnk[8]="javascript:$frm.page.value='8';$frm.submit();";
		$lnk[9]="javascript:$frm.page.value='$np';$frm.submit();";
	}
	
	// enable images and links based on the page we're on
	switch($page){
		case 1:
			$pic[0]=$pD;
			$pic[1]="../images/1n.gif";
			$lnk[0]="";
			break;
		case 2:
			$pic[2]="../images/2n.gif";
			break;
		case 3:
			$pic[3]="../images/3n.gif";
			break;
		case 4:
			$pic[4]="../images/4n.gif";
			break;
		case 5:
			$pic[5]="../images/5n.gif";
			break;
		case 6:
			$pic[6]="../images/6n.gif";
			break;
		case 7:
			$pic[7]="../images/7n.gif";
			if($tid=="3"){
				// turn off next
				$pic[8]="";
				$lnk[8]="";
			}
			break;
		case 8:
			if($tid=="1"){
				$pic[8]="../images/8n.gif";
				// turn off next
				$pic[9]="";
				$lnk[9]="";
			}
			break;
		default:
	}
	
	echo "<table border=0 cellspacing=0 cellpadding=0><tr>";
	$howMany=(8==$page)?9:10;
	if($tid=="3"){
		$howMany=(7==$page)?8:9;
	}
	
	for($i=0;$i<$howMany;$i++){
		echo "<td valign=top onClick=\"".$lnk[$i]."\">";
		echo "<img src='$pic[$i]'>";
		echo "</td>";
	}

	$lastPage=9;
	if($tid=="3"){
		$lastPage=8;
	}
	if($page!=$lastPage){
		echo "<td valign=top onClick=\"javascript:$frm.what.value='save';$frm.action='$whither';$frm.submit();\">&nbsp;&nbsp;<img src='$svX'></td>";
	}
	//echo $svX;
	echo "</tr></table>";
}

function writeSelfSurvey($page,$cid,$rid,$frm,$tid,$lid="1"){
	$lastPage=($tid=="3")?7:8;
	if(1==$page){
		showSelfDemographics($cid,$tid,$lid);	
	}
	elseif($page>=2 and $page<$lastPage){
		showSurvey($rid,$tid,$lid,$page);
	}
	elseif($page==$lastPage){
		showByeBye($rid,$frm,$tid,$lid);
	}
}

function showByeBye($rid,$frm,$tid,$lid="1"){
	$whither="selfexit.php";
	if($tid=="3"){
		$whither="selfonlyexit.php";
	}
	$cnt=getNoAnswerCount($rid,$tid);
	if("3"==$tid){
		$flid=getFLID("assess","selffns.php");
		$mlTxt=getMLText($flid,$tid,$lid);

		if($cnt>3){
			echo "<p><u>$mlTxt[1]</u> </p><p>&nbsp;<br>$mlTxt[2] $cnt $mlTxt[3] 99</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>$mlTxt[4]</u></p><p>".implode($unAnsw,",")."</p>";
		}
		elseif($cnt>0){
			echo "<p><u>$mlTxt[7]</u> </p><p>&nbsp;<br>$mlTxt[2] $cnt $mlTxt[3] 99</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>$mlTxt[4]</u></p><p>".implode($unAnsw,",")."</p>";
			echo "<p>$mlTxt[8]<br>$mlTxt[9]</p><p>";
			echo "<input type='button' value='$mlTxt[6]' onClick=\"javascript:$frm.what.value='finish';$frm.action='$whither';$frm.submit();\">";
			echo "</p><p>&nbsp;</p>";
		}
		else{
			echo "<p> $mlTxt[5]</p><p>";
			echo "<input type='button' value='$mlTxt[6]' onClick=\"javascript:$frm.what.value='finish';$frm.action='$whither';$frm.submit();\">";
			echo "</p><p>&nbsp;</p>";
		}
	}
	else{
		if($cnt>7){
			echo "<p><u>Please address the following issues:</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 114</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
		}
		elseif($cnt>0){
			echo "<p><u>Are you sure you want to complete the survey?</u> </p><p>&nbsp;<br>You have $cnt unanswered questions of 114</p>&nbsp;</p>";
			$unAnsw=getUnansweredItems($rid,$tid);
			echo "<p><u>Unanswered Questions:</u></p><p>".implode($unAnsw,",")."</p>";
			echo "<p> It is strongly recommended that you go back and answer all questions.<br>However, you may click on the 'Submit' button below to complete the survey.</p><p>";
			echo "<input type='button' value='Submit' onClick=\"javascript:$frm.what.value='finish';$frm.action='$whither';$frm.submit();\">";
			echo "</p><p>&nbsp;</p>";
		}
		else{
			echo "<p> Click on the 'Submit' button below to complete the survey.</p><p>";
			echo "<input type='button' value='Submit' onClick=\"javascript:$frm.what.value='finish';$frm.action='$whither';$frm.submit();\">";
			echo "</p><p>&nbsp;</p>";
		}
	}
}

?>
