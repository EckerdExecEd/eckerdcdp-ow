<?php 
require_once "../meta/dbfns.php";
$msg="";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$conid=$_POST['conid'];
// do a "fake login" so that we can impersonate the Candidate
$cid=$_POST['cid'];
$conn=dbConnect();
$pin=$_POST['pin'];
$uid=$_POST['uid'];
$query="select FNAME,LNAME from CANDIDATE a where a.CID=$cid";
$rs=mysql_query($query);
$row=mysql_fetch_row($rs)
	or die ("Invalid candidate id");
$_SESSION['cid']=$cid;
$_SESSION['cname']=$row[0]." ".$row[1]; 
$msg="You are about to act on behalf of Candidate ".$_SESSION['cname']."&nbsp;&nbsp;<small>(Candidate ID=".$_SESSION['cid'].")</small><br>";
$msg=$msg."<u>Please Note:</u> If you choose to continue you will be <b><i>logged out</i></u> from the consultant site and logged in<br>to the candidate site as the above Candidate.<br>";
$msg=$msg."After you are done acting as the above candidate you will have to return to the consultant site and<br>log in to perform other functions.";
require_once("../cons/consfn.php");
writeHead("Conflict Dynamics Profile - Consultants",false);
writeBody("Candidate Menu for Consultant",$msg);
$urls=array('selfonly.php','../cons/home.php');
$txts=array('Continue','Go back to Consultant Functions');
echo "<form name=\"menuform\" method=POST>";
echo "<input type='hidden' name='pin' value='$pin'>";
echo "<input type='hidden' name='uid' value='$uid'>";
menu($urls,$txts,"menuform");
echo "</form>";
writeFooter(false);
?>

