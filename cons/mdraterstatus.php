<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
session_start();

if (empty($_SESSION['conid'])){
    header('HTTP/1.0 401 Not Found');
    echo "<h1>401 Unauthorized</h1>";
    echo "No credentials provided for the page that you have requested.";
    exit();
}
date_default_timezone_set('America/New_York');


$pid =(isset($_REQUEST['pid'])&&$_REQUEST['pid']!="")? $_REQUEST['pid'] : 202048;

// Include PHPExcel 
require_once '../classes/PHPExcel.php';
// Include Candidate functions
require_once "../meta/candidate.php";
$raters=ckProgramCompletionStatus($pid);
//die(print_r($raters));
if(count($raters)< 1)
  die("Not enough raters to generate a report for program $pid");
  
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("www.onlinecdp.org")
							 ->setLastModifiedBy("www.onlinecdp.org")
							 ->setTitle("Mercer Rater Status Report")
							 ->setSubject("Mercer Rater Status Report")
							 ->setDescription("Status report for Mercer's CDP 360 program raters")
							 ->setKeywords("CDP360 Conflict Dynamics")
							 ->setCategory("Program $pid");

$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial'); 
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
							 
// Get Sheet 1 - Totals
$sheet1 = $objPHPExcel->setActiveSheetIndex(0);
  sheet1title($sheet1,$pid);
  sheet1columnheaders($sheet1); 
           
//==============================================================================
// Add worksheet 'Raters'
$sheet2 = $objPHPExcel->createSheet();
  sheet2columnheaders($sheet2);
  $rSummary= sheet2data($sheet2, $raters, $pid);
  
  //Set each column to autosize
  foreach(range('A','H') as $columnID) {
    $sheet2->getColumnDimension($columnID)->setAutoSize(true);
  }
//==============================================================================

  sheet1CandidateRows($sheet1,$rSummary,$pid);
  //Set each column to autosize
  foreach(range('A','N') as $columnID) {
    $sheet1->getColumnDimension($columnID)->setAutoSize(true);
  }
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client�s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="raterstatus'.$pid.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

function sheet1title($sheet1,$pid){
  //Set Cells for center alignment
  $sheet1->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet1->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  //Set Cells to be Bold
  $sheet1->getStyle('A2')->getFont()->setBold(true);
  $sheet1->getStyle('A3')->getFont()->setBold(true);
  //Set Font size for Title
  $sheet1->getStyle('A2')->getFont()->setSize(16);  
  //Merge Cells  
  $sheet1->mergeCells('A2:J2');
  $sheet1->mergeCells('A3:J3');
  //Place text in cells
  $sheet1->setCellValue('A2', 'Status Report for program '.$pid);  
  $sheet1->setCellValue('A3', date('m/d/Y g:iA')); 
  // Name initial worksheet 'Totals'
  $sheet1->setTitle('Totals');      
}

function sheet1columnheaders($sheet1){
  //Rows 5 & 6, Columns A-J
  //Set Cells for center alignment
  $sheet1->getStyle('B5:J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet1->getStyle('B6:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  //Set Cells to be Bold
  $sheet1->getStyle('A5:J5')->getFont()->setBold(true); 
  $sheet1->getStyle('A6:J6')->getFont()->setBold(true);
  //Set background color to gray
  $sheet1->getStyle('A5:J5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
  $sheet1->getStyle('A6:J6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
  $sheet1->getStyle('A5:J5')->getFill()->getStartColor()->setRGB('C0C0C0'); 
  $sheet1->getStyle('A6:J6')->getFill()->getStartColor()->setRGB('C0C0C0');  
  //Merge Certain Cells 
  $mCells=array('C5:D5','E5:F5','G5:H5','I5:J5'); 
  foreach($mCells as $cells)
    $sheet1->mergeCells($cells);
  //Place text in cells
  $cTxt=array( 'A5'=>' ','A6'=>'Participant','B5'=>'Requirements','B6'=>'Met',
               'C5'=>'Self','C6'=>'Total','D6'=>'Complete','E5'=>'Manager',
               'E6'=>'Total','F6'=>'Complete','G5'=>'Peer','G6'=>'Total',
               'H6'=>'Complete','I5'=>'Direct Report','I6'=>'Total','J6'=>'Complete');
  foreach($cTxt as $cell=>$val)              
    $sheet1->setCellValue($cell,$val);
    
  //Set borders for column headers
  
  $styleArray = array(
    'borders' => array(
      'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
      )
    )
  );
  $bCells=array('A5:A6','B5:B6','C5:D6','E5:F6','G5:H6','I5:J6');
  foreach($bCells as $cells)
    $sheet1->getStyle($cells)->applyFromArray($styleArray);
  
  unset($styleArray);

}

function sheet1CandidateRows($sheet1,$rSummary,$pid,$start=7){
  $colors=array(1=>'339966',2=>'FFCC00',3=>'FF0000');
  $y=$start;
  foreach($rSummary as $row){
    $x=0;
    // Display Participant Name
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['name']); $x++;     
   // Display report qualification
    $sheet1->getStyle('B'.$y)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    if($row['cplt1']>0){
      if(($row['cplt2']>0)&&($row['cplt3']>2)&&($row['cplt4']>2)){
        $ok=1;
      }else{
        $ok=2;
      }
    }else{
      $ok=3;
    }
    $sheet1->getStyle('B'.$y)->getFill()->getStartColor()->setRGB($colors[$ok]);     
    $x++;
    // Display Self Status
    $red='FF0000';
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['ttl1']); 
    if($row['ttl1']<1){ $sheet1->getStyle('C'.$y)->getFont()->getColor()->setRGB($red); } $x++;   
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['cplt1']); 
    if($row['cplt1']<1){ $sheet1->getStyle('D'.$y)->getFont()->getColor()->setRGB($red); } $x++; 
    // Display Manager Status 
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['ttl2']); 
    if($row['ttl2']<1){ $sheet1->getStyle('E'.$y)->getFont()->getColor()->setRGB($red); } $x++;       
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['cplt2']);
    if($row['cplt2']<1){ $sheet1->getStyle('F'.$y)->getFont()->getColor()->setRGB($red); } $x++;     
    // Display Peer Status 
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['ttl3']); 
    if($row['ttl3']<3){ $sheet1->getStyle('G'.$y)->getFont()->getColor()->setRGB($red); }  $x++;    
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['cplt3']);
    if($row['cplt3']<3){ $sheet1->getStyle('H'.$y)->getFont()->getColor()->setRGB($red); } $x++;     
    // Display Direct Reports Status 
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['ttl4']); 
    if($row['ttl4']<3){ $sheet1->getStyle('I'.$y)->getFont()->getColor()->setRGB($red); }  $x++;  
    $sheet1->setCellValueByColumnAndRow($x,$y,$row['cplt4']); 
    if($row['cplt4']<3){ $sheet1->getStyle('J'.$y)->getFont()->getColor()->setRGB($red); } $x++;                         
    $y++;
  }

  // Set All rows to center
  $sheet1->getStyle('B'.$start.':J'.($y-1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  
  $styleArray = array(
    'borders' => array(
      'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
      )
    )
  );
  $bCells=array('A:A','B:B','C:D','E:F','G:H','I:J');
  foreach($bCells as $z){
    $t=explode(":",$z);
    $cells=$t[0].$start.':'.$t[1].($y-1);
    //die($cells);
    $sheet1->getStyle($cells)->applyFromArray($styleArray);
  }
  unset($styleArray);
  
  // Draw Key information
  $kTxt=array(1=>'Individual Report has all minimum qualifications met (no categories missing)',
              2=>'Individual Report may be requested but does not meet all minimum qualifications',
              3=>'Individual Report cannot be generated due to incomplete self assessment');  
    foreach($kTxt as $i=>$txt){
      $k='A'.($y+$i);
      $sheet1->getStyle($k)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $sheet1->getStyle($k)->getFill()->getStartColor()->setRGB($colors[$i]);
      //Merge Cells
      $sheet1->mergeCells('B'.($y+$i).':J'.($y+$i));
      //Display Key Text
      $sheet1->setCellValue('B'.($y+$i), $txt); 
    } 
/*  */                      
}

function sheet2columnheaders($sheet2){
  $cols=array('A1'=>'SPSID','B1'=>'NAME','C1'=>'EMAIL','D1'=>'CATEGORY','E1'=>'COMP','F1'=>'START DATE','G1'=>'COMPLETE DATE');
  $sheet2->setTitle('Raters');
  $sheet2->getStyle('A1:G1')->getFont()->setBold(true);
  $sheet2->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
  $sheet2->getStyle('A1:G1')->getFill()->getStartColor()->setRGB('C0C0C0');      
  foreach($cols as $cell=>$val){
    $sheet2->setCellValue($cell,$val);
  }
  $styleArray = array(
    'borders' => array(
      'bottom' => array(
        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
      )
    )
  );
  $sheet2->getStyle('A1:G1')->applyFromArray($styleArray);
  unset($styleArray);
}

function sheet2data($sheet2, $raters, $pid, $start=2){
	$cat=array("","Self","Manager","Peer","Direct Report");
  $end=$start + count($raters);
  $y=$start;
  $lSPSID=0; //Last SPSID
  $i=0;
  $rSummary=array();
  //die(print_r($raters));
  foreach($raters as $rater){
    if($rater['participantID']!=$lSPSID){
      $lSPSID = $rater['participantID']; 
      $rSummary[$i]=array('rs'=>$y,'re'=>'','name'=>$rater['RNAME'],'ttl1'=>0,'cplt1'=>0,'ttl2'=>0,'cplt2'=>0,'ttl3'=>0,'cplt3'=>0,'ttl4'=>0,'cplt4'=>0);
      if($y!=$start){ $rSummary[$i-1]['re']=$y - 1; }
      $n=$i;
      $i++;
    }
  
    
    //Display SPSID
    $sheet2->setCellValueByColumnAndRow(0,$y,$rater['participantID']);
    // Display Rater Name 
    $sheet2->setCellValueByColumnAndRow(1,$y,$rater['RNAME']);
    // Display Email    
    $sheet2->setCellValueByColumnAndRow(2,$y,$rater['EMAIL']);
    // Display Rater Category
    $sheet2->setCellValueByColumnAndRow(3,$y,$cat[$rater['CATID']]);
    $rSummary[$n]['ttl'.$rater['CATID']]++; 
    // Display Completion Status
    $sheet2->setCellValueByColumnAndRow(4,$y,$rater['COMPLETE']); 
    $rSummary[$n]['cplt'.$rater['CATID']]+=($rater['COMPLETE']=='Y')? 1 : 0;
    // Display the Start Date
    $sheet2->setCellValueByColumnAndRow(5,$y,$rater['STARTDT']);     
    // Display the End Date
    $sheet2->setCellValueByColumnAndRow(6,$y,$rater['ENDDT']);
/* */                        
   $y++; 
  }
  $rSummary[$i-1]['re']=$end-1;
  
  //die(print_r($rSummary));
  $clrs=array(1=>"C6EFCE",2=>"FFEB9C",3=>"FFC7CE");
  $styleArray = array(
    'borders' => array(
      'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_MEDIUM
      )
    )
  );
   
  //Loop through summary array and set bacground color and border for rater groups. 
  foreach($rSummary as $row){
    if($row['cplt1']>0){
      if(($row['cplt2']>0)&&($row['cplt3']>2)&&($row['cplt4']>2)){
        $ok=1;
      }else{
        $ok=2;
      }
    }else{
      $ok=3;
    }
    $rs=$row['rs']; $re=$row['re'];
    if(($rs > 0) && ($re > 0)){
      $sheet2->getStyle('A'.$rs.':G'.$re)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $sheet2->getStyle('A'.$rs.':G'.$re)->getFill()->getStartColor()->setRGB($clrs[$ok]);  
    }else
      die(print_r($row));
   //Set Border   
   $sheet2->getStyle('A'.$rs.':G'.$re)->applyFromArray($styleArray);   
  }
  

  // Display the session/date/time of report
  $sheet2->mergeCells('A'.$end.':G'.$end);
  $sheet2->getStyle('A'.$end)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $sheet2->getStyle('A'.$end)->getFont()->setSize(8);  
  $sheet2->setCellValue('A'.$end, "Report based on program $pid data as of ". date('g:ia T m/d/Y'));  
  $sheet2->getStyle('A'.$end)->getFont()->getColor()->setRGB('A5A5A5');
  
  // Display Color Code descriptions
  $lbls=array(1=>"Individual Report has all minimum qualifications met (no categories missing)",
              2=>"Individual Report may be requested but does not meet all minimum qualifications (see qualifications below)",
              3=>"Individual Report cannot be generated due to incomplete self assessment");
  $n=$end+2;
 
  foreach($clrs as $k => $clr){
    $sheet2->getStyle('A'.$n)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $sheet2->getStyle('A'.$n)->getFill()->getStartColor()->setRGB($clr); 
    $sheet2->mergeCells('B'.$n.':G'.$n);
    $sheet2->setCellValue('B'.$n, $lbls[$k]);   
    $n++;       
  }
    $n++;
  $styleArray['borders']['outline']['style'] = PHPExcel_Style_Border::BORDER_THIN;
  $reqs=array(1=>'CDP Standard Report qualifications by category:',			
              2=>'Self - Self must complete assessment before report may be produced.',			
              3=>'Manager - 1 manager must complete their assessment to be included in the report.',			
              4=>'Peer - 3 Peers must complete before this category will appear in the report.',			
              5=>'Direct Report - 3 DRs must complete before this will appear in the report.');
  foreach($reqs as $k => $req){
    $sheet2->mergeCells('A'.$n.':D'.$n);
    if($k==1){
      $sheet2->getStyle('A'.$n.':D'.$n)->getFont()->setBold(true);
      $sheet2->getStyle('A'.$n.':D'.$n)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $sheet2->getStyle('A'.$n.':D'.$n)->getFill()->getStartColor()->setRGB('C0C0C0');
      $sheet2->getStyle('A'.$n.':D'.$n)->applyFromArray($styleArray);       
    }
    $sheet2->setCellValue('A'.$n, $req);       
    $n++;
  }
  $sheet2->getStyle('A'.($n-5).':D'.($n-1))->applyFromArray($styleArray);
  unset($styleArray);
  	
  return $rSummary; 
 
}
				 
?>