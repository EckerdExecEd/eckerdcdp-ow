<?php
$msg="";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$conid=$_SESSION['conid'];
require_once "../meta/program.php";
require_once "../meta/candidate.php";
$pid=$_POST['pid'];
$tid=$_POST['tid'];
if(!($pgmData=pgmInfo($pid)))
    die("Invalid program code");

$msg="";

require_once "consfn.php";

if("del"==$_POST['what']){
	$cid=$_POST['cid'];
	$conid=$_SESSION['conid'];
	if(!deleteCandidate($cid,$conid)){
		$msg="<font color='aa0000'>Error deleting candidate</font>";
	}
}
elseif("add"==$_POST['what']){
    $fnam=addslashes($_POST['fnam']);
    $lnam=addslashes($_POST['lnam']);
    $email=addslashes($_POST['email']);
    $conid=$_SESSION['conid'];
	$hasOth="Y";
	if($tid=="3"){
		// self only
		$hasOth="N";
	}
	// language support
	$lid=addslashes($_POST['lid']);
	if(strlen($lid)<1){
		$lid="1";
	}
    $candData=array($pid,$fnam,$lnam,$email,"Y",$hasOth,$conid,$lid);
    if(checkCandEmail($email,$pid)){
		$i=candInsert($candData);
        if($i){
			$msg="<font color='#00aa00'>Successfully added $fnam $lnam ($email)<font>";
		}
		else{
			$msg="<font color='#aa0000'>Error adding candidate.</font>";
		}
    }
    else{
		$msg="<font color='#aa0000'>Email address already exists.</font>";
    }
}
elseif("send"==$_POST['what']){
    $conid=$_SESSION['conid'];
	if($tid=="1"){
		// 360
		$msg=sendInitialEmailToCandidates($pid,$conid,"I");
	}
	elseif($tid=="3"){
		// Self only
		$msg=sendInitialEmailToCandidates($pid,$conid,"S");
	}
}

writeHead("Conflict Dynamics Profile - Consultants",false);
writeBody("Candidate Management for Program:<br>'$pgmData[1]' (ID:$pgmData[0])",$msg);
?>
<script type="text/javascript">
function verifyDelete(){
var answer = confirm("Are you sure you want to delete this candidate?")
if(answer) document.frm1.submit();
else return false;
}
</script>
<form name="frm1" action="candhome.php" method=POST>
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="tid" value="<?=$tid?>">
<input type="hidden" name="cid" value="">
<input type="hidden" name="uid" value="">
<input type="hidden" name="pin" value="">
<input type="hidden" name="what" value="">
<input type="hidden" name="conid" value="<?=$conid?>">
<table border=1>
<tr>
<td bgcolor="#dddddd"><small>First Name</small></td><td bgcolor="#dddddd"><small>Last Name</small></td><td bgcolor="#dddddd"><small>Email</small></td><td bgcolor="#dddddd"><small>Language</small>

</td><td bgcolor="#dddddd"><small>Actions</small></td>
</tr>
<tr>
<td><input type="text" name="fnam" value="" maxlength="20"></td>
<td><input type="text" name="lnam" value="" maxlength="20"></td>
<td><input type="text" name="email" value="" maxlength="50"></td>
<td><?=languageBox($tid)?></td>

<td><input type="button" value="Add Candidate" onClick="javascript:frm1.what.value='add';frm1.submit();"></td>
</tr>
<tr>
<td bgcolor="#dddddd"><small>Name</small></td><td bgcolor="#dddddd"><small>Email</small></td><td bgcolor="#dddddd"><small>PIN</small></td><td bgcolor="#dddddd"><small>Language</small></td><td bgcolor="#dddddd"><small>Actions</small></td>
</tr>
<?php
listCandidates($pid,"frm1",$tid);
?>
</table>
<br>
When you have added your candidates, please <input type=button onClick="javascript:frm1.what.value='send';frm1.submit();" value="Click here"><br> to send initial email to the candidates.
<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"frm1");
writeFooter(false);
?>

