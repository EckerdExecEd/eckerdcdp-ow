<?php 
require_once "../meta/groups.php";
require_once "../meta/mailfns.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$pid=$_POST['pid'];
$tid=$_POST['tid'];
$msg="Posted values are $pid and $tid";

writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("View/Print Reports",$msg);
?>
<form name="listfrm" action="home.php" method=POST>
<table border=1 cellpadding=5>
<?php
	$conid=$_SESSION['conid'];
	echo listGroupReportsByConsultant($conid,"listfrm",$tid); 
?>
</table>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>

