<?php 
require_once "../meta/candidate.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
session_start();
$msg="Enter candidate Information, click 'Add!'";
$pid=$_POST['pid'];
if("add"==$_POST['what']){
    $pid=$_POST['pid'];
    $fnam=addslashes($_POST['fnam']);
    $lnam=addslashes($_POST['lnam']);
    $email=addslashes($_POST['email']);
    $conid=$_SESSION['conid'];
    $candData=array($pid,$fnam,$lnam,$email,"Y","Y",$conid);
    if(checkCandEmail($email,$pid)){
		$i=candInsert($candData);
        if($i){
			$msg="<font color='#00aa00'>Successfully added $fnam $lnam ($email)<font>";
		}
		else{
			$msg="<font color='#aa0000'>Error adding candidate.</font>";
		}
    }
    else{
		$msg="<font color='#aa0000'>Email already exists.</font>";
    }
}

writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("Add Candidate",$msg);
?>
<form name="addfrm" action="addcand.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="pid" value="<?=$pid?>">
<table border=1 cellpadding=5>

<tr>
<td align=left>First Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="fnam" value=""></td>
</tr>

<tr>
<td align=left>Last Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="lnam" value=""></td>
</tr>

<tr>
<td align=left>Email<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="email" value=""></td>
</tr>

<tr>
<td colspan=2 align="left">
<input type="button" onClick="javascript:chkForm(addfrm);" value="Add!">
</td>
</tr>

</table>
</form>
<small>Mandatory fields are marked </small><font color="#ff0000">*</font>.
<?php
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">
function chkForm(frm){
    if(frm.fnam.value.length<1||frm.lnam.value.length<1||frm.email.value.length<1){
	alert("Please provide all mandatory fields!");
    }
    else{
	frm.what.value='add';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>
