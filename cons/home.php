<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" licensing.
*
*=============================================================================*/
$msg="";
session_start();
if("1"==$_POST['s']){
    // if we're here it's a login request
    require_once "../meta/dbfns.php";
    $uid=$_POST['uid'];
    $pwd=$_POST['pwd'];
    /*$conn=dbConnect();
    // removed 'quote_smart' function call wrapped around email value. not needed. wbs 2/7/2007
    $rs=mysql_query("select a.CONID,FNAME,LNAME from CONSULTANT a, CONS b where a.CONID=b.CONID and EMAIL='".$uid."' and PWD=PASSWORD('".$pwd."') and ACTIVE='Y'");
    // wbs 12/7/2006
    //$rs=mysql_query("select a.CONID,FNAME,LNAME from CONSULTANT a, CONS b where a.CONID=b.CONID and EMAIL=".quote_smart($uid)." and PWD=PASSWORD('".$pwd."') and ACTIVE='Y'");
    if(!($row=mysql_fetch_row($rs))){
	// Login error - go back to the login page
	header("Location: index.php?lerr=1");
    }
    else{
	$_SESSION['conid']=$row[0];
	$_SESSION['consname']=$row[1]." ".$row[2];
    }*/
    $mysqli=dbiConnect();
    if (!($query = $mysqli->prepare("select a.CONID,FNAME,LNAME from CONSULTANT a, CONS b where a.CONID=b.CONID and UPPER(EMAIL)=? and PWD=PASSWORD(?) and ACTIVE='Y'"))) {
    	echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_param("ss", strtoupper($uid), $pwd)) {
    	echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    if (!$query->execute()) {
    	echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }
    if (!$query->bind_result($out_CONID, $out_FNAME, $out_LNAME)) {
    	echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $row = $query->fetch();
    if(!($row)){
    	// Login error - go back to the login page
    	header("Location: index.php?lerr=1");
    }else{
    	$_SESSION['conid']=$out_CONID;
    	$_SESSION['consname']=$out_FNAME." ". $out_LNAME;
    }
    $query->close();
}
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$msg="Welcome ".$_SESSION['consname']."<br>";
require_once("consfn.php");
writeHead("Conflict Dynamics Profile - Consultants",false);
writeBody("Consultant Home",$msg);
?>
<table border=1>
<tr><td> CDP 360 </td><td> CDP Individual </td></tr>

<tr>
<td valign=top>
<?php
$urls=array('addpgm.php','listpgm.php','archpgm.php','../upload/upload1.php','listgrprpts.php','');
$txts=array('Add New Program','Manage Active Programs','Archived Programs','Upload Scanned Surveys','View Group Reports','');
echo "<form name='frm1' method=POST>\n";
echo "<input type=hidden name='tid' value='1'>\n";
menu($urls,$txts,"frm1");
echo "</form>\n";
?>
</td>
<td valign=top>
<?php
$urls=array('addpgm.php','listpgm.php','archpgm.php','');
$txts=array('Add New Program','Manage Active Programs','Archived Programs','');
echo "<form name='frm2' method=POST>\n";
echo "<input type=hidden name='tid' value='3'>\n";
menu($urls,$txts,"frm2");
echo "</form>\n";
?>
</td>
</tr>

<tr>
<td colspan=2>
Common functions
</td>
</tr>
<tr>
<td colspan=2>
<?php
// The log out works different for cons and impersonating admin
if(empty($_SESSION['admid'])){
    $urls=array('lichist.php','index.php');
    $txts=array('License History','Log out');
}
else{
    $urls=array('lichist.php','../admin/home.php');
    $txts=array('License History','Done');
}
echo "<form name='frm3' method=POST>";
menu($urls,$txts,"frm3");
echo "</form>";
?>
</td>
</tr>
</table>

<?php
writeFooter(false);
?>

