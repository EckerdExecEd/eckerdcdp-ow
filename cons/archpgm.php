<?php 
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$msg="";

$tid=$_POST['tid'];

if("restore"==$_POST['what']){
    if(chgProgramStatus($_POST['pid'],"N")){
	$msg="<font color='#00aa00'>Successfully restored program</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error restoring program</font>";
    }
}
writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("Manage Archived Programs",$msg);
?>
<form name="listfrm" action="archpgm.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="pid" value="">
<input type="hidden" name="tid" value="<?=$tid?>">
<table border=1 cellpadding=5>

<tr>
<td colspan=5 align="left">
Search:&nbsp;
<input type="text" name="narrow" value="">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    $narrow=$_POST['narrow'];
    $conid=$_SESSION['conid'];
    if(!listPrograms($narrow,"Y","listfrm",$conid,$tid)){
	echo "<font color='#aa0000'>Error listing programs</font></br>";
    }
?>

</table>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"listfrm");
?>
</form>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>
