<?php 
$msg="";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
require_once "../meta/program.php";
require_once "../meta/candidate.php";
$pid=$_POST['pid'];
$tid=$_POST['tid'];
if(!($pgmData=pgmInfo($pid)))
    die("Invalid program code");

$msg="";
require_once "consfn.php";

if("send"==$_POST['what']){
    $type=$_POST['mailtype'];
    $msgbody=$_POST['mailbody'];
    $rcp=array();
    $count=$_POST['count'];
    $conid=$_SESSION['conid'];
    //echo $count;
    for($i=0;$i<$count;$i++){
		$val=$_POST["rcp".$i];
		if("N"!=$val){
			$rcp[]=$val;
		}
    }
    $lst=sendCandidateEmail($rcp,$type,$msgbody,$conid);
    if(!$lst){
	$msg="<font color='#aa0000'>Unable to send email(s)</font>";
    }
    else{
	$msg="<font color='#00aa00'>Sent emails to $lst.</font>";
    } 
}

if("sendall"==$_POST['what']){
    $type=$_POST['mailtype'];
    $msgbody=$_POST['mailbody'];
    $rcp=array();
    $conid=$_SESSION['conid'];
    $pid=$_POST['pid'];
	$rcp=getAllCandidatesInProgram($pid);
    $lst=sendCandidateEmail($rcp,$type,$msgbody,$conid);
    if(!$lst){
		$msg="<font color='#aa0000'>Unable to send email(s)</font>";
    }
    else{
		$msg="<font color='#00aa00'>Sent emails to $lst.</font>";
    } 
}

writeHead("Conflict Dynamics Profile - Consultants",false);
writeBody("Email Candidates in Program:<br>'$pgmData[1]' (ID:$pgmData[0])",$msg);
?>
<form name="mailfrm" action="emailcand.php" method=POST>
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="tid" value="<?=$tid?>">
<input type="hidden" name="cid" value="">
<input type="hidden" name="what" value="">
<table border=1>
<tr>
<td><small>Name</small></td><td><small>Email</small></td><td><small> Include in mailing </small></td>
</tr>
<?
candidateMailList($pid);
if($tid=="1"){
	// This is for 360
	
?>
<tr>
<td colspan=3>
<table border=1><tr>
<td bgcolor="#dddddd">
<small>
Initial Email <input type="radio" name="mailtype" value="I" checked> 
</small>
</td>
<td bgcolor="#ffffff">
<small>
Reminder Email <input type="radio" name="mailtype" value="R" > 
</small>
</td>
<td bgcolor="#dddddd">
<small>
Custom Email <input type="radio" name="mailtype" value="C" > 
</small>
</td>
</tr></table>
</td>
</tr>
<tr>
<td colspan=3><small>
Enter text for Custom Email:<br><textarea name="mailbody" cols=40 rows=5></textarea>
<br>
<input type="button" value="Send to selected!" onClick="javascript:submitFrm(mailfrm,false);">&nbsp;&nbsp;
<input type="button" value="Send to all!" onClick="javascript:submitFrm(mailfrm,true);">
</small></td>
</tr>
</table>
<?

}
elseif($tid=="3"){
	// and this is for Individual

?>
<tr>
<td colspan=3>
<table border=1><tr>
<td bgcolor="#dddddd">
<small>
Initial Email <input type="radio" name="mailtype" value="SI" checked> 
</small>
</td>
<td bgcolor="#ffffff">
<small>
Reminder Email <input type="radio" name="mailtype" value="SR" > 
</small>
</td>
<td bgcolor="#dddddd">
<small>
Custom Email <input type="radio" name="mailtype" value="SC" > 
</small>
</td>
</tr></table>
</td>
</tr>
<tr>
<td colspan=3><small>
Enter text for Custom Email:<br><textarea name="mailbody" cols=40 rows=5></textarea>
<br>
<input type="button" value="Send to selected!" onClick="javascript:submitFrm(mailfrm,false);">&nbsp;&nbsp;
<input type="button" value="Send to all!" onClick="javascript:submitFrm(mailfrm,true);">
</small></td>
</tr>
</table>
<?php

}
$urls=array('listpgm.php');
$txts=array('Back');
menu($urls,$txts,"mailfrm");
?>
</form>
<?
writeFooter(false);
?>
<script language="JavaScript">
function submitFrm(frm,sendall){
    if(confirm("Are you sure you want to send the emails?")){
		if(sendall){
			frm.what.value="sendall";
		}
		else{
			frm.what.value="send";
		}
	frm.submit();
    }
}
</script>
