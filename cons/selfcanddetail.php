<?php 
require_once "../meta/candidate.php";
require_once "consfn.php";
session_start();

if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$msg="Edit candidate Information, click 'Save!'";
$cid=$_POST['cid'];
$fnam=addslashes($_POST['fnam']);
$lnam=addslashes($_POST['lnam']);
$email=addslashes($_POST['email']);
if("save"==$_POST['what']){
    $cid=$_POST['cid'];
    $fnam=addslashes($_POST['fnam']);
    $lnam=addslashes($_POST['lnam']);
    $email=addslashes($_POST['email']);
    $candData=array($cid,$fnam,$lnam,$email);
    if(saveCandidate($candData)){
		$msg="<font color='#00aa00'>Successfully saved $fnam $lnam ($email)</font>";
	}
	else{
		$msg="<font color='#aa0000'>Error saving candidate info.</font>";
	}
}

$rs=getCandInfo($cid);
if($rs){
    $fnam=stripslashes($rs[1]);
    $lnam=stripslashes($rs[2]);
    $email=stripslashes($rs[3]);
}


writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("Edit Candidate",$msg);
?>
<form name="edfrm" action="selfcanddetail.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="cid" value="<?=$cid?>">
<table border=1 cellpadding=5>

<tr>
<td align=left>First Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="fnam" value="<?=$fnam?>"></td>
</tr>

<tr>
<td align=left>Last Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="lnam" value="<?=$lnam?>"></td>
</tr>

<tr>
<td align=left>Email<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="email" value="<?=$email?>"></td>
</tr>

<tr>
<td colspan=2 align="left">
<input type="button" onClick="javascript:chkForm(edfrm);" value="Save!">
</td>
</tr>

</table>
</form>
<small>Mandatory fields are marked </small><font color="#ff0000">*</font>.
<?php
$urls=array('selfcandhome.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">
function chkForm(frm){
    if(frm.fnam.value.length<1||frm.lnam.value.length<1||frm.email.value.length<1){
	alert("Please provide all mandatory fields!");
    }
    else{
	frm.what.value='save';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>
