<?php
session_start();
// error_reporting(E_ALL); 
// ini_set("display_errors", 1); 
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}
$cid=$_GET['cid'];
$pid=$_GET['pid'];
$lid=$_GET['lid'];
/* Added for Comments page 17: 06/05/2008 JPC --------------------------------*/
$cPage=0; //Current Page
$pgsAdded=0; //Number of added pages for comments
$subPgs=array('A','B','C','D','E','F','G','H','I','J');
$rCnts=array(1=>0,2=>0,3=>0,4=>0,5=>0); //Counts set in renderPageIII
/*----------------------------------------------------------------------------*/
//if($cid != 2168517){
include_once "../meta/mdindividualreport.php"; 
list($orgid,$tempTxt)=getRptTemplate($pid,$lid=1);

//die(print_r($tempTxt));

$debug=false;
if(($cid != 1)&&(!$debug)){  
header("Content-Type: application/pdf");
header("Content-Disposition: inline; filename=$pid-$cid.pdf");
}
include_once "../meta/mdindividualreport.php";
$orgid=getRptTemplate($pid,$lid=1);

$pdf=pdf_new();
pdf_set_parameter($pdf,"license","L700102-010500-734253-ZZ4ND2-FMQ922");
pdf_open_file($pdf,"");

pdf_set_info($pdf,'Creator','conflictdynamics.org');
pdf_set_info($pdf,'Author','conflictdynamics.org');
pdf_set_info($pdf,'Title','Conflict Dynamics Profile');

// Default to NOT showing any data for Others of any kind
$noOther=true;
// This if for the organizational perspective
$indices=array(0,0,0);


// FORCE IT FOR TESTING!
//$noOther=false;
/**/
// if we're here we can generate the report
// Cover page
renderPageI($cid,$pdf,"i",$pid,$lid,$noOther);
/**/
// Credits page
renderPageII($cid,$pdf,"i",$pid,$lid,$noOther);

// table of contents
renderPageIII($cid,$pdf,"iii",$pid,$lid,$noOther);

// Blank page
renderBlankPage($cid,$pdf,"iv",$pid,$lid);

// This is the Introduction page
renderPage1($cid,$pdf,"1",$pid,$lid,$noOther);

// This is the Second page - begins with "responses to conflict"
renderPage2($cid,$pdf,"2",$pid,$lid,$noOther);

// This is the Third page - begins with "organizational perspective"
renderPage3($cid,$pdf,"3",$pid,$lid,$noOther);

// This is the Fourth page - Continuation of page 3
renderPage4($cid,$pdf,"4",$pid,$lid,$noOther);

// This is the Fifth page - "Guide to your feedback report"
renderPage5($cid,$pdf,"5",$pid,$lid,$noOther);

// This is the Sixth page - First Constuctive Graphs
renderPage6($cid,$pdf,"6",$pid,$lid,$noOther);

// This is the Seventh page - Constuctive Rater Agreement
renderPage7($cid,$pdf,"7",$pid,$lid,$noOther);

// This is the Eigth page - First Destuctive Graphs
renderPage8($cid,$pdf,"8",$pid,$lid,$noOther);

// This is the Ninth page - Destuctive Rater Agreement
renderPage9($cid,$pdf,"9",$pid,$lid,$noOther);

// No longer page 10, Top/Bottom 3
//renderPage10($cid,$pdf,"10",$pid,$lid,$noOther);

// This is page 10, Scale profile 
renderPage10($cid,$pdf,"10",$pid,$lid,$noOther);

// This is page 11, Discrepancy profile 
renderPage11($cid,$pdf,"11",$pid,$lid,$noOther);

// This is page 12, Intro to dynamic conflict sequence
renderPage12($cid,$pdf,"12",$pid,$lid,$noOther);

// This is page 13, Graphs for dynamic conflict sequence
renderPage13($cid,$pdf,"13",$pid,$lid,$noOther);

// This is page 14, Organizational perspective
renderPage14($cid,$pdf,"14",$pid,$lid,$noOther,$indices);

// This is page 15, Organizational perspective + Hot buttons
renderPage15($cid,$pdf,"15",$pid,$lid,$noOther,$indices);

// This is page 16, Hot buttons Continued
renderPage16($cid,$pdf,"16",$pid,$lid,$noOther);

// This will be page 17, Comments
renderPage17($cid,$pdf,"17",$pid,$lid,$noOther);

// This is page 18, Developmental Work Sheet
renderPage18($cid,$pdf,"18",$pid,$lid,$noOther);

// This is page 19, Responses to Conflict
renderPage19($cid,$pdf,"19",$pid,$lid,$noOther);

// Finally a blank page
renderBlankPage($cid,$pdf,"",$pid,$lid);
/**/

pdf_close($pdf);
echo pdf_get_buffer($pdf);
pdf_delete($pdf);
?>

