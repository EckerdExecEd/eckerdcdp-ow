<?php
require_once "../meta/program.php";
require_once "consfn.php";
session_start();
if(empty($_SESSION['conid'])){
    die("Not Logged in.");
}

$tid=$_POST["tid"];

$msg="Enter Program Information, click 'Add!'";

if("add"==$_POST['what']){
    $descr=addslashes($_POST['descr']);
    $startDt=date('Y-m-d',strtotime($_POST['startdt']));
    $endDt=date('Y-m-d',strtotime($_POST['enddt']));
    $conid=$_SESSION['conid'];
    $pgmData=array($descr,$startDt,$endDt,$conid);
    $i=pgmInsert($pgmData);
    if($i){
		// $tid is the isntrument ID
		// 1 = 360
		// 3 = Inidvidual
		if("1"==$tid){
			// 360
			if(instrInsert($i,"1")&&instrInsert($i,"2")){
				$msg="<font color='#00aa00'>Successfully added program '$descr'.</font>";
			}
			else{
				$msg="<font color='#aa0000'>Error adding instrument ($tid).</font>";
			}
		}
		elseif("3"==$tid){
			// Individual
			if(instrInsert($i,"3")){
				$msg="<font color='#00aa00'>Successfully added program '$descr'.</font>";
			}
			else{
				$msg="<font color='#aa0000'>Error adding instrument ($tid).</font>";
			}
		}
		else{
			$msg="<font color='#aa0000'>Invalid instrument ($tid).</font>";
		}
    }
    else{
		$msg="<font color='#aa0000'>Error adding program. ($tid)</font>";
    }
}

writeHead("Conflict Dynamics Profile - Consultant",false);
writeBody("Add Program",$msg);
$now=date('m/d/Y');
$end=date('m/d/Y',strtotime("$now + 1 months"));
?>
<form name="addfrm" action="addpgm.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="tid" value='<?=$tid?>'>
<table border=1 cellpadding=5>

<tr>
<td align=left>Program Name<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="descr" value="" maxlength="255"></td>
</tr>

<tr>
<td align=left>Start Date<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="startdt" value="<?=$now?>"></td>
</tr>

<tr>
<td align=left>End Date<font color="#ff0000">*</font></td>
<td align=left><input type="text" name="enddt" value="<?=$end?>"></td>
</tr>

<tr>
<td colspan=2 align="left">
<input type="button" onClick="javascript:chkForm(addfrm);" value="Add!">
</td>
</tr>

</table>
</form>
<small>Mandatory fields are marked </small><font color="#ff0000">*</font>.
<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">
function chkForm(frm){
    if(frm.descr.value.length<1||frm.startdt.value.length<1||frm.enddt.value.length<1){
	alert("Please provide all mandatory fields!");
    }
    else{
	frm.what.value='add';
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>
