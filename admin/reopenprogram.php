<?php 
session_start();
require_once "../meta/program.php";
require_once "admfn.php";
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";
$what=$_POST['what'];
$pid=$_POST['pid'];

if($what=="open"){
	$msg=sendReopenEmail($pid);	
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Re-open Program",$msg);
?>
<form name="listfrm" action="reopenprogram.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="exp" value="Y">
<input type="hidden" name="pid" value="<?=$pid?>">
<table border=1 cellpadding=5>

<?php
    if(!listProgramsToReopen("listfrm")){
		echo "<font color='#aa0000'>No pending programs</font></br>";
    }
?>

</table>
</form>
<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
writeFooter(false);
?>

