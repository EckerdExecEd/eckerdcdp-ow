<?php 
require_once "../meta/consultant.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";

if("restore"==$_POST['what']){
    if(chgConsultantStatus($_POST['conid'],"Y")){
	$msg="<font color='#00aa00'>Successfully restored consultant</font>";
    }
    else{
	$msg="<font color='#aa0000'>Error restoring consultant</font>";
    }
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Manage Archived Consultants",$msg);
?>
<form name="listfrm" action="archcons.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="conid" value="A">
<table border=1 cellpadding=5>

<tr>
<td colspan=3 align="left">
Search:&nbsp;
<input type="text" name="narrow" value="">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    $narrow=$_POST['narrow'];
    if(!listConsultants($narrow,"N","listfrm")){
	$msg="<font color='#aa0000'>Error listing consultants</font></br>";
    }
?>

</table>
</form>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">

</script>
<?php
writeFooter(false);
?>
