<?php
require_once "../meta/program.php";
require_once "../meta/candidate.php";
require_once "../cons/consfn.php";
session_start();

if(empty($_SESSION['admid'])){
  die("Not Logged in.");
}

$msg="Edit candidate Information, click 'Save!'";
$cid=$_POST['cid'];
$pid=$_POST['pid'];
$tid=$_POST['tid'];
$fnam=addslashes($_POST['fnam']);
$lnam=addslashes($_POST['lnam']);
$email=addslashes($_POST['email']);
$lid="";
if("save"==$_POST['what']){
    $cid=$_POST['cid'];
    $pid=$_POST['pid'];
    $fnam=addslashes($_POST['fnam']);
    $lnam=addslashes($_POST['lnam']);
    $email=addslashes($_POST['email']);
	// language support
	$lid=addslashes($_POST['lid']);
	if(strlen($lid)<1){
		$lid="1";
	}

    $candData=array($cid,$fnam,$lnam,$email,$lid);
    if(checkCandEmail($email,$pid,$cid)){
        if(saveCandidate($candData)){
			$msg="<font color='#00aa00'>Successfully saved $fnam $lnam ($email)</font>";
		}
		else{
			$msg="<font color='#aa0000'>Error saving candidate info.</font>";
		}
    }
    else{
		$msg="<font color='#aa0000'>Email already exists.</font>";
    }
}

$rs=getCandInfo($cid);
if($rs){
  $fnam=stripslashes($rs[1]);
  $lnam=stripslashes($rs[2]);
  $email=stripslashes($rs[3]);
	// language support
	$lid=stripslashes($rs[10]);
}


writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Edit Candidate (Admin)",$msg);
?>
<form name="edfrm" action="admcanddetail.php" method="post">
<input type="hidden" name="what" value="">
<input type="hidden" name="pid" value="<?=$pid?>">
<input type="hidden" name="cid" value="<?=$cid?>">
<input type="hidden" name="tid" value="<?=$tid?>">

<table border="1" cellpadding="5">

<tr>
<td align=left>First Name<span style="color:#ff0000;">*</span></td>
<td align=left><input type="text" name="fnam" value="<?=$fnam?>"></td>
</tr>

<tr>
<td align=left>Last Name<span style="color:#ff0000;">*</span></td>
<td align=left><input type="text" name="lnam" value="<?=$lnam?>"></td>
</tr>

<tr>
<td align=left>Email<span style="color:#ff0000;">*</span></td>
<td align=left><input type="text" name="email" value="<?=$email?>"></td>
</tr>

<tr>
<td align=left>Language<span style="color:#ff0000;">*</span></td>
<td align=left><?=languageBox($tid,$lid)?></td>
</tr>

<tr>
<td colspan="2" align="left">
<input type="button" onClick="javascript:chkForm(edfrm);" value="Save!">
</td>
</tr>

</table>
<small>Mandatory fields are marked </small><span style="color:#ff0000;">*</span>.
<?php
$urls=array('candSearch.php');
$txts=array('Back');
menu($urls,$txts,"edfrm");
?>
</form>
<script type="text/javascript">
function chkForm(frm){
if(frm.fnam.value.length<1||frm.lnam.value.length<1||frm.email.value.length<1){
alert("Please provide all mandatory fields!");
}
else{
frm.what.value='save';
frm.submit();
}
}
</script>
<?php
writeFooter(false);
?>
