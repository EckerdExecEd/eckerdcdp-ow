<?php

require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
  die("Not Logged in.");
}

$msg="";


writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Search for Candidates",$msg);
?>
<script type="text/javascript">
function ckNull(){
  var bool=true;
  var fname=document.getElementById('fname').value;
  var lname=document.getElementById('lname').value;
  var email=document.getElementById('email').value;
  if(fname=="" && lname=="" && email==""){
    alert("No search criteria has been entered.");
    bool=false;
  }
  return bool;
}
</script>
<form name="listfrm" action="candList.php" onsubmit="javascript:return ckNull()" method="get">
<div style="font-size:14px;margin-bottom:5px;">Please search by one field only.</div>
<table border="1" cellpadding="5">
<tr>
<td colspan="3" align="left">
First Name:&nbsp;
</td>
<td>
<input type="text" name="fname" id="fname" value="">&nbsp;
<input type="submit" value="Search">
</td>
</tr>
<tr>
<td colspan="3" align="left">
Last Name:&nbsp;
</td>
<td>
<input type="text" name="lname" id="lname" value="">&nbsp;
<input type="submit" value="Search">
</td>
</tr>
<tr>
<td colspan="3" align="left">
Email Addr:&nbsp;
</td>
<td>
<input type="text" name="email" id="email" value="">&nbsp;
<input type="submit" value="Search">
</td>
</tr>
</table>
</form>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");

writeFooter(false);
?>
