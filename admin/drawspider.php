<?php
define("PAGE_ORIENTATION",0);
define("PAGE_HEIGHT",792);
define("PAGE_WIDTH",612);
$license="L700102-010500-734253-ZZ4ND2-FMQ922";

header("Content-Type: application/pdf");
//header("Content-Disposition: inline; filename=$gid-$pin.pdf");
header("Content-Disposition: inline; filename=test.pdf");

$pdf=pdf_new();
pdf_set_parameter($pdf,"license",$license);
pdf_open_file($pdf,"");
pdf_set_info($pdf,'Creator','kiwi@presensit.com');
pdf_set_info($pdf,'Author','kiwi@presensit.com');
pdf_set_info($pdf,'Title','Graph test');

drawAReallyComplexPage($pdf);

drawAnotherReallyComplexPage(&$pdf);

pdf_close($pdf);
echo pdf_get_buffer($pdf);
pdf_delete($pdf);
exit;

function drawAReallyComplexPage(&$pdf){
	// begin a new page
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);

	// this is some faked out data
	$data=array(0,1,2,3,4,5);
	$x=300;
	$y=350;
	$radius=275;
	$colors=array(1.0,0.5,0.5);

	// print some kind of header somewhere above the graph
	$font=pdf_findfont($pdf,"Helvetica","host",0);
	pdf_setfont($pdf,$font,18.0);
	pdf_show_xy($pdf,"This is an Incredibly Complicated Graph",$x-150,$y+400);

	// use the bogus data to draw the graph
	drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors);

	//end of this page
	pdf_end_page($pdf);
}

function drawAnotherReallyComplexPage(&$pdf){
	// begin a new page
	pdf_begin_page($pdf,PAGE_WIDTH,PAGE_HEIGHT);

	// this is some faked out data
	$data=array(1.7,1.2,2.9,4.5,3.6,3.6);
	$x=150;
	$y=625;
	$radius=150;
	$colors=array(0.5,1.0,0.5);
	// use the bogus data to draw the graph
	drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors);

	$data=array(1.7,1.2,2.9,4.5,3.6,3.6,2.6,3.2,2.3);
	$x=450;
	$y=625;
	$radius=150;
	$colors=array(1.0,0.0,0.0);
	// use the bogus data to draw the graph
	drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors);

	$data=array(4.4,1.7,3.6,3.1,2.7,4.4,1.7,3.6,3.1,2.7,4.4,1.7,3.6,3.1,2.7,2.3);
	$x=325;
	$y=400;
	$radius=100;
	$colors=array(0.5,1.0,1.0);
	// use the bogus data to draw the graph
	drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors);
	
	$data=array(3.2,4.2,1.3,3.1,3.6,3.7,2.2,3.9,2.9,3.3,2.3);
	$x=160;
	$y=175;
	$radius=150;
	$colors=array(1.0,1.0,0.0);
	// use the bogus data to draw the graph
	drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors);

	$data=array(4.4,1.7,3.6,3.1,2.7);
	$x=450;
	$y=175;
	$radius=150;
	$colors=array(0.0,1.0,0.0);
	// use the bogus data to draw the graph
	drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors);

	//end of this page
	pdf_end_page($pdf);
}


function drawTheIncrediblyComplicatedGraph(&$pdf,$data,$x,$y,$radius,$colors){
	// How many sectors do we need in this particular case?
	$scount=count($data);

	$angle=360/$scount;

	// Usa a 1 pixel pen 
	pdf_setlinewidth($pdf,1);
	pdf_setcolor($pdf,"both","rgb",$colors[0],$colors[1],$colors[2],0);
	
	// for each sector of the graph
	for($i=0;$i<$scount;$i++){
		pdf_moveto($pdf,$x,$y);
		// the scale goes from 1 to 5
		$rad=($data[$i]/5)*$radius;
		
		// Draw to the first corner of the sector
		// Note that we draw in reverse order, clockwise
		$j=$scount-$i;
		$a1=deg2rad(($j*$angle)+90);

		$xpos=$x+($rad*cos($a1));
		$ypos=$y+($rad*sin($a1));
		pdf_lineto($pdf,$xpos,$ypos);
		
		// Draw to the second corner of the sector
		$a2=deg2rad((($j-1)*$angle)+90);
		$xpos=$x+($rad*cos($a2));
		$ypos=$y+($rad*sin($a2));
		pdf_lineto($pdf,$xpos,$ypos);
		
		pdf_lineto($pdf,$x,$y);
		pdf_fill_stroke($pdf);
	}
	
	// draw the black outlines
	pdf_setcolor($pdf,"both","rgb",0,0,0,0);
	
	for($i=0;$i<$scount;$i++){
		// Spokes
		$a1=deg2rad($i*($angle)+90);
		$a2=deg2rad(($i-1)*($angle)+90);
		pdf_moveto($pdf,$x,$y);
		$xpos=$x+($radius*cos($a1));
		$ypos=$y+($radius*sin($a1));
		pdf_lineto($pdf,$xpos,$ypos);
		
		// The 1-5 scales
		for($j=1;$j<=5;$j++){
			$rad=($j*$radius)/5;
			$xpos=$x+($rad*cos($a1));
			$ypos=$y+($rad*sin($a1));
			pdf_moveto($pdf,$xpos,$ypos);
			$xpos=$x+($rad*cos($a2));
			$ypos=$y+($rad*sin($a2));
			pdf_lineto($pdf,$xpos,$ypos);
		}
	}
	pdf_fill_stroke($pdf);
	
}
?>
