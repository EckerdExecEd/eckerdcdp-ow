<?php 
session_start();
require_once "../meta/program.php";
require_once "admfn.php";
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";
$what=$_POST['what'];
$pid=$_POST['pid'];

if($what=="score"){
	$tid=getTypeOfScore($pid);
	if(false==$tid){
		$msg="<font color=\"#ff0000\">Unable to score program: unable to get type</font>";		
	}
	else{
		if($tid=="1"){
			require_once "../meta/score.php";	
		}
		elseif($tid=="3"){
			require_once "../meta/scoreandreportselfonly.php";	
		}
		$msg=scoreProgram($pid);
		sendScoreEmail($pid);
	}
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Score Program",$msg);
?>
<form name="listfrm" action="scoreprogram.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="exp" value="Y">
<input type="hidden" name="pid" value="<?=$pid?>">
<table border=1 cellpadding=5>

<?php
    if(!listProgramsToScore("listfrm")){
		echo "<font color='#aa0000'>No pending programs</font></br>";
    }
?>

</table>
</form>
<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
writeFooter(false);
?>

