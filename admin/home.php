<?php
/*=============================================================================*
* 04-22-2005 TRM: Added support for "Self Only" licensing.
*
*=============================================================================*/
$msg="";
session_start();
if("1"==$_POST['s']){
    // if we're here it's a login request
	require_once "../meta/dbfns.php";
	$uid=$_POST['uid'];
	$pwd=$_POST['pwd'];
	$mysqli=dbiConnect();
	if (!($query = $mysqli->prepare("select CONID,FNAME,LNAME from CONSULTANT, ADMINS where CONID=ADMID and UID=(?) and PWD=PASSWORD(?)"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_param("ss", $uid, $pwd)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	if (!$query->execute()) {
		echo "Execute failed: (" . $mysqli->errno . ") " . $mysqli->error;
	}
	if (!$query->bind_result($out_CONID, $out_FNAME, $out_LNAME)) {
	    echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
	}
	$row = $query->fetch();

    if(!($row)){
	// Login error - go back to the login page
	header("Location: index.php?lerr=1");
    }
    else{
	$_SESSION['admid']=$out_CONID;
	$_SESSION['name']=$out_FNAME." ".$out_LNAME;
    }
}
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}
if(isset($_SESSION['conid'])){
    // make sure we don't have a consultant selected here
    unset($_SESSION['conid']);
}
$msg="Welcome ".$_SESSION['name']."<br>" .$debug;
require_once("admfn.php");
writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Administrator Home",$msg);
// 02/08/2008: Added support for multiple admins, some with non-godlike powers. Requested by Patty. TRM.
if(1==$_SESSION['admid']){
	$urls=array('addcons.php','listcons.php','candSearch.php','license.php','selflicense.php','mailcons.php',
		'archcons.php','editcomments.php','scoreprogram.php','reopenprogram.php','grpreport.php','index.php');
	$txts=array('Add Consultant','Manage Active Consultants','Search For Candidates','360 License Management','Self-Only License Management',
		'Email Active Consultants','Manage Archived Consultants','Edit Rater Comments','Score Program','Re-open Program','Create Group Report','Log out');
}
else{
	$urls=array('listcons.php','candSearch.php','reopenprogram.php','index.php');
	$txts=array('Manage Active Consultants','Search For Candidates','Re-open Program','Log out');
}
menu($urls,$txts,"");
writeFooter(false);
?>

