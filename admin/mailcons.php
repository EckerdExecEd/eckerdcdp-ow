<?php 
require_once "../meta/consultant.php";
require_once "admfn.php";
session_start();
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";

if("send"==$_POST['what']){
    $subject=$_POST['subject'];
    $msgBody=$_POST['msgbody'];
    $maxItems=$_POST['items'];
    $conids=array();
    for($i=0;$i<$maxItems;$i++){
		$yorn=$_POST["rcp".$i];
		if("Y"==$yorn){
			$conids[]=$_POST["conid".$i];
		}
    }
    if(!($rc=sendConsEmail($conids,$subject,$msgBody))){
		$msg="<font color='aa0000'>Unable to send email(s)</font>";
    }
    else{
		$msg="<font color='00aa00'>Sent email(s) to ".$rc."</font>";
    }
}

if("sendall"==$_POST['what']){
    $subject=$_POST['subject'];
    $msgBody=$_POST['msgbody'];
    $conids=array();
	$conids=getAllConids();
    if(!($rc=sendConsEmail($conids,$subject,$msgBody))){
		$msg="<font color='aa0000'>Unable to send email(s)</font>";
    }
    else{
		$msg="<font color='00aa00'>Sent email(s) to ".$rc."</font>";
    }
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Email Consultants",$msg);
?>
<form name="listfrm" action="mailcons.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="conid" value="">
<table border=1 cellpadding=5>

<tr>
<td colspan=3 align="left">
Search:&nbsp;
<input type="text" name="narrow" value="">&nbsp;
<input type="submit" value="Refresh View!">
</td>
</tr>

<?php
    $narrow=$_POST['narrow'];
    if(!listMailAddresses($narrow,"Y","listfrm")){
	echo "<font color='#aa0000'>Error listing consultants</font></br>";
    }
?>
<tr>
<td valign=top>
Subject:
</td>
<td colspan=2 align=left>
<input type="text" name="subject" value="<?=$_POST['subject']?>" size=60>
</td>
</tr>

<tr>
<td valign=top>
Message:
</td>
<td colspan=2 align=left>
<textarea name="msgbody" cols=60 rows=5><?=$_POST['msgbody']?></textarea>
</td>
</tr>

<tr>
<td colspan=3 align=left>
<input type="button" value="Send Mail to Selected!" onClick="javascript:chkForm(listfrm,false);">&nbsp;&nbsp;
<input type="button" value="Send Mail to All!" onClick="javascript:chkForm(listfrm,true);">
</td>
</tr>
</table>
</form>

<?php
$urls=array('home.php');
$txts=array('Back');
menu($urls,$txts,"");
?>
<script language="Javascript">
function chkForm(frm,sendall){
    if(frm.subject.value.length<1){
	alert("Please provide a subject!");
    }
    else if(frm.msgbody.value.length<1){
	alert("The message body is empty!");
    }
    else{
		if(sendall){
			frm.what.value='sendall';
		}
		else{
			frm.what.value='send';
		}
        frm.submit();
    }
}
</script>
<?php
writeFooter(false);
?>
