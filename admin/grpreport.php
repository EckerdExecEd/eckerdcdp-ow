<?php 
session_start();
require_once "../meta/groups.php";
require_once "admfn.php";
if(empty($_SESSION['admid'])){
    die("Not Logged in.");
}

$msg="";
$what=$_POST['what'];
$show=$_POST['show'];
$conid=$_POST['conid'];
$instr;

switch($show){
	case "progs":
		$instr="Select the programs you would like to include in the report.";
		break;
	case "cands":
		$instr="Select the candidates you would like to include in the report.";
		break;
	default:
		$instr="Select the consultant who will receive the report.";
}

writeHead("Conflict Dynamics Profile - Admin",false);
writeBody("Generate Group Reports",$msg. " ".$instr);
?>
<form name="listfrm" action="grpreport.php" method=POST>
<input type="hidden" name="what" value="">
<input type="hidden" name="show" value="<?=$show?>">
<table border=1 cellpadding=5>

<?php
	if($show=="progs"){
		// show eligible programs for this consultant
		echo "<input type='hidden' name='conid' value='$conid'>";
		showPrograms($conid,"listfrm");
	}
	elseif($show=="cands"){
		// show candidates in the selected programs
		echo "<input type='hidden' name='conid' value='$conid'>";
		// get the selected programids into an array
		$pids=array();
		$keys=array_keys($_POST);
		foreach($keys as $key){
			if("on"==$_POST[$key]){
				$pids[]=$key;	
			}
		}
		showCandidates($pids,"listfrm");
	}
	else{
		// default: show the consultants
		showConsultants("listfrm");
	}
?>

</table>
</form>
<?php
showScripts("listfrm");
$urls=array('home.php');
$txts=array('Cancel');
menu($urls,$txts,"");
writeFooter(false);
?>

